let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
.combine(['resources/assets/js/toastr.min.js'],'public/js/extras.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   	.combine([
   		'resources/assets/css/style.css', 
   		'resources/assets/css/toastr.css',
   		],'public/css/style.css');
*/
/*mix.js('resources/assets/js/app.js', 'public/js')
.sass('resources/assets/sass/app.scss', 'public/css')
   	.combine(['resources/assets/css/style.css'],'public/css/style.css');*/
mix.js('resources/assets/js/app.js', 'public/js')
    .scripts([
      'resources/js/alerts/sweetalert2.all.min.js',
      'resources/js/alerts/toastr.js',
      'resources/js/charts/Chart.min.js',
    ], 'public/js/alerts.js')
    .webpackConfig({
         module: {
             rules: [
                 {
                     test: /\.jsx?$/,
                     exclude: /node_modules(?!\/foundation-sites)|bower_components/,
                     use: [
                         {
                             loader: 'babel-loader',
                             options: Config.babel()
                         }
                     ]
                 },
                 
			    {
				  test: /\.js$/,
				  loader: 'babel-loader',
				  exclude: /node_modules/,
				},
				{
				  test: /\.(png|jpg)$/,
				  loader: 'file-loader',
				  options: {
				    name: 'images/[name].[ext]'
				  }
				}

             ]
         },
         resolve: {
            alias: {
              '@': path.resolve('resources/assets/sass')
            }
          },
          resolve: {	
			       extensions: [ '.js', '.vue', '.json' ],
			       alias: {
            '@': path.resolve( __dirname, '..' )
			   }
			}
     })
   .sass('resources/assets/sass/app.scss', 'public/css') 
   .combine(['resources/assets/css/style.css'],'public/css/style.css');
