<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function($router) {
	
	Route::post('login', 				'AuthController@login');
	Route::post('logout', 				'AuthController@logout');
	Route::post('refresh', 				'AuthController@refresh');
	Route::post('me', 					'AuthController@me');
	Route::post('register', 			'AuthController@register');
	Route::post('pyrs/update/{id}', 	'AuthController@updateUser');
	Route::post('email', 				'AuthController@recover');
	Route::post('reset', 				'AuthController@reset');

});

Route::group(['prefix' => 'admin/api'], function(){
	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');
	Route::get('cmbClubs', 			'Web\CombosController@comboClubs');
	Route::get('cmbEntrenador', 	'Web\CombosController@selectEntrenador');

});

Route::prefix('api')->group(function(){
	Route::get('jumbotron/{id}', 						'Web\PageController@getJumbotron');
	Route::get('columnas/{id}', 						'Web\PageController@getColumnas');
	Route::get('welcome/{id}', 							'Web\PageController@getWelcome');
	Route::get('social/{id}',							'Web\PageController@getSocial');
	Route::get('patrocinadores/{id}',					'Web\PageController@getPatrocinadores');
	Route::get('noticias/{id}', 						'Web\PageController@getNoticias');
	Route::get('lstnoticias', 							'Web\PageController@indexNoticias');
	Route::get('partidas', 								'Web\PageController@getPartidas');
	Route::get('ligas/{id}', 							'Web\PageController@getLigas');
	Route::get('clasificacionLiga/{id}', 				'Web\PageController@getClasificacionLiga');
	Route::get('resultadosLiga/{id}', 					'Web\PageController@getResultadosLiga');
	Route::get('mispartidas', 							'Web\PageController@misPartidas');
	Route::post('mispartidas/eliminar', 				'Web\PageController@eliminarPartida');
	Route::get('misesperas', 							'Web\PageController@misEsperas');
	Route::delete('misesperas/eliminar/{id}', 			'Web\PageController@eliminarEspera');
	Route::post('espera', 								'Web\PageController@listaEspera');
	Route::post('espera/apuntado', 						'Web\PageController@apuntadoEspera');
	Route::get('galeria/{id}', 							'Web\PageController@getGalerias');
	Route::get('glrs/{galeria}', 						'Web\PageController@galeria');
	Route::get('slider/{id}', 							'Web\PageController@getSlider');
	Route::post('contacto',								'Web\PageController@contacto');
	Route::post('apuntarse',							'Web\PageController@addUserTorneo');
	Route::delete('borrarse/{id}',						'Web\PageController@delUserTorneo');
	Route::get('torneos',								'Web\PageController@getTorneo');
	/*Route::get('cls/{id}', 					'Web\PageController@getClases');
	Route::get('cpt/{id}', 					'Web\PageController@getCompeticion');*/


	Route::get('cmbprvs',			'Web\CombosController@comboProvincias');
	Route::get('pblprv',			'Web\CombosController@poblacionProvincia');
	Route::get('cmbnvls',			'Web\CombosController@comboNiveles');
	Route::get('cmbnsts',			'Web\CombosController@comboInstalaciones');
	Route::get('pyrs/club/',		'Web\CombosController@clubUser');
	Route::get('pistasDisponibles',	'Web\CombosController@pistasDisponibles');
	Route::get('stats', 			'Web\CombosController@estadisticas');
	Route::get('cmbClubs', 			'Web\CombosController@comboClubs');
	
	Route::post('registerUser', 		'userAuth\RegisterController@register');
});
