	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('{any}', function () {
    return view('welcome');
})->where('any', '.*');
*/
//Route::get('/{club}' , 'HomeController@index');
Route::get('/' , 'HomeController@index');
Auth::routes();


Route::get('uploadCSV', 'uploadCSV@index')->name('vercsv');
Route::post('uploadCSV', 'uploadCSV@upload')->name('uploadcsv');


//Club
Route::group(['prefix' => 'admin'], function(){
	// Authentication Routes...
	Route::get('iniciarSesion', 			'clubAuth\LoginController@showLoginForm')->name('club.login');
	Route::post('iniciarSesion', 			'clubAuth\LoginController@login');
	Route::get('logout', 					'clubAuth\LoginController@logout');

	// Password Reset Routes...
	Route::get('club-password/reset', 				'clubAuth\ForgotPasswordController@showLinkRequestForm')->name('club.password.request');
	Route::post('club-password/email', 			'clubAuth\ForgotPasswordController@sendResetLinkEmail')->name('club.password.email');
	Route::get('club-password/reset/{token}', 		'clubAuth\ResetPasswordController@showResetForm')->name('club.password.reset');
	Route::post('club-password/reset', 			'clubAuth\ResetPasswordController@reset');

	
	Route::get('dashboard',    					'Club\ClubController@index')->name('club.dashboard');
	Route::get('jugadores', 					'Club\JugadoresController@viewIndex');
	Route::get('ver/{id}', 						'Club\JugadoresController@show');
	Route::resource('pyrs', 					'Club\JugadoresController', ['except' => ['create']]);
	Route::post('pyrs/update/{id}', 			'Club\JugadoresController@update');
	Route::get('clbs/{id}', 					'Club\ClubController@show');
	Route::post('clbs/update/{id}', 			'Club\ClubController@update');
	Route::post('clbs/imagen',					'Club\ClubController@uploadImage');
	
	//rutas galeria
	Route::get('glrs', 							'Club\GaleriaController@listaGalerias');
	Route::delete('glrs/{galeria}/{imagen}',  	'Club\GaleriaController@eliminarImagen');
	Route::delete('glrs/{galeria}',  			'Club\GaleriaController@eliminarGaleria');
	Route::get('glrs/{galeria}', 				'Club\GaleriaController@galeria');
	Route::post('glrs', 						'Club\GaleriaController@crearGaleria');
	Route::patch('glrs/{galeria}', 				'Club\GaleriaController@update');
	Route::post('glrs/subirImagenes', 			'Club\GaleriaController@subirImagenes');
	
	//rutas patrocinadores
	Route::get('ptdr', 							'Club\PatrocinadorController@listaPatrocinadores');
	Route::delete('ptdr/{id}',  				'Club\PatrocinadorController@destroy');
	Route::post('ptdr', 						'Club\PatrocinadorController@crearPatrocinador');
	Route::post('ptdr/update/{id}', 			'Club\PatrocinadorController@update');

	//rutas slider
	Route::resource('sldr', 					'Club\SliderController', ['only' => ['index']]);
	Route::delete('sldr/{id}',  				'Club\SliderController@destroy');
	Route::post('sldr', 	 					'Club\SliderController@slider');

	//rutas partidas
	Route::resource('ptds', 					'Club\PartidasController', ['except' => ['create', 'update']]);
	Route::post('ptds/update/{id}',				'Club\PartidasController@update');
	Route::patch('ptds/posicion/{id}', 			'Club\PartidasController@updatePos');
	Route::resource('hrpst',					'Club\HorarioController');
	//rutas noticias
	Route::resource('ntcs',						'Club\NoticiasController', ['except' => ['create']]);
	Route::post('ntcs/imagen',					'Club\NoticiasController@subirImagen');
	Route::post('ntcs/update/{id}', 			'Club\NoticiasController@update');

	//rutas club
	Route::get('content/jumbotron', 			    'Club\ClubController@indexJumbotron');
	Route::post('content/jumbotron', 				'Club\ClubController@jumbotron');
	Route::get('content/columnas', 					'Club\ClubController@getColumnas');
	Route::post('content/columnas', 				'Club\ClubController@columnas');
	Route::get('content/welcome', 					'Club\ClubController@getWelcome');
	Route::post('content/welcome', 					'Club\ClubController@welcome');
	Route::get('content/social', 					'Club\ClubController@getSocial');
	Route::post('content/social', 					'Club\ClubController@social');
	Route::post('content/paralax', 					'Club\ClubController@paralax');
	Route::patch('content/update/{id}', 			'Club\ClubController@updateMod');
	
	//rutas clases y competiciones
	Route::get('cpt', 			    				'Club\CompeticionController@index');
	Route::post('cpt', 			    				'Club\CompeticionController@competicion');
	Route::get('cls', 			    				'Club\ClasesController@index');
	Route::post('cls', 								'Club\ClasesController@clases');

	//rusta lista de espera
	Route::resource('spr',							'Club\EsperaController', ['only' => ['store','destroy']]);    	

	//rutas ligas
	Route::resource('lgas',							'Club\LigasController', ['except' => ['create']]);
	Route::post('ljrnd/{id}',						'Club\LigasController@jornadas');
	Route::patch('ljrnd/update/{id}',				'Club\LigasController@updateResultados');
	Route::get('ljrnd/resultados/{id}',				'Club\LigasController@getResultados');
	Route::resource('lcsfn',						'Club\EquiposController', ['except' => ['create']]);
	//Route::resource('lrltd',						'Club\ResultadosController', ['except' => ['create']]);

	//rutas torneos
	Route::resource('trns',							'Club\TorneosController', ['except' => ['create', 'show']]);
	//Route::resource('tcsfn',						'Club\ClasificacionTorneoController', ['except' => ['create']]);
	//Route::resource('trltd',						'Club\ResultadosTorneoController', ['except' => ['create']]);
	Route::post('tjrnd/{id}',						'Club\TorneosController@jornadas');
	Route::patch('tjrnd/update/{id}',				'Club\TorneosController@updateResultados');
	Route::get('tjrnd/resultados/{id}',				'Club\TorneosController@getResultados');	
	Route::delete('trns/userRegistrado/{id}',		'Club\TorneosController@destroyUserRegistrado');
	Route::delete('trns/userAnonimo/{id}',			'Club\TorneosController@destroyUserAnonimo');
	Route::get('trns/usersTorneo/{id}',				'Club\TorneosController@usersTorneo');
	Route::post('trns/addUser',						'Club\TorneosController@addUserTorneo');
	Route::get('trns/torneos', 						'Club\TorneosController@getTorneos');
	//emails
	Route::get('emails',						'Club\EmailsController@index'); 
	Route::get('plantillasMail',				'Club\EmailsController@plantillas'); 
	Route::post('emails/imagen',				'Club\EmailsController@subirImagen');
	Route::post('emails/enviar',				'Club\EmailsController@enviar'); 
	Route::post('emails/guardar',				'Club\EmailsController@store');
	Route::delete('emails/{id}',				'Club\EmailsController@destroy');
	Route::get('queue',							'Club\EmailsController@queue');
	//Entrenadores
	Route::get('ntrdor',						'Club\EntrenadorController@index');
	Route::post('ntrdor',						'Club\EntrenadorController@store');
	Route::put('/ntrdor/activar',      			'Club\EntrenadorController@activar');
	Route::put('/ntrdor/desactivar',   			'Club\EntrenadorController@desactivar');
	Route::post('/ntrdor/update/{id}', 			'Club\EntrenadorController@update');

	
});



//Route::post('pyrs', 					'Web\PageController@registerUser');

Route::get('iniciarSesion', 			'userAuth\LoginController@showLoginForm')->name('user.login');
Route::post('iniciarSesion', 			'userAuth\LoginController@login');
Route::get('logout', 					'userAuth\LoginController@logout');
					