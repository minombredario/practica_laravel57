<?php
	$servername = "localhost";
	$username = "root";
	$password = "9S6wg!4a";
	$dbname = "PracticaPadel_";

    // Crear una conexión a la base de datos
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Verificar si la conexión fue exitosa
    if ($conn->connect_error) {
        // Si hay un error en la conexión, imprimir el mensaje de error y terminar el script
        die("Connection failed: " . $conn->connect_error);
    }

	/*$sql = "select p.clubs_id, p.pistas_id, p.nivelPartida_id, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.inicio_alquiler)), '%Y-%m-%d %H:%i:%s') as inicio, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.fin_alquiler)), '%Y-%m-%d %H:%i:%s') as fin, p.status, p.abierta_por, p.partida_fija, p.equipo, p.texto, p.mensaje, p.torneo, p.cartel, p.observaciones,CURRENT_TIMESTAMP as created_at,CURRENT_TIMESTAMP as updated_at, pu.club_id, pu.pala_id, pu.pala, pu.pala2_id, pu.pala2, pu.pala3_id, pu.pala3, pu.pala4_id, pu.pala4, pf.id as fija_id
from partidas as p inner join partidas_fijas as pf on p.id = pf.partida_id inner join partidas_user as pu on p.id = pu.partida_id where WEEKDAY(p.inicio_alquiler) = WEEKDAY(now())-1;";*/

    $sql = "SELECT p.clubs_id, p.pistas_id, p.nivelPartida_id, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 8 DAY)), ' ', time(p.inicio_alquiler)), '%Y-%m-%d %H:%i:%s') as inicio, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 8 DAY)), ' ', time(p.fin_alquiler)), '%Y-%m-%d %H:%i:%s') as fin, p.status, p.abierta_por, p.partida_fija, p.equipo, p.clase, p.mensaje, p.torneo, p.cartel, p.observaciones,CURRENT_TIMESTAMP as created_at,CURRENT_TIMESTAMP as updated_at, pu.club_id, pf.jugadores_fijos, pf.id as fija_id, p.entrenador_id
            FROM partidas AS p
            INNER JOIN partidas_fijas AS pf ON p.id = pf.partida_id
            INNER JOIN partidas_user AS pu ON p.id = pu.partida_id
            WHERE WEEKDAY(p.inicio_alquiler) = WEEKDAY(now())+1;";

	$result = $conn->query($sql);

if (!$result) {
    echo "Error in query: " . $conn->error;
} else {
    // Abrir archivos CSV para escritura y lectura
    $partidas_csv = fopen('partidas.csv', 'a+');
    $usuarios_csv = fopen('usuarios.csv', 'a+');

    if ($result->num_rows > 0) {
        // Encabezados de los archivos CSV
        fputcsv($partidas_csv, ["Fecha", "consulta", "partida_id", "inicio", "fin", "pista", "mensaje"]);
        fputcsv($usuarios_csv, ["Fecha", "consulta", "partida_id", "mensaje"]);

        fgets($partidas_csv); //coloco el puntero al final del documento
        fseek($partidas_csv, -2, SEEK_END);

        $usuarios_csv = fopen('usuarios.csv', 'a+');
        fgets($partidas_csv);
        fseek($partidas_csv,-2,SEEK_END);

        $fecha = new DateTime();
        $hora_registro = date('m/d/Y H:i:s', $fecha->getTimestamp());
        // output data of each row
        while($row = $result->fetch_assoc()) {

//            $field1		= $row["clubs_id"];
//            $field2		= $row["pistas_id"];
//            $field3 	= $row["nivelPartida_id"];
//            $field4 	= $row["inicio"];
//            $field5 	= $row["fin"];
//            $field6 	= $row["status"];
//            $field7		= $row["abierta_por"];
//            $field8		= $row["partida_fija"];
//            $field9		= $row["equipo"];
//            $field10	= $row["clase"];
//            $field11	= $row["mensaje"];
//            $field12 	= $row["torneo"];
//            $field13 	= $row["cartel"];
//            $field14	= $row["observaciones"];
//            $field15 	= $row["created_at"];
//            $field16 	= $row["updated_at"];

            $fijos = json_decode($row['jugadores_fijos']);

            if ($fijos === null && json_last_error() !== JSON_ERROR_NONE) {
                // Hubo un error al decodificar el JSON
                // Puedes manejar el error aquí o simplemente asignar un objeto vacío
                $fijos = (object)[];
            }

            $field17 = $fijos->pala_id ?? "";
            $field18 = $fijos->pala ?? "";
            $field19 = $fijos->pala2_id ?? "";
            $field20 = $fijos->pala2 ?? "";
            $field21 = $fijos->pala3_id ?? "";
            $field22 = $fijos->pala3 ?? "";
            $field23 = $fijos->pala4_id ?? "";
            $field24 = $fijos->pala4 ?? "";

            $field25 = $row["fija_id"];
            $field26 = $row["entrenador_id"];

            // Consulta SQL para insertar en la tabla partidas
            $sqlInsertPartidas = "INSERT IGNORE INTO `partidas` (`clubs_id`, `pistas_id`, `nivelPartida_id`, `inicio_alquiler`, `fin_alquiler`, `status`, `abierta_por`, `partida_fija`, `equipo`, `clase`, `mensaje`, `torneo`, `cartel`, `observaciones`, `created_at`, `updated_at`, `fija_id`,`entrenador_id`) " .
                "VALUES ('{$row["clubs_id"]}', '{$row["pistas_id"]}', '{$row["nivelPartida_id"]}', '{$row["inicio"]}', '{$row["fin"]}', '{$row["status"]}', '{$row["abierta_por"]}', '{$row["partida_fija"]}', '{$row["equipo"]}', '{$row["clase"]}', '{$row["mensaje"]}', '{$row["torneo"]}', '{$row["cartel"]}', '{$row["observaciones"]}', '{$row["created_at"]}', '{$row["updated_at"]}', '{$field25}', '{$field26}') ";

            // Ejecutar la consulta para insertar en la tabla partidas
            $resultInsertPartidas = $conn->query($sqlInsertPartidas);
            $last_id = $conn->insert_id;

            // Restablecer el valor AUTO_INCREMENT después de cada inserción
            $restableceIdPartida = "ALTER TABLE `partidas` AUTO_INCREMENT=1";
            $conn->query($restableceIdPartida);

            // Verificar si la inserción en la tabla partidas fue exitosa
            if ($resultInsertPartidas === true) {
                // Consulta SQL para insertar en la tabla partidas_user
                $sqlInsertPartidasUser = "INSERT IGNORE INTO `partidas_user` (`partida_id`, `club_id`, `pala_id`, `pala`, `pala2_id`, `pala2`, `pala3_id`, `pala3`, `pala4_id`, `pala4`) " .
                    "VALUES ('$last_id','{$row["clubs_id"]}','{$field17}','{$field18}','{$field19}','{$field20}','{$field21}','{$field22}','{$field23}','{$field24}')";

                // Ejecutar la consulta para insertar en la tabla partidas_user
                $resultInsertPartidasUser = $conn->query($sqlInsertPartidasUser);

                // Restablecer el valor AUTO_INCREMENT después de cada inserción
                $restableceIdPartidaUser = "ALTER TABLE `partidas_user` AUTO_INCREMENT=1";
                $conn->query($restableceIdPartidaUser);

                // Verificar si la inserción en la tabla partidas_user fue exitosa
                if ($resultInsertPartidasUser === true) {
                    // Escribir en el archivo CSV de partidas
                    fputcsv($partidas_csv, [date('m/d/Y H:i:s'), $last_id, $row["inicio"], $row["fin"], $row["pistas_id"], "Registros ingresados correctamente"]);

                    // Escribir en el archivo CSV de usuarios
                    fputcsv($usuarios_csv, [date('m/d/Y H:i:s'), $last_id, "Registros ingresados correctamente"]);
                } else {
                    // Escribir en el archivo CSV de partidas en caso de error
                    fputcsv($partidas_csv, [date('m/d/Y H:i:s'), $last_id, $row["inicio"], $row["fin"], $row["pistas_id"], "ERROR: No se pudo ejecutar $sqlInsertPartidasUser. " . $conn->error]);

                    // Escribir en el archivo CSV de usuarios en caso de error
                    fputcsv($usuarios_csv, [date('m/d/Y H:i:s'), $last_id, "ERROR: No se pudo ejecutar $sqlInsertPartidasUser. " . $conn->error]);
                }
            } else {
                // Escribir en el archivo CSV de partidas en caso de error
                fputcsv($partidas_csv, [date('m/d/Y H:i:s'), $last_id, $row["inicio"], $row["fin"], $row["pistas_id"], "ERROR: No se pudo ejecutar $sqlInsertPartidas. " . $conn->error]);

                // Escribir en el archivo CSV de usuarios en caso de error
                fputcsv($usuarios_csv, [date('m/d/Y H:i:s'), $last_id, "ERROR: No se pudo ejecutar $sqlInsertPartidas. " . $conn->error]);
            }
        }
    } else {
        echo "0 results";
    }
    // Cerrar archivos CSV
    fclose($partidas_csv);
    fclose($usuarios_csv);

}
    // Cerrar la conexión a la base de datos
	$conn->close();
?>