<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        
       	$this->call(rolesSeeder::class);
        $this->call(nivelesSeeder::class);
        $this->call(instalacionesSeeder::class);
        $this->call(pistasSeeder::class);
        $this->call(provinciasSeeder::class);
        $this->call(poblacionesSeeder::class);
        $this->call(poblaciones2Seeder::class);
        $this->call(poblaciones3Seeder::class);
        $this->call(poblaciones4Seeder::class);
        $this->call(poblaciones5Seeder::class);
        $this->call(poblaciones6Seeder::class);
        //$this->call(clubsSeeder::class);
        // $this->call(usersSeeder::class);
        // $this->call(PartidasSeeder::class);
        $this->call(administradorSeeder::class);
        $this->call(configWebSeeder::class);
    }
}
