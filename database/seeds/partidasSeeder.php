<?php

use Illuminate\Database\Seeder;
//use Faker\Generator as Faker;

//php artisan db:seed --class=PartidasSeeder
class PartidasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Partidas::class, 20)->create()->each(function(App\Partidas $partida){
            
            $user = App\User::where('id','=',$partida->user_id)->pluck('nick');

            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            
            if($partida->partida_fija == "SI"){
                $partida->partidaFija()->sync([
                    $partida->id => [
                        'clubs_id'        => $partida->clubs_id,
                        'user_id'         => $partida->user_id,
                        'pistas_id'       => $partida->pistas_id,
                        'inicio_alquiler' => $partida->inicio_alquiler,
                    ]
                
                ]);  
            };

            if($partida->status == "ABIERTA"){
                $partida->partidaJugador()->sync([
                    $partida->id => [
                        'partida_id' => $partida->id,
                        'user_id'    => $partida->user_id,
                        'pala'       => $user[0],
                        'pala2'      => $faker->firstName(),
                        'pala3'      => '',
                        'pala4'      => '',
                    ]
                
                ]);   
            }else{
                $partida->partidaJugador()->sync([
                    $partida->id => [
                        'partida_id' => $partida->id,
                        'user_id'    => $partida->user_id,
                        'pala'       => $user[0],
                        'pala2'      => $faker->firstName(),
                        'pala3'      => $faker->firstName(),
                        'pala4'      => $faker->firstName(),
                    ]
                
                ]);   
            }
            
        });
    }
}
