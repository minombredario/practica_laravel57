<?php

use Illuminate\Database\Seeder;

class pistasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i = 1; $i <= 20; $i++){
             App\Pistas::create( ['nombre' => 'Pista '. $i , 'slug' => 'pista-' . $i]);
        };
       
    }
}
