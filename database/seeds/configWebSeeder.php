<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class configWebSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config_web')->insert([
            [
                'club_id' => 1,
                'cards' => 1,
                'paralax' => 1,
                'imgparalax' => '../image/club/1/paralax/fondo4.jpg',
                'galeria' => 1,
                'jumbotron' => 1,
                'imageslider' => 1,
                'patrocinadores' => 1,
                'brsocial' => 1,
                'columnas' => 1,
                'welcome' => 1,
                'noticias' => 1
            ]
        ]);
    }
}
