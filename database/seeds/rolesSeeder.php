<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class rolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['nombre' => 'ADMIN', 'slug' => 'admin'],
            ['nombre' => 'PLAYER', 'slug' => 'player'],
            ['nombre' => 'CLUB', 'slug' => 'club'],
            ['nombre' => 'INVITADO', 'slug' => 'invitado'],
        ]);
    }
}