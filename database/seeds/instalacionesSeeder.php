<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class instalacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instalaciones')->insert([
            ['nombre' => 'Cafetería', 'slug' => 'cafeteria'],
            ['nombre' => 'Zona infantil', 'slug' => 'zona-infantil'],
            ['nombre' => 'Comedor', 'slug' => 'comedor'],
            ['nombre' => 'Pistas Indoor', 'slug' => 'pistas-indoor'],
            ['nombre' => 'Pistas Outdoor', 'slug' => 'pistas-outdoor'],
            ['nombre' => 'Zona de descanso', 'slug' => 'zona-de-descanso'],
            ['nombre' => 'Terraza', 'slug' => 'terraza'],
            ['nombre' => 'Gimnasio', 'slug' => 'gimansio'],
            ['nombre' => 'Tienda', 'slug' => 'tienda'],
        ]);
    }
}
