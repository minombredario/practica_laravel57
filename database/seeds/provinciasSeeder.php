<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class provinciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('provincias')->insert([
            ['id' => '1', 'nombre' =>  'Álava', 'slug' =>  'alava', 'nombre_corto' =>  'ALV'],
            ['id' => '2', 'nombre' =>  'Albacete', 'slug' =>  'albacete', 'nombre_corto' =>  'ABC'],
            ['id' => '3', 'nombre' =>  'Alicante', 'slug' =>  'alicante', 'nombre_corto' =>  'ALA'],
            ['id' => '4', 'nombre' =>  'Almería', 'slug' =>  'almeria', 'nombre_corto' =>  'ALM'],
            ['id' => '5', 'nombre' =>  'Ávila', 'slug' =>  'avila', 'nombre_corto' =>  'AVL'],
            ['id' => '6', 'nombre' =>  'Badajoz', 'slug' =>  'badajoz', 'nombre_corto' =>  'BDJ'],
            ['id' => '7', 'nombre' =>  'Baleares', 'slug' =>  'baleares', 'nombre_corto' =>  'BAL'],
            ['id' => '8', 'nombre' =>  'Barcelona', 'slug' =>  'barcelona', 'nombre_corto' =>  'BCN'],
            ['id' => '9', 'nombre' =>  'Burgos', 'slug' =>  'burgos', 'nombre_corto' =>  'BRG'],
            ['id' => '10', 'nombre' =>  'Cáceres', 'slug' =>  'caceres', 'nombre_corto' =>  'CAC'],
            ['id' => '11', 'nombre' =>  'Cádiz', 'slug' =>  'cadiz', 'nombre_corto' =>  'CDZ'],
            ['id' => '12', 'nombre' =>  'Castellón', 'slug' =>  'castellon', 'nombre_corto' =>  'CAS'],
            ['id' => '13', 'nombre' =>  'Ciudad Real', 'slug' =>  'ciudad-real', 'nombre_corto' =>  'CRE'],
            ['id' => '14', 'nombre' =>  'Córdoba', 'slug' =>  'cordoba', 'nombre_corto' =>  'CBA'],
            ['id' => '15', 'nombre' =>  'A Coruña', 'slug' =>  'coruna', 'nombre_corto' =>  'LCO'],
            ['id' => '16', 'nombre' =>  'Cuenca', 'slug' =>  'cuenca', 'nombre_corto' =>  'CNC'],
            ['id' => '17', 'nombre' =>  'Girona', 'slug' =>  'girona', 'nombre_corto' =>  'GIR'],
            ['id' => '18', 'nombre' =>  'Granada', 'slug' =>  'granada', 'nombre_corto' =>  'GND'],
            ['id' => '19', 'nombre' =>  'Guadalajara', 'slug' =>  'guadalajara', 'nombre_corto' =>  'GLJ'],
            ['id' => '20', 'nombre' =>  'Guipúzcoa', 'slug' =>  'guipuzcoa', 'nombre_corto' =>  'GPZ'],
            ['id' => '21', 'nombre' =>  'Huelva', 'slug' =>  'huelva', 'nombre_corto' =>  'HLV'],
            ['id' => '22', 'nombre' =>  'Huesca', 'slug' =>  'huesca', 'nombre_corto' =>  'HSC'],
            ['id' => '23', 'nombre' =>  'Jaén', 'slug' =>  'jaen', 'nombre_corto' =>  'JAE'],
            ['id' => '24', 'nombre' =>  'León', 'slug' =>  'leon', 'nombre_corto' =>  'LEO'],
            ['id' => '25', 'nombre' =>  'Lleida', 'slug' =>  'lleida', 'nombre_corto' =>  'LLE'],
            ['id' => '26', 'nombre' =>  'La Rioja', 'slug' =>  'rioja', 'nombre_corto' =>  'LRJ'],
            ['id' => '27', 'nombre' =>  'Lugo', 'slug' =>  'lugo', 'nombre_corto' =>  'LUG'],
            ['id' => '28', 'nombre' =>  'Madrid', 'slug' =>  'madrid', 'nombre_corto' =>  'MAD'],
            ['id' => '29', 'nombre' =>  'Málaga', 'slug' =>  'malaga', 'nombre_corto' =>  'MAL'],
            ['id' => '30', 'nombre' =>  'Murcia', 'slug' =>  'murcia', 'nombre_corto' =>  'MUR'],
            ['id' => '31', 'nombre' =>  'Navarra', 'slug' =>  'navarra', 'nombre_corto' =>  'NVR'],
            ['id' => '32', 'nombre' =>  'Ourense', 'slug' =>  'ourense', 'nombre_corto' =>  'OUR'],
            ['id' => '33', 'nombre' =>  'Asturias', 'slug' =>  'asturias', 'nombre_corto' =>  'AST'],
            ['id' => '34', 'nombre' =>  'Palencia', 'slug' =>  'palencia', 'nombre_corto' =>  'PAL'],
            ['id' => '35', 'nombre' =>  'Las Palmas', 'slug' =>  'palmas', 'nombre_corto' =>  'LPA'],
            ['id' => '36', 'nombre' =>  'Pontevedra', 'slug' =>  'pontevedra', 'nombre_corto' =>  'PNV'],
            ['id' => '37', 'nombre' =>  'Salamanca', 'slug' =>  'salamanca', 'nombre_corto' =>  'SAL'],
            ['id' => '38', 'nombre' =>  'S.C. Tenerife', 'slug' =>  'tenerife', 'nombre_corto' =>  'SCT'],
            ['id' => '39', 'nombre' =>  'Cantabria', 'slug' =>  'cantabria', 'nombre_corto' =>  'CTB'],
            ['id' => '40', 'nombre' =>  'Segovia', 'slug' =>  'segovia', 'nombre_corto' =>  'SGV'],
            ['id' => '41', 'nombre' =>  'Sevilla', 'slug' =>  'sevilla', 'nombre_corto' =>  'SVL'],
            ['id' => '42', 'nombre' =>  'Soria', 'slug' =>  'soria', 'nombre_corto' =>  'SOR'],
            ['id' => '43', 'nombre' =>  'Tarragona', 'slug' =>  'tarragona', 'nombre_corto' =>  'TRN'],
            ['id' => '44', 'nombre' =>  'Teruel', 'slug' =>  'teruel', 'nombre_corto' =>  'TER'],
            ['id' => '45', 'nombre' =>  'Toledo', 'slug' =>  'toledo', 'nombre_corto' =>  'TOL'],
            ['id' => '46', 'nombre' =>  'Valencia', 'slug' =>  'valencia', 'nombre_corto' =>  'VAL'],
            ['id' => '47', 'nombre' =>  'Valladolid', 'slug' =>  'valladolid', 'nombre_corto' =>  'VLL'],
            ['id' => '48', 'nombre' =>  'Vizcaya', 'slug' =>  'vizcaya', 'nombre_corto' =>  'VZC'],
            ['id' => '49', 'nombre' =>  'Zamora', 'slug' =>  'zamora', 'nombre_corto' =>  'ZAM'],
            ['id' => '50', 'nombre' =>  'Zaragoza', 'slug' =>  'zaragoza', 'nombre_corto' =>  'ZAR'],
            ['id' => '51', 'nombre' =>  'Ceuta', 'slug' =>  'ceuta', 'nombre_corto' =>  'CEU'],
            ['id' => '52', 'nombre' =>  'Melilla', 'slug' =>  'melilla', 'nombre_corto' =>  'MEL'],


        ]);
        
    }
}
