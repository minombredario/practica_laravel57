<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class nivelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('niveles')->insert([
            ['nombre' => '1.00', 'descripcion' => 'Acaba de empezar a jugar a pádel'],
            ['nombre' => '1.25', 'descripcion' => 'Experiencia limitada'],
            ['nombre' => '1.50', 'descripcion' => 'Sigue intentando mantener las pelotas en juego.'],
            ['nombre' => '1.75', 'descripcion' => ''],
            ['nombre' => '2.00', 'descripcion' => 'DERECHA: Gesto (swing) incompleto, falta de control direccional, velocidad de bola lenta. | REVES: Evita el revés, golpeo errático, problemas de empuñadura, gesto incompleto. | SERVICIO/RESTO: Gesto incompleto, habitualmente comete dobles faltas, bote de la bola inconsistente, resto con muchos problemas. | VOLEA: Evita subir a la red, evita la volea de revés, mal posicionamiento de pies. | REBOTES: No consigue devolver ningún rebote. | ESTILO DE JUEGO: Familiar con las posiciones básicas aunque se posiciona frecuentemente de manera incorrecta.'],
            ['nombre' => '2.25', 'descripcion' => ''],
            ['nombre' => '2.50', 'descripcion' => 'DERECHA: En desarrollo, velocidad de bola moderada. | REVES: Problemas en preparación y empuñadura, a menudo prefiere el golpeo de derecha al de revés. | SERVICIO/RESTO: Intento de realizar el gesto completo, velocidad de bola en servicio lenta, bote de bola inconsistente, devuelve servicios lentos.| VOLEA: Incómodo en la red, especialmente en el revés. Utiliza frecuentemente la cara del drive en las voleas de revés. | REBOTES: Se intenta posicionar para los rebotes aunque sólo golpea la bola de forma ocasional. | GOLPES ESPECIALES: Hace globos intencionados pero con poco control, empala la bola ocasionalmente en golpes altos (smash).| ESTILO DE JUEGO: Puede pelotear con una velocidad de bola lenta, débil cobertura de su espacio en la pista, permanece en la posición inicial del juego.'],
            ['nombre' => '2.75', 'descripcion' => ''],
            ['nombre' => '3.00', 'descripcion' => 'DERECHA: Relativamente buena consistencia y moderada variedad de golpes, buen control direccional, desarrollando gama de golpes (cortado, plano, liftado). | REVES: Preparado frecuentemente para golpear con relativa consistencia. | SERVICIO/RESTO: Desarrollando el gesto de servicio, poca consistencia cuando intenta golpear con potencia, segundo servicio es considerablemente peor que el primero. Resta con relativa consistencia. | VOLEA: Volea de derecha de forma consistente, inconsistente en la volea de revés, problemas con las bolas a los pies y en golpes abiertos. | REBOTES: Se posiciona correctamente en golpes lentos, consigue empalar. No se desplaza hacia delante en bolas fuertes. | GOLPES ESPECIALES: Hace globos de forma moderadamente consistente. Es hora de Mejorar el GLOBO incluso tras una rebote de pared. | ESTILO DE JUEGO: Consistente con velocidad de bola media, poca consistencia en posicionamiento con el compañero, a veces uno arriba y otro abajo, sube a la red cuando lo dicta el juego pero es débil en la ejecución.'],
            ['nombre' => '3.25', 'descripcion' => ''],
            ['nombre' => '3.50', 'descripcion' => 'DERECHA: Buena consistencia y moderada variedad de golpes, buen control direccional, desarrollando gama de golpes (cortado, plano, liftado). | REVÉS: Golpeo con control direccional en algunos golpes, dificultad de devolución en bolas fuertes o altas, dificultad en la defensa de revés. | SERVICIO/RESTO: Comienza a servir con control y algo de potencia, desarrollando el saque cortado, puede restar consistentemente con control direccional con velocidad de bola moderada. | VOLEA: Más agresivo en el juego de red, alguna habilidad en los golpes laterales, bastante buen posicionamiento de pies, tiene control direccional en las voleas de derecha, control en las voleas de revés, pero con muy bajo poder ofensivo. | REBOTES: Se posiciona correctamente en golpes de derecha. Consigue defender de manera consistente de derecha y velocidad de bola moderada. Defiende de revés con dificultad las bolas rápidas. Desarrollando la bajadas de pared. |GOLPES ESPECIALES: consistente en golpes altos, desarrollando bandeja, segundas voleas y golpes de aproximación. Devuelve la mayor parte de los segundos servicios. | ESTILO DE JUEGO: Consistencia y control direccional con velocidades de bola media, posicionamiento con el compañero medianamente consistente (desarrollando el trabajo en equipo), busca la oportunidad de ganar la red.'],
            ['nombre' => '3.75', 'descripcion' => ''],
            ['nombre' => '4.00', 'descripcion' => 'DERECHA: Fiable, golpea con bastante control, intenta golpear con control en golpes defensivos difíciles. | REVES: Puede direccionar la bola con consistencia y velocidades de bola moderadas, desarrollando variedad de golpes. | SERVICIO/RESTO: Coloca primeros y segundos, potencia y colocación frecuente con el primero, usa bolas cortadas en el servicio, fiable en el resto, resta con profundidad y control. | VOLEA: Profundidad y control en la volea de derecha, direcciona la volea de revés, pero con falta de profundidad y agresividad. Desarrollando las voleas bajas. | REBOTES: Devuelve consistentemente los rebotes de derecha, se posiciona correctamente en los rebotes de revés. Bajada de pared consistente y con potencia con la derecha, al revés suele jugar globo. | GOLPES ESPECIALES: Golpea por alto con consistencia, consigue volear a golpes agresivos, comienza a definir puntos con la volea y el smash, busca y dirige la volea a la zona débil de los rivales, defiende con globos de forma consistente. Comienza a desarrollar la bandeja. | ESTILO DE JUEGO: Golpeo fiable y con control con velocidades de bola medio-alta. Juego en equipo, pérdida del punto en peloteos por falta de paciencia.'],
            ['nombre' => '4.25', 'descripcion' => ''],
            ['nombre' => '4.50', 'descripcion' => 'DERECHA: Muy fiable, usa velocidad, control y variedad de golpes con profundidad. Ofensivo en la mayoría de golpes. | REVES: Puede controlar dirección y profundidad pero sufre defendiendo golpes difíciles, puede golpear con relativa potencia. | SERVICIO/RESTO: Servicio con colocación y control, con pocas doble faltas, usa potencia y bolas cortadas, segundos con buena profundidad y colocación, restos agresivos y con control. | VOLEA: Buen posicionamiento de pies, puede controlar peloteos en las voleas, tiene potencia, control y profundidad en las voleas de derechas, errores mas comunes cuando golpea con potencia. | REBOTES: Buena bajada de pared de derecha, de revés consigue profundidad y colocación con velocidad de bola media. | GOLPES ESPECIALES: Golpes de aproximación con control y profundidad, consistencia en voleas y golpes altos. Consigue remates definitivos, llega a sacar la bola por 4 en algunas ocasiones, consigue en ocasiones dejadas de volea. | ESTILO DE JUEGO: Golpeo con relativa potencia, buena cobertura en la pista, empieza a variar el juego en función del oponente, juego agresivo en la red, buena anticipación.'],
            ['nombre' => '4.75', 'descripcion' => ''],
            ['nombre' => '5.00', 'descripcion' => 'DERECHA: Golpeo fuerte con control, profundidad y variedad de golpes, usa la derechas para preparar la subida a la red. Consistente en los globos profundos. | REVES: Puede usar el revés en golpes agresivos con bastante buena consistencia, buen control en dirección y profundidad en la mayoría de los golpes, Bastante consistente en globos profundos y defensa. | SERVICIO/RESTO: Coloca el servicio con efectividad y buscando el punto débil del rival y ganar la red de forma rápida, buena variedad de servicios, profundidad, colocación en segundos para forzar restos poco potentes, resta con control servicios difíciles. | VOLEA: Puede golpear a la mayoría de las voleas con profundidad y potencia, juega voleas difíciles con profundidad, buscando el punto débil del rival. | REBOTES: Buena defensa de rebotes, incluso con bolas fuertes. Consigue puntos ganadores con rebotes fuertes de pared. Bajadas de pared buenas tanto de derecha como de revés. Aprende aquí cómo ATACAR desde el Fondo de Pista con una BAJADA de PARED. | GOLPES ESPECIALES: Golpea con fuerza y alto grado de efectividad, buenos globos defensivos y ofensivos, golpea por alto en cualquier posición de la pista con control, juega bandejas con consistencia, consigue golpes ganadores con smash fuerte. Consigue sacar la bola por 3 en ocasiones. | ESTILO DE JUEGO: Varía el juego según el rival, sólido trabajo en equipo, consigue leer el partido y encuentra el punto débil del rival, es menos consistente mental y físicamente que el jugador 5.5'],
            ['nombre' => '5.25', 'descripcion' => ''],
            ['nombre' => '5.50', 'descripcion' => 'Juega golpes fiables en situaciones comprometidas. | Ha desarrollado una buena anticipación. | Lee el partido con facilidad buscando los puntos débiles de los rivales. | Primeros y segundos servicios son golpes profundos y colocados. | Ha desarrollado fuerza y consistencia como su mayor arma. | Varía la estrategia y el estilo de juego en una situación comprometida.'],
            ['nombre' => '5.75', 'descripcion' => ''],
            ['nombre' => '6.00', 'descripcion' => 'Tiene entrenamientos intensivos para torneos regionales y nacionales. Tiene ranking nacional.'],
            ['nombre' => '6.25', 'descripcion' => ''],
            ['nombre' => '6.50', 'descripcion' => 'Tiene potencial para llegar a ser jugador 7.0. Juega torneos nacionales de forma continuada.'],
            ['nombre' => '6.75', 'descripcion' => ''],
            ['nombre' => '7.00', 'descripcion' => 'Es el jugador profesional de pádel, compite en torneos open y su mayor fuente de ingresos son los premios de los torneos y sponsors.']
        ]);
    }
}

/*insert into
   `niveles` (`descripcion`, `nombre`) values (Acaba de empezar a jugar al pádel, 1), (Experiencia limitada, 1.25), (
  Sigue intentando mantener las pelotas en juego., 1.50), (, 1.75), (DERECHA: Gesto (swing) incompleto, falta de cont
  rol direccional, velocidad de bola lenta. | REVES: Evita el revés, golpeo errático, problemas de empuñadura, gesto
  incompleto. | SERVICIO/RESTO: Gesto incompleto, habitualmente comete dobles faltas, bote de la bola inconsistente,
  resto con muchos problemas. | VOLEA: Evita subir a la red, evita la volea de revés, mal posicionamiento de pies. |
  REBOTES: No consigue devolver ningún rebote. | ESTILO DE JUEGO: Familiar con las posiciones básicas, aunque se posi
  ciona frecuentemente de manera incorrecta., 2.00), (, 2.25), (DERECHA: En desarrollo, velocidad de bola moderada. |
   REVES: Problemas en preparación y empuñadura, a menudo prefiere el golpeo de derecha al de revés. | SERVICIO/RESTO
  : Intento de realizar el gesto completo, velocidad de bola en servicio lenta, bote de bola inconsistente, devuelve
  servicios lentos.| VOLEA: Incomodo en la red especialmente en el revés, utiliza frecuentemente la cara del drive en
   las voleas de revés. | REBOTES: Se intenta posicionar para los rebotes aunque solo golpea la bola de forma ocasion
  al. | GOLPES ESPECIALES: Hace globos intencionados pero con poco control, empala la bola ocasionalmente en golpes a
  ltos (smash).| ESTILO DE JUEGO: Puede pelotear con una velocidad de bola lenta, débil cobertura de su espacio en la
   pista, permanece en la posición inicial del juego., 2.50), (, 2.75), (DERECHA: Relativamente buena consistencia y
  moderada variedad de golpes, buen control direccional, desarrollando gama de golpes (cortado, plano, liftado). | RE
  VES: Preparado frecuentemente para golpear con relativa consistencia. | SERVICIO/RESTO: Desarrollando el gesto de s
  ervicio, poca consistencia cuando intenta golpear con potencia, segundo servicio es considerablemente peor que el p
  rimero. Resta con relativa consistencia. | VOLEA: Volea de derecha de forma consistente, inconsistente en la volea
  de revés, problemas con las bolas a los pies y en golpes abiertos. | REBOTES: Se posiciona correctamente en golpes
  lentos, consigue empalar. No se desplaza hacia delante en bolas fuertes. | GOLPES ESPECIALES: Hace globos de forma
  moderadamente consistente. Es hora de Mejorar el GLOBO incluso tras una rebote de pared. | ESTILO DE JUEGO: Consist
  ente con velocidad de bola media, poca consistencia en posicionamiento con el compañero, a veces uno arriba y otro
  abajo, sube a la red cuando lo dicta el juego pero es débil en la ejecución., 3.00), (, 3.25), (DERECHA: Buena cons
  istencia y moderada variedad de golpes, buen control direccional, desarrollando gama de golpes (cortado, plano, lif
  tado). | REVÉS: Golpeo con control direccional en algunos golpes, dificultad de devolución en bolas fuertes o altas
  , dificultad en la defensa de revés. | SERVICIO/RESTO: Comienza a servir con control y algo de potencia, desarrolla
  ndo el saque cortado, puede restar consistentemente con control direccional con velocidad de bola moderada. | VOLEA
  : Mas agresivo en el juego de red, alguna habilidad en los golpes laterales, bastante buen posicionamiento de pies,
   tiene control direccional en las voleas de derecha, control en las voleas de revés, pero con muy bajo poder ofensi
  vo. | REBOTES: Se posiciona correctamente en golpes de derecha. Consigue defender de manera consistente de derecha
  y velocidad de bola moderada. Defiende de revés con dificultad las bolas rápidas. Desarrollando la bajadas de pared
  . |GOLPES ESPECIALES: consistente en golpes altos, desarrollando bandeja, segundas voleas y golpes de aproximación.
   Devuelve la mayor parte de los segundos servicios. | ESTILO DE JUEGO: Consistencia y control direccional con veloc
  idades de bola media, posicionamiento con el compañero medianamente consistente (desarrollando el trabajo en equipo
  ), busca la oportunidad de ganar la red., 3.50), (, 3.75), (DERECHA: Fiable, golpea con bastante control, intenta g
  olpear con control en golpes defensivos difíciles. | REVES: Puede direccionar la bola con consistencia y velocidade
  s de bola moderadas, desarrollando variedad de golpes. | SERVICIO/RESTO: Coloca primeros y segundos, potencia y col
  ocación frecuente con el primero, usa bolas cortadas en el servicio, fiable en el resto, resta con profundidad y co
  ntrol. | VOLEA: Profundidad y control en la volea de derecha, direcciona la volea de revés, pero con falta de profu
  ndidad y agresividad. Desarrollando las voleas bajas. | REBOTES: Devuelve consistentemente los rebotes de derecha,
  se posiciona correctamente en los rebotes de revés. Bajada de pared consistente y con potencia con la derecha, al r
  evés suele jugar globo. | GOLPES ESPECIALES: Golpea por alto con consistencia, consigue volear a golpes agresivos,
  comienza a definir puntos con la volea y el smash, busca y dirige la volea a la zona débil de los rivales, defiende
   con globos de forma consistente. Comienza a desarrollar la bandeja. | ESTILO DE JUEGO: Golpeo fiable y con control
   con velocidades de bola medio-alta. Juego en equipo, pérdida del punto en peloteos por falta de paciencia., 4.00),
   (, 4.25), (DERECHA: Muy fiable, usa velocidad, control y variedad de golpes con profundidad. Ofensivo en la mayorí
  a de golpes. | REVES: Puede controlar dirección y profundidad pero sufre defendiendo golpes difíciles, puede golpea
  r con relativa potencia. | SERVICIO/RESTO: Servicio con colocación y control, con pocas doble faltas, usa potencia
  y bolas cortadas, segundos con buena profundidad y colocación, restos agresivos y con control. | VOLEA: Buen posici
  onamiento de pies, puede controlar peloteos en las voleas, tiene potencia, control y profundidad en las voleas de d
  erechas, errores mas comunes cuando golpea con potencia. | REBOTES: Buena bajada de pared de derecha, de revés cons
  igue profundidad y colocación con velocidad de bola media. | GOLPES ESPECIALES: Golpes de aproximación con control
  y profundidad, consistencia en voleas y golpes altos. Consigue remates definitivos, llega a sacar la bola por 4 en
  algunas ocasiones, consigue en ocasiones dejadas de volea. | ESTILO DE JUEGO: Golpeo con relativa potencia, buena c
  obertura en la pista, empieza a variar el juego en función del oponente, juego agresivo en la red, buena anticipaci
  ón., 4.50), (, 4.75), (DERECHA: Golpeo fuerte con control, profundidad y variedad de golpes, usa la derechas para p
  reparar la subida a la red. Consistente en los globos profundos. | REVES: Puede usar el revés en golpes agresivos c
  on bastante buena consistencia, buen control en dirección y profundidad en la mayoría de los golpes, Bastante consi
  stente en globos profundos y defensa. | SERVICIO/RESTO: Coloca el servicio con efectividad y buscando el punto débi
  l del rival y ganar la red de forma rápida, buena variedad de servicios, profundidad, colocación en segundos para f
  orzar restos poco potentes, resta con control servicios difíciles. | VOLEA: Puede golpear a la mayoría de las volea
  s con profundidad y potencia, juega voleas difíciles con profundidad, buscando el punto débil del rival. | REBOTES:
   Buena defensa de rebotes, incluso con bolas fuertes. Consigue puntos ganadores con rebotes fuertes de pared. Bajad
  as de pared buenas tanto de derecha como de revés. Aprende aquí cómo ATACAR desde el Fondo de Pista con una BAJADA
  de PARED. | GOLPES ESPECIALES: Golpea con fuerza y alto grado de efectividad, buenos globos defensivos y ofensivos,
   golpea por alto en cualquier posición de la pista con control, juega bandejas con consistencia, consigue golpes ga
  nadores con smash fuerte. Consigue sacar la bola por 3 en ocasiones. | ESTILO DE JUEGO: Varia el juego segun el riv
  al, solido trabajo en equipo, consigue leer el partido y encuentra el punto débil del rival, es menos consistente m
  ental y físicamente que el jugador 5.5, 5.00), (, 5.25), (Juega golpes fiables en situaciones comprometidas. | Ha d
  esarrollado una buena anticipación. | Lee el partido con facilidad buscando los puntos débiles de los rivales. | Pr
  imeros y segundos servicios son golpes profundos y colocados. | Ha desarrollado fuerza y consistencia como su mayor
   arma. | Varía la estrategia y el estilo de juego en una situaciones comprometidas., 5.50), (, 5.75), (Tiene entren
  amientos intensivos para torneos regionales y nacionales, y tiene ranking nacional., 6.00), (, 6.25), (Tiene potenc
  ial para llegar a ser jugador 7.0 juega torneos nacionales de forma continuada., 6.50), (, 6.75), (Es el jugador pr
  ofesional de pádel, compite en torneos open y su mayor fuente de ingresos son los premios de los torneos y sponsors
  ., 7.00)*/