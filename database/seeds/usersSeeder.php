<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        
        factory(App\User::class, 300)->create()->each(function(App\User $user){
            
            $user->clubs()->attach([
                $user->clubs_id => [
                    'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                    'user_email' => $user->email, //asigno al id del club el email del usuario inertandolo a traves de su id.
                ]
                
            ]);
            /*$user->clubs()->attach([
        		rand(1,5),
                rand(6,14),
                rand(15,20)
        	]);*/
        });

        App\User::create([
        	'nombre' 			=> 'Dario',
	        'apellidos' 		=> 'Navarro Andrés',
	        'telefono' 			=> '646136523',
	        'poblacion_id' 		=> '6953',
	        'email' 			=> 'minombredario@gmail.com',
	        'password' 			=> bcrypt('123'),
	        'nick' 				=> 'morfeo',
	        'clubs_id' 			=> '1',
	        'posicion' 			=> 'REVÉS',
	        'desplazamiento'    => null,
            //'franja_id' 		=> '1',
	        'nivel_id' 			=> '2',
	        'rol_id' 			=> '1',
	        'notificaciones' 	=> 'SI',
	        'comentarios' 		=> null,
	        'remember_token' 	=> Str::random(10)

        	]);
    }
}