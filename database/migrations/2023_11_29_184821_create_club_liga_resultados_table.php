<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubLigaResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ligas_resultados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liga_id')->unsigned();
            $table->integer('club_id')->unsigned();
            $table->integer('jornada');
            $table->integer('a');
            $table->integer('b');
            $table->string('juego1', 45)->nullable()->default('0-0');
            $table->string('juego2', 45)->nullable()->default('0-0');
            $table->string('juego3', 45)->nullable()->default('0-0');
            $table->integer('juego11')->default(0);
            $table->integer('juego12')->default(0);
            $table->integer('juego21')->default(0);
            $table->integer('juego22')->default(0);
            $table->integer('juego31')->default(0);
            $table->integer('juego32')->default(0);
            $table->integer('ganador')->default(0);

            $table->timestamps();

            $table->foreign('club_id')->references('id')->on('clubs')->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('liga_id')->references('id')->on('ligas')->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ligas_resultados');
    }
}
