<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 128);
            $table->string('apellidos', 150)->nullable();
            $table->enum('sexo', ['HOMBRE', 'MUJER'])->default('HOMBRE');
            $table->integer('poblacion_id')->unsigned();
            $table->integer('telefono')->nullable();
            $table->string('email', 128);
            $table->string('password');
            $table->string('nick', 60);
            $table->string('avatar', 255)->nullable();
            $table->integer('clubs_id')->unsigned();
            $table->enum('posicion',['REVÉS','DERECHA','INDIFERENTE'])->default('INDIFERENTE');
            $table->integer('desplazamiento')->unsigned()->nullable();
            $table->string('disponibilidad',255)->nullable();
            $table->integer('nivel_id')->unsigned()->default('1');
            $table->integer('nivel_real_id')->unsigned()->default('1');
            $table->integer('rol_id') ->unsigned()->default('2');
            $table->enum('notificaciones', ['SI', 'NO'])->default('SI');
            $table->string('comentarios',255)->nullable();
            $table->boolean('online')->default(0);
            $table->integer('current_login_time')->nullable();
            $table->integer('last_logged_at')->nullable();
            
            $table->rememberToken();
            $table->timestamps();

            $table->index(['nick', 'clubs_id', 'email']);
            $table->unique(['nick', 'clubs_id', 'email']);         
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
