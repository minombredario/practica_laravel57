<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWelcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('welcome', function (Blueprint $table) { 
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('titulo',128);
            $table->text('contenido');
            $table->enum('status',  ['BORRADOR','PUBLICADO'])->default('BORRADOR');
            $table->boolean('visible')->default(true);

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id', 'titulo']);
            $table->unique(['club_id', 'titulo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('welcome');
        Schema::enableForeignKeyConstraints();
    }
}
