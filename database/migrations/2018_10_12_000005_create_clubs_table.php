<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',128)->unique();
            $table->string('slug', 128);
            $table->string('cif',9);
            $table->string('web',128)->unique();
            $table->string('logo',250);
            $table->integer('telefono')->nullable();
            $table->integer('movil')->nullable();
            $table->string('email', 128)->unique();
            $table->string('email_web', 128)->nullable();
            $table->string('password');
            $table->string('responsable',128);
            $table->integer('pistas')->unsigned();
            $table->string('horario',255);
            $table->integer('rol_id') ->unsigned()->default('3');
            $table->string('direccion');
            $table->integer('poblacion_id')->unsigned();
            $table->decimal('latitud', 9, 6)->nullable();
            $table->decimal('longitud', 9, 6)->nullable();
            $table->text('nosotros');
            $table->boolean('online')->default(0);
            $table->integer('current_login_time')->nullable();
            $table->integer('last_logged_at')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clubs');
        Schema::enableForeignKeyConstraints();
    }
}
