<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubHorarioPistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario_pistas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->integer('pista_id')->unsigned();
            $table->string('lunes', 45);
            $table->string('martes', 45);
            $table->string('miercoles', 45);
            $table->string('jueves', 45);
            $table->string('viernes', 45);
            $table->string('sabado', 45);
            $table->string('domingo', 45);

            $table->index(['club_id', 'pista_id']);
            $table->unique(['club_id', 'pista_id']);

            $table->foreign('club_id')->references('id')->on('clubs');

            $table->foreign('pista_id')->references('id')->on('pistas')
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('horarios');
        Schema::enableForeignKeyConstraints();
    }
}
