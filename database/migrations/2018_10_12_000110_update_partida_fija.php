<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePartidaFija extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partidas_fijas', function (Blueprint $table) {
            //Relacion

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('partida_id')->references('id')->on('partidas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
