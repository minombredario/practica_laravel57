<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListaEsperasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_espera', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubs_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('partida_id')->unsigned();
            $table->integer('torneo_id')->unsigned();
            $table->string('comentario', 180);

            $table->foreign('clubs_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('partida_id')->references('id')->on('partidas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('torneo_id')->references('id')->on('torneos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['clubs_id', 'user_id', 'partida_id']);
            $table->unique(['clubs_id', 'user_id', 'partida_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('lista_espera');
        Schema::enableForeignKeyConstraints();
    }
}
