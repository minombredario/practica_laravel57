<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigWebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'config_web',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('club_id')->unsigned();
                $table->boolean('cards')->default(TRUE);
                $table->boolean('paralax')->default(TRUE);
                $table->string('imgparalax', 255)->nullable();
                $table->boolean('galeria')->default(TRUE);
                $table->boolean('jumbotron')->default(TRUE);
                $table->boolean('imageslider')->default(TRUE);
                $table->boolean('bqsocial')->default(TRUE);
                $table->boolean('patrocinadores')->default(TRUE);
                $table->boolean('brsocial')->default(TRUE);
                $table->boolean('columnas')->default(TRUE);
                $table->boolean('welcome')->default(TRUE);
                $table->boolean('noticias')->default(TRUE);

                $table->timestamps();

                $table->foreign('club_id')->references('id')->on('clubs')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
