<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administradores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 128);
            $table->string('email', 128);
            $table->integer('rol_id')->unsigned();
            $table->string('password'); 

            $table->rememberToken();
           
             //Relacion

            $table->foreign('rol_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['nombre', 'email']);
            $table->unique(['nombre', 'email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('administradores');
       // Schema::enableForeignKeyConstraints();
    }
}
