<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubLigaClasificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ligas_clasificacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liga_id')->unsigned();
            $table->integer('club_id')->unsigned();
            $table->string('nombre', 125);
            $table->integer('pj')->default(0);
            $table->integer('pg')->default(0);
            $table->integer('pp')->default(0);
            $table->integer('sg')->default(0);
            $table->integer('sp')->default(0);
            $table->integer('jg')->default(0);
            $table->integer('jp')->default(0);
            $table->integer('pts')->default(0);

            $table->timestamps();

            $table->foreign('club_id')->references('id')->on('clubs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ligas_clasificacion');
    }
}
