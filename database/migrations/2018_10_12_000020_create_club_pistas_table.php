<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubPistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs_pistas', function (Blueprint $table) {
            $table->increments('pistas_id');
            $table->integer('clubs_id')->unsigned();
            
            $table->unique(['pistas_id', 'clubs_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clubs_pistas');
        Schema::enableForeignKeyConstraints();
    }
}
