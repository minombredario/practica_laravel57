<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('facebook',125);
            $table->string('twitter',125);
            $table->string('google',125);
            $table->string('instagram',125);
            $table->string('youtube',125);
            $table->enum('status',  ['BORRADOR','PUBLICADO'])->default('BORRADOR');
            $table->boolean('visible')->default(true);
            

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id']);
            $table->unique(['club_id']);
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('social');
        Schema::enableForeignKeyConstraints();
    }
}
