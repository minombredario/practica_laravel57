<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrocinadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrocinadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id')->unsigned();
            $table->string('nombre',125);
            $table->string('slug',125);
            $table->string('web',100);
            $table->string('imagen',255)->nullable();
            $table->enum('status',  ['BORRADOR','PUBLICADO'])->default('BORRADOR');
            $table->boolean('visible')->default(true);
            $table->integer('patrocina')->unsigned()->nullable();
            

            //Relacion

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['club_id', 'nombre']);
            $table->unique(['club_id', 'nombre']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('patrocinadores');
        Schema::enableForeignKeyConstraints();
    }
}
