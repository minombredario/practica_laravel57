<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubs_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('user_email', 128);
            $table->string('password');
            
            $table->index(['clubs_id', 'user_id', 'user_email']);
            $table->unique(['clubs_id', 'user_id', 'user_email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clubs_user');
        Schema::enableForeignKeyConstraints();
    }
}
