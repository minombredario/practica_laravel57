<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClubsPartidasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partidas', function (Blueprint $table) {

            $table->foreign('clubs_id')->references('id')->on('clubs')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('pistas_id')->references('id')->on('pistas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            /*$table->foreign('partida_id')->references('id')->on('partidas_user')
                ->onDelete('cascade')
                ->onUpdate('cascade');*/

        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
