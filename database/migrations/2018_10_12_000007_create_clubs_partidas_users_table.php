<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsPartidasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clubs_id')->unsigned();
            $table->integer('pistas_id')->unsigned();
            $table->integer('nivelPartida_id')->unsigned();
            $table->dateTime('inicio_alquiler')->nullable();
            $table->dateTime('fin_alquiler')->nullable();
            $table->enum('status',  ['CERRADA', 'ABIERTA'])->default('ABIERTA');
            $table->enum('abierta_por',  ['USUARIO', 'CLUB', 'INVITADO'])->default('USUARIO');
            $table->enum('partida_fija',  ['SI', 'NO'])->default('NO');
            $table->integer('fija_id')->unsigned();
            $table->enum('equipo',  ['SI', 'NO'])->default('NO');
            $table->enum('clase',  ['SI', 'NO'])->default('NO');
            $table->integer('entrenador_id')->unsigned();
            $table->string('mensaje', 255)->nullable();
            $table->enum('torneo',  ['SI', 'NO'])->default('NO');
            $table->integer('torneo_id')->unsigned();
            $table->string('cartel', 255)->nullable();
            $table->string('observaciones', 255)->nullable();

            $table->index(['clubs_id', 'pistas_id', 'inicio_alquiler']);
            $table->unique(['clubs_id', 'pistas_id', 'inicio_alquiler']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('partidas');
        Schema::enableForeignKeyConstraints();
    }
}
