<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Partidas::class, function (Faker $faker) {
	//$users = App\User::all()->toArray();
    //$clubs = App\Clubs::all()->toArray();
    //$clubPistas = 0;
    $users = App\User::all()->where('clubs_id', '=', '1')->toArray();
    $clubs = App\Clubs::all()->where('id', '=', '1')->toArray();
    $clubPistas = 5;

    $year = "2018";
    //$month = rand(6,8);
    $month = 9;
    $day = rand(24,30);
    $hour = rand(8,22);
    $medias = array('00', '30');
    $minute = $medias[array_rand($medias,1)];
    $second = '00';
    $inicio = Carbon::create($year, $month, $day, $hour, $minute, $second);
    
    $minute2 = $inicio->minute == 30 ? '00' : '30';
    $hour2 = $inicio->minute == 30 ? $inicio->hour + 2 :  $inicio->hour + 1;

    $fin = Carbon::create($year, $month, $day, $hour2, $minute2, $second);
    
    $user = $users[array_rand($users,1)];

    for ($x = 0; $x < count($clubs); $x++){
        if($clubs[$x]['id'] == $user['clubs_id']){
            $clubPistas = $clubs[$x]['pistas'];
        }
    }


    return [
        'clubs_id'         => $user['clubs_id'],
	    'user_id'          => $user['id'],
	    'pistas_id'        => rand(1, $clubPistas),
        'nivelPartida_id'  => $user['nivel_id'],
	    'inicio_alquiler'  => $inicio,
	    'fin_alquiler'     => $fin,
	    'status'           => $faker->randomElement(['ABIERTA', 'CERRADA']),
        'abierta_por'      => $faker->randomElement(['USUARIO', 'CLUB', 'INVITADO']),
        'partida_fija'     => $faker->randomElement(['SI', 'NO']),
    ];
});
