<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\User::class, function (Faker $faker) {
   $sexo = $faker->randomElement($array = ['HOMBRE','MUJER']);
   //$avatar = $sexo == "HOMBRE"? 'http://127.0.0.1:8000/img/avatar/avatarH.png':'http://127.0.0.1:8000/img/avatar/avatarM.png';
    return [
        'nombre'            => $faker->firstName(),
        'apellidos'         => $faker->lastName,
        'sexo'              => $sexo,
        'telefono'          => $faker->numberBetween($min = 99999999, $max = 999999999),
        'poblacion_id'      => $faker->numberBetween($min = 1, $max = 500),
        'email'             => $faker->unique()->freeEmail,
        'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'nick'              => $faker->unique()->userName,
        'avatar'            => $sexo == "HOMBRE"? 'http://127.0.0.1:8000/img/avatar/avatarH.png':'http://127.0.0.1:8000/img/avatar/avatarM.png',
        'clubs_id'          => $faker->numberBetween($min = 1, $max = 9),
        'posicion'          => $faker->randomElement(['REVÉS','DERECHA','INDIFERENTE']),
        'nivel_id'          => $faker->numberBetween($min = 1, $max = 9),
        'rol_id'            => '2',
        'notificaciones'    => $faker->randomElement(['SI','NO']),
        'comentarios'       => $faker->text(200),
        'remember_token'    => Str::random(10),
    ];
});