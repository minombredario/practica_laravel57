<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
 		$year = '2023';
        $month = rand(11, 12);
        $day = rand(1, 30);

        $date = Carbon::create($year,$month ,$day , 0, 0, 0);
        $hora = $faker->time($format = 'H:i:s', $max = '21:00:00'); // '20:49:42'
        $inicio_alquiler = new DateTime($date . ' ' . $hora);
        $fin_alquiler = $inicio_alquiler->add(new DateInterval('PT1H30M'));
    return [
        'clubs_id' 			=> $faker->numberBetween($min = 1, $max = 30),
        'pista_id' 			=> $faker->numberBetween($min = 1, $max = 5),
        'user_id' 			=> $faker->numberBetween($min = 1, $max = 300),
        'inicio_alquiler' 	=> $inicio_alquiler,
        'fin_alquiler' 		=> $fin_alquiler,
        'status'			=> $faker->randomElement(['LIBRE', 'OCUPADA']),
    ];
});
