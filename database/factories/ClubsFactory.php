<?php
 
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Clubs::class, function (Faker $faker) {
	$title = $faker->unique()->sentence(2);
   	return [
        'nombre' 		 => $title,
        'slug'			 => Str::slug($title),	
        'cif'			 => $faker->randomLetter . $faker->unique()->numberBetween($min = 9999999, $max = 99999999),
        'web'            => 'www.' . $faker->unique()->domainName,
        'logo'           => $faker->imageUrl($width = 100, $height = 100),           
        'telefono'       => $faker->numberBetween($min = 99999999, $max = 999999999),
        'movil'          => $faker->numberBetween($min = 99999999, $max = 999999999),
        'email' 		 => $faker->unique()->safeEmail,
        'password' 		 => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => Str::random(10),
		'responsable'    => $faker->name,
        'pistas'         => $faker->numberBetween($min = 1, $max = 9),
        'horario'        => '',
        'rol_id'         => '3',
        'direccion'      => $faker->streetName,
        'poblacion_id'   => $faker->numberBetween($min = 1, $max = 100),
        'latitud'        => $faker->latitude($min = -90, $max = 90),
        'longitud'       => $faker->longitude($min = -180, $max = 180),
        'updated_at'     => null
    ];
});
