-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-01-2019 a las 12:55:16
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `padel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torneo_users_anonimos`
--

CREATE TABLE `torneo_users_anonimos` (
  `id` int(11) NOT NULL,
  `torneo_id` int(10) NOT NULL,
  `club_id` int(10) UNSIGNED NOT NULL,
  `user` varchar(45) DEFAULT NULL,
  `posicion` enum('REVES','DERECHA','INDIFERENTE') DEFAULT 'INDIFERENTE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `torneo_users_anonimos`
--
ALTER TABLE `torneo_users_anonimos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_id_clubs_foreign_idx` (`club_id`),
  ADD KEY `torneo_id_torneos_foreign_idx` (`torneo_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `torneo_users_anonimos`
--
ALTER TABLE `torneo_users_anonimos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `torneo_users_anonimos`
--
ALTER TABLE `torneo_users_anonimos`
  ADD CONSTRAINT `club_id_clubs` FOREIGN KEY (`club_id`) REFERENCES `clubs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `torneo_id_torneos` FOREIGN KEY (`torneo_id`) REFERENCES `torneos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
