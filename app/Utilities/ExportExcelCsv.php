<?php

namespace App\Utilities;

use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportExcelCsv implements FromCollection, WithHeadings, WithStrictNullComparison
{
	private $datos;
	private $headers;

  	public function __construct($datos, $headers)
    {
        $this->datos = $datos;
        $this->headers = $headers;
    }

    public function collection()
    {
        return $this->datos;
    }
 
    public function headings(): array
    {
        return $this->headers;
    }
	
}
