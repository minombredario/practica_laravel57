<?php

namespace App\Utilities;

use Illuminate\Database\Eloquent\Model;

class GoogleMaps extends Model
{
    public static function geocodeAddress( $direccion, $poblacion, $ciudad, $postal ){
    	  /*
		    Builds the URL and request to the Google Maps API
		  */
		  $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode( $direccion.' '.$poblacion.', '.$ciudad.' '.$postal ).'&key='.env('GOOGLE_MAPS_KEY');

		  /*
		    Creates a Guzzle Client to make the Google Maps request.
		  */
		  $client = new \GuzzleHttp\Client();
		//	$url1 = file_get_contents($url)
		  /*
		    Send a GET request to the Google Maps API and get the body of the
		    response.
		  */
		  $geocodeResponse = $client->get( $url )->getBody();
		 
		  $geocodeData = json_decode( $geocodeResponse ); 
 
		  /*
		    Initializes the response for the GeoCode Location
		  */
		  $coordinates['lat'] = null;
		  $coordinates['lng'] = null;

		  /*
		    If the response is not empty (something returned),
		    we extract the latitude and longitude from the
		    data.
		  */
		  if( !empty( $geocodeData ) && $geocodeData->status != 'ZERO_RESULTS' && isset( $geocodeData->results ) && isset( $geocodeData->results[0] ) ){
		    $coordinates['lat'] = $geocodeData->results[0]->geometry->location->lat;
		    $coordinates['lng'] = $geocodeData->results[0]->geometry->location->lng;

		  }

		  /*
		    Return the found coordinates.
		  */
		  return $coordinates;

		}

}
