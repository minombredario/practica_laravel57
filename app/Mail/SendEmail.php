<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $club;
    private $mensaje;

    public function __construct($club, $mensaje)
    {
        $this->club = $club;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->club->email_web, $this->club->responsable . ', ')
                    ->subject("Equipo " . $this->club->nombre)
                    ->view('mails.plantillaEmail')
                    ->text('mails.plantillaEmail_plain')
                    ->with(['mensaje' => $this->mensaje, 'club' => $this->club]);
    }
}
