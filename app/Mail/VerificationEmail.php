<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Clubs;
 
class VerificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $user;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        return $this->from($this->user->email_web, $this->user->responsable . ', ')
                    ->view('mails.verification')
                    ->text('mails.verification_plain')
                    ->subject("Alta en el club " . $this->user->club)
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ]);
                   /* ->attach(public_path('/image/club/'). $this->user->id . "/logo/logo.png", [
                        'mime' => 'image/jpeg,image/png,image/jpg,image/gif,image/svg',
                    ]);*/
    }
}