<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class ContactEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $datos;

    public function __construct($datos)
    {
        
        $this->datos = $datos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->datos->asunto)
                    ->view('mails.contactEmail')
                    ->text('mails.contactEmail_plain')
                    ->with(['mensaje' => $this->datos->mensaje, 'telf' => $this->datos->telf, 'from' => $this->datos->from]);
    }
}