<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

 
class RecoveryEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $user;
    public $token;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->club->email_web, $this->user->responsable . ', ')
                    ->view('mails.recovery')
                    ->text('mails.recovery_plain')
                    ->subject("Recuperar en el club " . $this->user->club)
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ]);
                    /*->attach(public_path('/image/club/'). $this->user->id . "/logo/logo.png", [
                        'as' => 'logo.png',
                        'mime' => 'image/png',
                    ]);*/
    }
}