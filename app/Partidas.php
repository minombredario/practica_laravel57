<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Clubs;
use Carbon\CarbonInterval;

class Partidas extends Model
{
    protected $table = 'partidas';
    protected $fillable = [
       'clubs_id','pistas_id', 'nivelPartida_id', 'inicio_alquiler', 'fin_alquiler','status',
        'abierta_por', 'partida_fija', 'equipo', 'observaciones', 'mensaje', 'clase', 'torneo',
         'cartel', 'torneo_id','fija_id','entrenador_id'
    ];
    
    
   
    public function club(){

        return $this->belongsToMany(Clubs::class, 'partidas', 'id','clubs_id');
    }
    public function pista(){

        return $this->belongsToMany(Pistas::class, 'partidas', 'id','pistas_id');
    }

    public function partidaFija(){  

        return $this->belongsToMany(PartidasFijas::class,'partidas_fijas', 'partida_id','club_id');
    }

    public function partidaEspera(){
        return $this->belongsToMany(ListaEspera::class, 'lista_espera', 'partida_id')
            ->withPivot('user_id', 'clubs_id', 'partida_id','comentario');
            
    }

    public function datosJugador($id){

        $jugadores = DB::table('users')
            ->join('poblaciones', 'users.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'provincias.id', '=', 'poblaciones.provincia_id')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->join('clubs', 'users.clubs_id', '=', 'clubs.id')
            ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club', 'partidas.*')
            ->Where('users.id','=', $id)
            ->where('partidas.abierta_por', '=', 'USUARIO')
            ->first();  
            
        return $jugadores;
    }

    public function listaEspera($id){

        $listaEspera = DB::table('lista_espera')
            ->select('*')
            ->Where('partida_id','=', $id)
            ->get();
            
       foreach ($listaEspera as $jugador){
            $user = DB::table('users')
                ->select('users.nick', 'niveles.nombre', 'users.avatar','users.posicion')
                ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
                ->where('users.id', '=', $jugador->user_id)
                ->first();
                $jugador->nick = $user->nick;
                $jugador->nivel = $user->nombre;
                $jugador->avatar = $user->avatar;
                $jugador->posicion = $user->posicion;
        };    


        return $listaEspera;
    }

    public function avatarJugadores($id, $club){

        $avatar = DB::table('users')
            ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
            ->select('users.avatar', 'users.posicion', 'niveles.nombre as nivel','users.id as user_id', 'users.nick as nick', 'users.sexo')
            ->Where('users.id','=', $id)
            ->where('users.clubs_id', '=', $club)
            ->first();
            
       

        return $avatar;
    }

    public function avataresPartida($partida_id, $club_id){
        
        $avatares = [];
        $palas = DB::table('partidas_user')
            ->select('partidas_user.pala_id','partidas_user.pala2_id','partidas_user.pala3_id','partidas_user.pala4_id','partidas_user.pala','partidas_user.pala2','partidas_user.pala3','partidas_user.pala4')
            ->where('partidas_user.partida_id', '=', $partida_id)
            ->first();
        
        if($palas){
            $objPalas = [['id' => $palas->pala_id, 'nick' => $palas->pala], ['id' => $palas->pala2_id, 'nick' => $palas->pala2],['id' => $palas->pala3_id, 'nick' => $palas->pala3],['id' => $palas->pala4_id, 'nick' => $palas->pala4]];
        }else{
            $objPalas = [['id' => null, 'nick' => null], ['id' => null, 'nick' => null],['id' => null, 'nick' => null],['id' => null, 'nick' => null]];
        }

        foreach($objPalas as $user){
            $avatar = $this->avatarJugadores($user['id'], $club_id);
            $avatar = json_decode(json_encode($avatar),true);

            if($avatar === null && ($user['nick'] != null || $user['nick'] != '')){

                $avatar = ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => ''];

            }
            else if($avatar === null && ($user['nick'] === null || $user['nick'] === '')){
                $avatar = ['avatar' => "../img/avatar/avatarH.png", 'nivel' => '', 'posicion' => ''];

            }else if($avatar['avatar'] === null || $avatar['avatar'] === ''){

                $img = $avatar['sexo'] === 'HOMBRE' ? 'avatarH' : 'avatarM';
                $avatar = ['avatar' => "../img/avatar/${$img}.png", 'nivel' => $avatar['nivel'], 'posicion' => $avatar['posicion'] ];
            }
            $avatares[] = $avatar;
        }

        return $avatares;
            
    }

    public function jugadoresPartida($partida_id){

        $jugadoresPorPartida = DB::table('partidas_user')
            ->select('partidas_user.*')
            ->where('partidas_user.partida_id', '=',$partida_id)
            ->get();
        $vacio = ['club_id' => 1,'pala' => '','pala2' => '','pala2_id' => '','pala3' => '','pala3_id' => '','pala4' => '','pala4_id' => '','pala_id' => '','partida_id' => $partida_id];

        return sizeof($jugadoresPorPartida) ? $jugadoresPorPartida[0] : $vacio;
    }

    public function jugadoresFijos($fijos, $partida_id, $clud_id){

        $response = [
            'club_id' => $clud_id,'partida_id' => $partida_id,
            'pala' => '', 'pala_id' => '', 'pala_fija' => false,
            'pala2' => '','pala2_id' => '', 'pala2_fija' => false,
            'pala3' => '','pala3_id' => '', 'pala3_fija' => false,
            'pala4' => '','pala4_id' => '', 'pala4_fija' => false
        ];
        if(!is_null($fijos)) {
            $fijos->pala_fija = true;
            $fijos->pala2_fija = true;
            $fijos->pala3_fija = true;
            $fijos->pala4_fija = true;
            $index = "";
            $fijos = (array)$fijos;
            foreach ( $fijos as $key => $value ) {

                if(str_ends_with($key, '_id') && !is_null($value)){
                    $col = $this->avatarJugadores($value,$clud_id);
                    if($col){
                        $fijos["pala" . ($index)] = $col->nick;
                    }

                }
                $index === "" ? 1 : $index++;

            }

            $response = array_replace_recursive($response, $fijos);


        }
        return $response;
    }

    public function partidasFijas($date, $club_id, $datos = ''){
        $fijas = [];
        
        $dia = $date->dayOfWeek + 1;

        if ($datos == 'todo') {
            // Obtener todas las partidas fijas del club
            $fijas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('partidas_fijas', 'partida_id', 'partidas.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores', 'entrenadores.id', 'partidas.entrenador_id')
                ->select('partidas.id as id', 'partidas.pistas_id as pistas_id',
                    'partidas.pistas_id as resources', 'partidas.fin_alquiler as end',
                    'partidas.inicio_alquiler as start', 'partidas.status', 'partidas.abierta_por',
                    'partidas.partida_fija', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida',
                    'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel',
                    'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.clase', 'partidas.observaciones',
                    'partidas.torneo', 'partidas.torneo_id', 'partidas.fija_id', 'partidas.entrenador_id',
                    'entrenadores.nombre as entrenador', 'entrenadores.avatar as avatar_entrenador',
                    'partidas_fijas.jugadores_fijos')
                ->where('partidas.clubs_id', $club_id)
                ->orderBy('partidas.fija_id', 'DESC')
                ->get();
        } else {
            // Obtener partidas fijas específicas del club para un día de la semana
            $fijas = DB::table('partidas')
                ->join('partidas_fijas', 'partidas_fijas.id', 'partidas.fija_id')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores', 'entrenadores.id', 'partidas.entrenador_id')
                ->select('partidas.id as id', 'partidas.pistas_id as pistas_id',
                    'partidas.pistas_id as resources', 'partidas.fin_alquiler as end',
                    'partidas.inicio_alquiler as start', 'partidas.status', 'partidas.abierta_por',
                    'partidas.partida_fija', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida',
                    'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel',
                    'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.clase', 'partidas.observaciones',
                    'partidas.torneo', 'partidas.torneo_id', 'partidas.fija_id', 'partidas.entrenador_id',
                    'entrenadores.nombre as entrenador', 'entrenadores.avatar as avatar_entrenador',
                    'partidas_fijas.jugadores_fijos')
                ->whereRaw("DAYOFWEEK(inicio_alquiler) = " . $dia . " and partidas.clubs_id = " . $club_id)
                ->groupBy(DB::Raw('time(partidas.inicio_alquiler), partidas.pistas_id'))
                ->get();

            foreach ($fijas as $fija) {
                // Ajustar las fechas para que coincidan con el formato esperado
                $fija->start = $date->format('Y-m-d') . " " . substr($fija->start, 11, 8);
                $fija->inicio_alquiler = $date->format('Y-m-d') . " " . substr($fija->start, 11, 8);
                $fija->end = $date->format('Y-m-d') . " " . substr($fija->end, 11, 8);
                $fija->fin_alquiler = $date->format('Y-m-d') . " " . substr($fija->end, 11, 8);

                // Obtener información adicional para cada partida fija
                $listaEspera = $this->listaEspera($fija->id);
                $juagadoreDecode = json_decode($fija->jugadores_fijos);
                $jugadoresPartida = $this->jugadoresFijos($juagadoreDecode, $fija->id, $club_id );
                $avatares = $this->avataresPartida($fija->id, $fija->clubs_id);

                $torneoUsers = ($fija->torneo_id != null && $fija->torneo_id != 0) ? $this->usersTorneo($fija->torneo_id, $fija->clubs_id) : null;
                $fija->jugadores_fijos = $jugadoresPartida;
                $fija->listaEspera = $listaEspera;
                $fija->avatares = $avatares;
                $fija->jugadoresPartida = ($fija->torneo_id != null && $fija->torneo_id != 0) ? $torneoUsers : $jugadoresPartida;
                $fija->partidaVirtual = true;
            }
        }



        return $fijas;
    }

    public function horarioPista($club_id){
        $horario = DB::table('horario_pistas')->where('club_id', $club_id)->select('*')->get();
        return $horario;
    }

    public function isTorneo($partida_id, $club_id){
        $torneo = DB::table('torneos')->where('club_id', $club_id)->where('partida_id', $partida_id)->pluck('id')->first();
        return $torneo;
    }

    public function usersTorneo($id, $club_id){
        $jugadores = [];

        $users = DB::table('torneo_users')
                ->join('users', 'users.id', 'torneo_users.user_id')
                ->join('niveles', 'users.nivel_id', '=', 'niveles.id')
                ->where('club_id', $club_id)
                ->where('torneo_id', $id)
                ->select('users.nick', 'niveles.nombre as nivel')
                ->get();
        $anonimos = DB::table('torneo_users_anonimos')
                ->where('club_id', $club_id)
                ->where('torneo_id', $id)
                ->select('torneo_users_anonimos.user as nick')
                ->get();

        foreach ($users as $user){ 
            array_push($jugadores, $user);
        }
        foreach ($anonimos as $user){ 
            array_push($jugadores, $user);
        }  

        return ($users->count() || $anonimos->count()) ? $jugadores : [];
    }


    /*-------------------------------------- LISTA PARTIDAS POR CLUB O USER --------------------------------------*/


    public function scopeBusquedaClubPartida($query,$id){
        config(['app.timezone' => 'Europe/Madrid']);
        
        $date = Carbon::today();
        $date = Carbon::parse($date, 'Europe/Madrid')->addHours(Carbon::parse($date, 'Europe/Madrid')->offsetHours);

        $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id',
                'partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start',
                'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 
                'niveles.nombre as nivelPartida', 'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id',
                'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.clase', 'partidas.cartel',
                'partidas.observaciones', 'partidas.torneo', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->where('partidas.clubs_id', '=', $id)
                ->whereDate('inicio_alquiler', '=', $date->format('Y-m-d'))
                ->orderBy('inicio_alquiler', 'ASC')
                ->get();

                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->partida_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->partida_id);
                    $avatares = $this->avataresPartida($partida->partida_id, $partida->clubs_id);

                    $partida->listaEspera = $listaEspera;
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = $jugadoresPartida;
                };

                return $partidas;
    }


    public function scopeBuscarPartida($query,$id){
        $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                //->join('users', 'partidas.user_id', '=', 'users.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id',
                'partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start',
                'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 
                'niveles.nombre as nivelPartida', 'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id',
                'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje','partidas.clase', 'partidas.cartel',
                'partidas.observaciones', 'partidas.torneo', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->where('partidas.id', '=', $id)
                ->get();

        //meto los demas datos en cada partida
        foreach ($partidas as $partida){
            $listaEspera = $this->listaEspera($partida->partida_id);
            //$datosJugador = $this->datosJugador($partida->user_id);
            $jugadoresPartida = $this->jugadoresPartida($partida->partida_id);
            $avatares = $this->avataresPartida($partida->partida_id, $partida->clubs_id);

            //$partida->datosJugador = $datosJugador;
            $partida->listaEspera = $listaEspera;
            $partida->avatares = $avatares;
            $partida->jugadoresPartida = $jugadoresPartida;
        };

        return $partidas;

              
    }

    /*-------------------------------------- LISTA PARTIDAS --------------------------------------*/

    public function scopeBusqueda($query,$busqueda,$sortBy,$orden,$semana,$id){ 
        CarbonInterval::setLocale('es');
        $vista = str_ireplace(" ", "%",substr($semana,10,6)); 
        $ano  = (int)substr($semana,0,4); //2018-03-19
        $mes  = (int)substr($semana,5,2);
        $dia  = (int)substr($semana,8,2);
        $date = Carbon::create($ano,$mes,$dia);
        $from = $date->format('Y-m-d'); 
        $to   = $date->addDays(7)->format('Y-m-d');
               
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);

        if($vista == 'agenda'){
            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->leftJoin('partidas_user', 'partidas_user.partida_id', '=', 'partidas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as partida_id', 'partidas.pistas_id as pistas_id', 'partidas.fin_alquiler',
                'partidas.inicio_alquiler', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija',
                'partidas.clubs_id', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida', 
                'clubs.nombre as club','pistas.nombre as pista','clubs.pistas as Npistas', 'partidas.equipo',
                'partidas.mensaje', 'partidas.cartel', 'partidas.observaciones', 'partidas_user.pala',
                'partidas_user.pala2','partidas_user.pala3','partidas_user.pala4', 'partidas.torneo',
                'partidas.clase', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->whereDate('inicio_alquiler', '=', $from)
                ->where(function($query) use ($busqueda, $id){
                    $id == null ? 
                        $query->orWhere('pistas.nombre','like', $busqueda .'%')
                              ->orWhere('partidas_user.pala', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala2', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala3', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala4', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas.observaciones', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas.mensaje', 'like','%' . $busqueda .'%')
                        : $query->where('clubs.nombre' ,'like','%' . $busqueda .'%')
                                ->where(function($partida) use ($id){
                                    $partida->Where('partidas.clubs_id','=', $id);
                                            //->orWhere('partidas.user_id','=', $id);
                                })
                                ->orWhere(function($subquery) use ($busqueda){
                                    $busqueda == null ?
                                        '' 
                                        : $subquery->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas.observaciones', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas.mensaje', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala2', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala3', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala4', 'like','%' . $busqueda .'%');
                                });
                })
                
                ->orderBy($sortBy, $orden)
                ->orderBy('inicio_alquiler','ASC')
                ->paginate(8);

                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->partida_id);
                    //$datosJugador = $this->datosJugador($partida->user_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->partida_id);
                    $avatares = $this->avataresPartida($partida->partida_id, $partida->clubs_id);

                    //$partida->datosJugador = $datosJugador;
                    $partida->listaEspera = $listaEspera;
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = $jugadoresPartida;
                };

                return $partidas;

        }else if($vista == 'semana'){

            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->leftJoin('partidas_user', 'partidas_user.partida_id', '=', 'partidas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as partida_id', 'partidas.pistas_id as pistas_id', 'partidas.fin_alquiler',
                'partidas.inicio_alquiler', 'partidas.status', 'partidas.abierta_por', 'partidas.partida_fija',
                'partidas.clubs_id', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida',
                'clubs.nombre as club','pistas.nombre as pista','clubs.pistas as Npistas',
                'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.observaciones',
                'partidas_user.pala', 'partidas_user.pala2','partidas_user.pala3','partidas_user.pala4',
                'partidas.torneo','partidas.clase', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->whereBetween('inicio_alquiler', [$from, $to])
                ->where(function($query) use ($busqueda, $id){
                    $id == null ? 
                        $query->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                              ->orWhere('pistas.nombre','like', $busqueda .'%')
                              ->orWhere('partidas_user.pala', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala2', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala3', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas_user.pala4', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas.observaciones', 'like','%' . $busqueda .'%')
                              ->orWhere('partidas.mensaje', 'like','%' . $busqueda .'%')
                        : $query->where('clubs.nombre' ,'like','%' . $busqueda .'%')
                                ->where(function($partida) use ($id){
                                    $partida->Where('partidas.clubs_id','=', $id);
                                            //->orWhere('partidas.user_id','=', $id);
                                })
                                ->orWhere(function($subquery) use ($busqueda){
                                    $busqueda == null ?
                                        '' 
                                        : $subquery->orWhere('clubs.nombre' ,'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas.observaciones', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas.mensaje', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala2', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala3', 'like','%' . $busqueda .'%')
                                                    ->orWhere('partidas_user.pala4', 'like','%' . $busqueda .'%');

                                });
                })
                ->orderBy($sortBy, $orden)
                ->orderBy('inicio_alquiler','ASC')
                ->paginate(8);

                
                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->partida_id);
                    //$datosJugador = $this->datosJugador($partida->user_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->partida_id);
                    $avatares = $this->avataresPartida($partida->partida_id, $partida->clubs_id);

                    //$partida->datosJugador = $datosJugador;
                    $partida->listaEspera = $listaEspera;
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = $jugadoresPartida;
                };

                return $partidas;


        }
        
    }
 
    /*-------------------------------------- FULLCALENDAR --------------------------------------*/
    public function scopeBusquedaPartidaClub2($query,$dia,$vista,$club_id, $user_id){ 
        CarbonInterval::setLocale('es');
        $ano  = (int)substr($dia,0,4); //2018-03-19
        $mes  = (int)substr($dia,5,2);
        $dia_  = (int)substr($dia,8,2);
        $date = Carbon::createFromDate($ano,$mes,$dia_, 'Europe/London');
        $date_ = Carbon::createFromDate($ano,$mes,$dia_, 'Europe/London');
        $from = $date_->format('Y-m-d'); 
        $to   = $date_->addDays(7)->format('Y-m-d');
        $startWeek = Carbon::now()->startOfWeek()->format('Y-m-d');       
        $endWeek =   Carbon::now()->endOfWeek()->format('Y-m-d H:i');

        
            if($vista == 'resourceDay'){
                $partidas = DB::table('partidas')
                            ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                            ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                            ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                            ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                            ->select('partidas.id as id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources',
                            'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.status',
                            'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 
                            'niveles.nombre as nivelPartida', 'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id',
                            'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 'partidas.cartel', 'partidas.clase',
                            'partidas.observaciones', 'partidas.torneo', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                            'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                            ->whereDate('partidas.inicio_alquiler', '=', $dia)
                            ->where(function($query) use ($user_id, $club_id){
                                $user_id != 'club' ? 
                                    $query->where('partidas.clubs_id', '=', $club_id)
                                            ->where(function($subquery) use ($user_id){
                                                $subquery->orwhere('partidas_user.pala_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala2_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala3_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala4_id','=', $user_id);
                                            })
                                    : $query->where('partidas.clubs_id', '=', $club_id);
                                            
                            })
                            ->get();
                            
                            //meto los demas datos en cada partida
                            foreach ($partidas as $partida){
                                $listaEspera = $this->listaEspera($partida->id);
                                //$datosJugador = $this->datosJugador($partida->user_id);
                                $jugadoresPartida = $this->jugadoresPartida($partida->id);
                                $avatares = $this->avataresPartida($partida->id, $partida->clubs_id);

                                $torneoUsers = ($partida->torneo_id != null && $partida->torneo_id != 0)? $this->usersTorneo($partida->torneo_id, $partida->clubs_id) : null;
                                //$partida->datosJugador = $datosJugador;
                                $partida->listaEspera = $listaEspera; 
                                $partida->avatares = $avatares;
                                $partida->jugadoresPartida = ($partida->torneo_id != null && $partida->torneo_id != 0) ? $torneoUsers : $jugadoresPartida;
                                
                                
                            };

                $response = [
                            'partidas' => $partidas,
                            'fijas' => $this->partidasFijas($date, $club_id, ''),
                            'horario' => $this->horarioPista($club_id),
                            ];      
                return $response;
            }else{
                $partidas = DB::table('partidas')
                            ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                            ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                            ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                            ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                            ->select('partidas.id as id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources',
                             'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.status', 
                             'partidas.abierta_por', 'partidas.partida_fija', 'partidas.nivelPartida_id', 
                             'niveles.nombre as nivelPartida', 'pistas.nombre as pista', 'clubs.pistas', 
                             'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje', 
                             'partidas.cartel', 'partidas.clase', 'partidas.observaciones', 'partidas.torneo', 
                             'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                             'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                            ->whereBetween('partidas.inicio_alquiler', [$dia, $endWeek])
                            ->where(function($query) use ($user_id, $club_id){
                                $user_id != 'club' ? 
                                    $query->where('partidas.clubs_id', '=', $club_id)
                                            ->where(function($subquery) use ($user_id){
                                                $subquery->orwhere('partidas_user.pala_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala2_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala3_id','=', $user_id)
                                                         ->orWhere('partidas_user.pala4_id','=', $user_id);
                                            })
                                    : $query->where('partidas.clubs_id', '=', $club_id);
                                            
                            })
                            ->get();

                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->id);
                    //$datosJugador = $this->datosJugador($partida->user_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->id);
                    $avatares = $this->avataresPartida($partida->id, $partida->clubs_id);
                    $torneoUsers = ($partida->torneo_id != null && $partida->torneo_id != 0)? $this->usersTorneo($partida->torneo_id, $partida->clubs_id) : null;
                                //$partida->datosJugador = $datosJugador;
                    $partida->listaEspera = $listaEspera; 
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = ($partida->torneo_id != null && $partida->torneo_id != 0) ? $torneoUsers : $jugadoresPartida;
                   
                    
                };    

                $response = [
                            'partidas' => $partidas,
                            'fijas' => $this->partidasFijas($date, $club_id, 'todo'),
                        ];

                return $response;
            }
            
    }


    public function scopeBusquedaPartidaClub($query,$semana,$club_id, $user_id){ 
        CarbonInterval::setLocale('es');
        $vista = str_ireplace(" ", "%",substr($semana,10,6)); 
        $ano  = (int)substr($semana,0,4); //2018-03-19
        $mes  = (int)substr($semana,5,2);
        $dia  = (int)substr($semana,8,2);
        $date = Carbon::createFromDate($ano,$mes,$dia, 'Europe/London');
        $from = $date->format('Y-m-d'); 
        $to   = $date->addDays(7)->format('Y-m-d');
        $startWeek = Carbon::now()->startOfWeek()->format('Y-m-d');       
        $endWeek =   Carbon::now()->endOfWeek()->format('Y-m-d H:i');

        if($vista == 'agenda'){

            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources',
                'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.status', 'partidas.abierta_por',
                'partidas.partida_fija', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida', 'pistas.nombre as pista',
                'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje',
                'partidas.cartel', 'partidas.clase', 'partidas.observaciones', 'partidas.torneo', 'partidas.torneo_id',
                'partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->whereDate('partidas.inicio_alquiler', '=', $from)
                ->where(function($query) use ($user_id, $club_id){
                    $user_id != 'club' ? 
                        $query->where('partidas.clubs_id', '=', $club_id)
                                ->where(function($subquery) use ($user_id){
                                    $subquery->orwhere('partidas_user.pala_id','=', $user_id)
                                             ->orWhere('partidas_user.pala2_id','=', $user_id)
                                             ->orWhere('partidas_user.pala3_id','=', $user_id)
                                             ->orWhere('partidas_user.pala4_id','=', $user_id);
                                })
                        : $query->where('partidas.clubs_id', '=', $club_id);
                                
                })
                ->get();
                
                                   
                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->id);
                    //$datosJugador = $this->datosJugador($partida->user_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->id);
                    $avatares = $this->avataresPartida($partida->id, $partida->clubs_id);

                    //$partida->datosJugador = $datosJugador;
                    $partida->listaEspera = $listaEspera;
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = $jugadoresPartida;
                }; 

                return 'agenda';

        }else if($vista == 'semana'){

            $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->select('partidas.id as id', 'partidas.pistas_id as pistas_id','partidas.pistas_id as resources',
                'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start', 'partidas.status', 'partidas.abierta_por',
                'partidas.partida_fija', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida', 'pistas.nombre as pista',
                'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 'partidas.equipo', 'partidas.mensaje',
                'partidas.cartel', 'partidas.clase', 'partidas.observaciones', 'partidas.torneo', 'partidas.torneo_id',
                'partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->whereBetween('partidas.inicio_alquiler', [$startWeek, $endWeek])
                //->where('partidas.clubs_id', '=', $club_id)
                ->where(function($query) use ($user_id, $club_id){
                    $user_id != 'club' ? 
                        $query->where('partidas.clubs_id', '=', $club_id)
                                ->where(function($subquery) use ($user_id){
                                    $subquery->orwhere('partidas_user.pala_id','=', $user_id)
                                             ->orWhere('partidas_user.pala2_id','=', $user_id)
                                             ->orWhere('partidas_user.pala3_id','=', $user_id)
                                             ->orWhere('partidas_user.pala4_id','=', $user_id);
                                })
                        : $query->where('partidas.clubs_id', '=', $club_id);
                                
                })
                ->get();

                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $listaEspera = $this->listaEspera($partida->id);
                    //$datosJugador = $this->datosJugador($partida->user_id);
                    $jugadoresPartida = $this->jugadoresPartida($partida->id);
                    $avatares = $this->avataresPartida($partida->id, $partida->clubs_id);

                    //$partida->datosJugador = $datosJugador;
                    $partida->listaEspera = $listaEspera;
                    $partida->avatares = $avatares;
                    $partida->jugadoresPartida = $jugadoresPartida;
                };    

                return $partidas;

        }
    }


    public function scopeMisPartidas($query,$club_id, $nick, $user_id){ 
        $partidas = DB::table('partidas')
                ->join('clubs', 'partidas.clubs_id', '=', 'clubs.id')
                ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
                ->join('niveles', 'partidas.nivelPartida_id', '=', 'niveles.id')
                ->leftjoin('entrenadores','entrenadores.id','partidas.entrenador_id')
                ->leftJoin('partidas_user', 'partidas_user.partida_id', '=', 'partidas.id')
                ->select('partidas.id as id','partidas.id as partida_id', 'partidas.pistas_id as pistas_id',
                'partidas.pistas_id as resources', 'partidas.fin_alquiler as end', 'partidas.inicio_alquiler as start',
                'partidas.fin_alquiler', 'partidas.inicio_alquiler', 'partidas.status', 'partidas.abierta_por', 
                'partidas.partida_fija', 'partidas.nivelPartida_id', 'niveles.nombre as nivelPartida', 
                'pistas.nombre as pista', 'clubs.pistas', 'partidas.clubs_id', 'niveles.nombre as nivel', 
                'partidas.equipo', 'partidas.mensaje', 'partidas.clase', 'partidas.cartel', 'partidas.observaciones',
                'partidas.torneo', 'partidas.torneo_id','partidas.fija_id','partidas.entrenador_id',
                'entrenadores.nombre as entrenador','entrenadores.avatar as avatar_entrenador')
                ->where('partidas.clubs_id', '=', $club_id)
                ->where(function($query) use ($user_id){
                    $query->orWhere('partidas_user.pala_id', '=', $user_id)
                        ->orWhere('partidas_user.pala2_id', '=', $user_id)
                        ->orWhere('partidas_user.pala3_id', '=', $user_id)
                        ->orWhere('partidas_user.pala4_id', '=', $user_id);
                })
                
                ->orderBy('start', 'DESC')
                ->paginate(8);

                //meto los demas datos en cada partida
                foreach ($partidas as $partida){
                    $jugadoresPartida = $this->jugadoresPartida($partida->id);

                    $partida->jugadoresPartida = $jugadoresPartida;
                };

                return $partidas;

        
    }

    public function scopeMisEsperas($club_id, $user_id){

        $listaEspera = DB::table('lista_espera')
            ->join('partidas', 'lista_espera.partida_id', '=','partidas.id')
            ->select('lista_espera.*', 'partidas.inicio_alquiler', 'partidas.fin_alquiler')
            ->Where('lista_espera.user_id','=', $user_id)
            ->where('lista_espera.clubs_id','=', $club_id)
            ->get();

        return $listaEspera;
    }


}
