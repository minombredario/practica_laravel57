<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'social';

     protected $fillable = [
       'club_id','facebook','twitter', 'google','instagram','youtube', 'status','visible',
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }

}
