<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioPista extends Model
{
    protected $table = 'horario_pistas';

    protected $fillable = [
       'club_id','pista_id', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo'
    ];

    public $timestamps = false;
}
