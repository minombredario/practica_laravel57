<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clases extends Model
{
    protected $table = 'clases';

    protected $fillable = [
       'club_id','clases', 'visible'
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
