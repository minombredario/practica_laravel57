<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidasUsuario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
       'club_id', 'partida_id', 'pala_id', 'pala','pala2_id', 'pala2','pala3_id', 'pala3','pala4_id', 'pala4',
    ];

    protected $table = 'partidas_user';
    public $timestamps = false;


}
