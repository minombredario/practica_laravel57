<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincias extends Model
{
    
    protected $fillable = ['nombre', 'slug', 'nombre_corto'];
    public $timestamps = false;

    public function poblaciones()
    {
        return $this->hasMany(Poblaciones::class);
    }

    
}
