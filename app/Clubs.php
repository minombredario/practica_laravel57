<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\notifications\ClubResetPasswordNotification;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class Clubs extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
       'nombre','slug', 'cif','telefono','logo', 'movil','email','email_web', 'password','responsable','pistas','horario', 'web', 'rol_id', 'direccion', 'poblacion_id','latitud', 'longitud','remember_token', 'nosotros'
    ];
    

  
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ClubResetPasswordNotification($token));
    }

    public function poblacion(){

        return $this->belongsTo(Poblaciones::class);
    }
    
    public function instalaciones(){

        return $this->belongsToMany(Instalaciones::class);
    } 

    public function partidas()    {
        return $this->belongsToMany(User::class, 'partidas')
            ->withPivot('partida_id', 'inicio_alquiler', 'fin_alquiler', 'status');
    }

    public function users(){

        return $this->belongsToMany(User::class);
    }       


    public function pistas(){

        return $this->belongsToMany(Pistas::class);
    }

    public function ligas(){

        return $this->belongsToMany(Ligas::class);
    }

    public function torneos(){

        return $this->belongsToMany(Torneos::class);
    }

    public function rol(){
        return $this->belongsTo(Roles::class);
    }

    public function scopeBusqueda($query,$busqueda,$sortBy,$orden){
       
        $busqueda = str_ireplace(" ", "%", $busqueda);
        $sortBy = str_ireplace(" ", "%", $sortBy);
        $orden = str_ireplace(" ", "%", $orden);
        $datos = [];
        $clubs = DB::table('clubs')
            ->join('poblaciones', 'clubs.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
            ->select('clubs.id','clubs.cif','clubs.nombre','clubs.direccion', 'clubs.email_web','clubs.horario','clubs.logo','clubs.movil','clubs.pistas','clubs.responsable','clubs.telefono','clubs.movil','clubs.web','clubs.slug','clubs.poblacion_id','clubs.latitud','clubs.longitud','clubs.nosotros','poblaciones.nombre as poblacion','poblaciones.postal as postal','provincias.id as provincia_id', 'provincias.nombre as provincia')
            ->orwhere('clubs.nombre' ,'like','%' . $busqueda .'%')
            ->orwhere('clubs.responsable' ,'like','%' . $busqueda .'%')
            ->orwhere('clubs.telefono' ,'like', $busqueda .'%')
            ->orwhere('poblaciones.nombre' ,'like','%' . $busqueda .'%')
            ->orderBy($sortBy, $orden)
            ->paginate(8);
       
        foreach ($clubs as $club){
            $instalaciones = DB::table('clubs_instalaciones')
                ->join('instalaciones', 'clubs_instalaciones.instalaciones_id', '=', 'instalaciones.id')
                ->select('instalaciones.id','instalaciones.nombre')
                ->where('clubs_instalaciones.clubs_id', '=', $club->id)
                ->get();
                $club->instalaciones = $instalaciones;
        };

        return $clubs;
        

    }

    public function scopeMostrar($query,$id){
        $date = Carbon::today();
        $date = Carbon::parse($date, 'Europe/Madrid')->addHours(Carbon::parse($date, 'Europe/Madrid')->offsetHours);

        $instalaciones = DB::table('clubs_instalaciones')
                        ->join('instalaciones', 'clubs_instalaciones.instalaciones_id', '=', 'instalaciones.id')
                        ->select('instalaciones.id','instalaciones.nombre')
                        ->where('clubs_instalaciones.clubs_id', '=', $id)
                        ->get();    
        
        $clases = DB::table('clases')->where([['club_id', '=', $id], ['visible', '=', 1]])->get(); 
        $competiciones = DB::table('competiciones')->where([['club_id', '=', $id], ['visible', '=', 1]])->get();
        $ligas = DB::table('ligas')->where([['clubs_id', '=', $id], ['visible', '=', 1]])->get();
        $torneos = DB::table('torneos')->where([['club_id', '=', $id], ['visible', '=', 1]])->whereDate('fecha', '>=', $date->format('Y-m-d'))->orderBy('fecha', 'ASC')->get();

        $club = DB::table('clubs')
            ->join('poblaciones', 'clubs.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
            ->join('config_web', 'club_id', '=', 'clubs.id')
            ->select('clubs.id','clubs.cif','clubs.nombre','clubs.direccion','clubs.email','clubs.email_web','clubs.horario','clubs.logo','clubs.movil','clubs.pistas','clubs.responsable','clubs.telefono','clubs.movil','clubs.web','clubs.slug','clubs.rol_id','clubs.latitud','clubs.longitud','clubs.nosotros','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'provincias.nombre as provincia', 'poblaciones.provincia_id','config_web.cards','config_web.paralax','config_web.imgparalax','config_web.galeria','config_web.jumbotron','config_web.imageslider','config_web.bqsocial','config_web.patrocinadores','config_web.brsocial','config_web.columnas','config_web.welcome','config_web.noticias')
            ->where('clubs.id', '=', $id)
            ->get();

        return $response = [
            'club' => $club,
            'instalaciones' => $instalaciones,
            'clases' => $clases,
            'competiciones' => $competiciones,
            'ligas' => $ligas,
            'torneos' => $torneos,
        ]; 
    }

    public function scopeMostrarHome($query,$id){
        $date = Carbon::today();
        $date = Carbon::parse($date, 'Europe/Madrid')->addHours(Carbon::parse($date, 'Europe/Madrid')->offsetHours);

        $instalaciones = DB::table('clubs_instalaciones')
                        ->join('instalaciones', 'clubs_instalaciones.instalaciones_id', '=', 'instalaciones.id')
                        ->select('instalaciones.id','instalaciones.nombre')
                        ->where('clubs_instalaciones.clubs_id', '=', $id)
                        ->get();    
        
        $clases = DB::table('clases')->where([['club_id', '=', $id], ['visible', '=', 1]])->get(); 
        $competiciones = DB::table('competiciones')->where([['club_id', '=', $id], ['visible', '=', 1]])->get();
        $ligas = DB::table('ligas')->where([['clubs_id', '=', $id], ['visible', '=', 1]])->get();
        $torneos = DB::table('torneos')->where([['club_id', '=', $id], ['visible', '=', 1]])->whereDate('fecha', '>=', $date->format('Y-m-d'))->orderBy('fecha', 'ASC')->get();

        $club = DB::table('clubs')
            ->join('poblaciones', 'clubs.poblacion_id', '=', 'poblaciones.id')
            ->join('provincias', 'poblaciones.provincia_id', '=', 'provincias.id')
            ->join('config_web', 'club_id', '=', 'clubs.id')
            ->select('clubs.id','clubs.cif','clubs.nombre','clubs.direccion','clubs.email_web as email','clubs.horario','clubs.logo','clubs.movil','clubs.pistas','clubs.responsable','clubs.telefono','clubs.movil','clubs.web','clubs.slug','clubs.rol_id','clubs.latitud','clubs.longitud','clubs.nosotros','poblaciones.nombre as poblacion','poblaciones.postal as postal', 'provincias.nombre as provincia', 'poblaciones.provincia_id','config_web.cards','config_web.paralax','config_web.imgparalax','config_web.galeria','config_web.jumbotron','config_web.imageslider','config_web.bqsocial','config_web.patrocinadores','config_web.brsocial','config_web.columnas','config_web.welcome','config_web.noticias')
            ->where('clubs.id', '=', $id)
            ->get();

        return $response = [
            'club' => $club,
            'instalaciones' => $instalaciones,
            'clases' => $clases,
            'competiciones' => $competiciones,
            'ligas' => $ligas,
            'torneos' => $torneos,
        ]; 
    }

}
