<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TorneoUsersAnonimos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
       'club_id', 'torneo_id', 'user', 'posicion'
    ];

    protected $table = 'torneo_users_anonimos';
    public $timestamps = false;


}