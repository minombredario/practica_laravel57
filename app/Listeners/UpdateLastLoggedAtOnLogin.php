<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateLastLoggedAtOnLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $now = Carbon::now()->timestamp; 
        
        // para que no se actualice la columna "updated_at"
        $event->user->timestamps = false;
 
        if (is_null($event->user->last_logged_at)) {
            $event->user->last_logged_at = $now;
        }
        $event->user->current_login_time = $now;
        $event->user->online = 1;
 
        $event->user->save();
    }
}
