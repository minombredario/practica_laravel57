<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateLastLoggedAtOnLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        // para que no se actualice la columna "updated_at"
        $event->user->timestamps     = false;
 
        $event->user->last_logged_at = $event->user->current_login_time;
        $event->user->online = 0;
        
        $event->user->save();
        // Log::info("ultima conexion: {$event->user->last_logged_at}" );
    }
}
