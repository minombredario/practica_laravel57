<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Columnas extends Model
{
   protected $table = 'columnas';

    protected $fillable = [
       'club_id','titulo','contenido', 'imagen','nombre','web', 'visible',
    ];
    
    public $timestamps = false;

    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
