<?php

namespace App\Http\Controllers\userAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Tymon\JWTAuth\Exceptions\JWTException;

use JWTAuth;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
        
        
        
        $validar = $this->validate($request, [
                    'email'     => 'required|email',
                    'password'  => 'required'
                ]);
        
        $credential = $request->only('email','password');

       if(!$token = JWTAuth::attempt($credential)){
        return "no autenticado";
       };

       return ['token' => $token];
        
    }

    public function logout()
    {
        Auth::guard('user')->logout();
        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard('user');
    }
}
