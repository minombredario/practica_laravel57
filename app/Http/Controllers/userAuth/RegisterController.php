<?php

namespace App\Http\Controllers\userAuth;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use Image;
use App\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'club/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function register(Request $request){
        
        $validar = $this->validate($request, [
            'nombre' => 'required|string',
            'apellidos' => 'nullable|string',
            'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $request->get('clubs_id') .',telefono,' . $request->get('telefono'),  //compruebo que el nick no esta registrado ya en ese club.
            'sexo' => 'required|in:HOMBRE,MUJER',
            'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
            'desplazamiento' => 'nullable|integer',
            'disponibilidad' => 'nullable|string',
            'telefono' => 'string|required|unique:users,telefono,null,id,clubs_id,' . $request->get('clubs_id') . ',nick,' . $request->get('nick'),
            'poblacion_id' => 'required|integer|min:0',
            'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,' . $request->get('nick') . ',telefono,' . $request->get('telefono'),
            'nivel_id' => 'required|integer|min:0',
            'comentarios' => 'nullable|string',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);


      if($validar){
        
        $create = User::create($request->except('avatar'));
        $user = User::find($create->id);

        $user->update([
          'password' => bcrypt($request->get('password'))
        ]);

        $user->clubs()->attach([
          $request->get('clubs_id') => [
              'email' => $request->get('email'),//asigno al id del club el email del usuario inertandolo a traves de su id.
              'password' => bcrypt($request->get('password')),
            ]
          ]);

        if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(499,498, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $create->id . '/avatar/' . $filename;
                $user->update([
                   'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
                 ]); 

              }else{
                $avatar = $request->get('sexo') == "HOMBRE"? 'http://127.0.0.1:8000/img/avatar/avatarH.png' : 'http://127.0.0.1:8000/img/avatar/avatarM.png';
                $user->update([
                  'avatar' => $avatar,
                ]);

              }


              return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);
            }


    }
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
