<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    protected function validateSendResetLinkEmail(Request $request)
	{
	    $this->validate($request, ['email' => 'required|email']);
	}

	protected function getSendResetLinkEmailCredentials(Request $request)
	{
	    return $request->only('email', 'clubs_id');
	}

	protected function getSendResetLinkEmailFailureResponse($response)
	{
	    return redirect()->back()->withErrors([
	    	'email' => trans($response),
	    	'clubs_id' => trans($response),
	    ]);
	}

	public function showResetForm(Request $request, $token = null)
	{
	    if (is_null($token)) {
	        return $this->getEmail();
	    }

	    $email = $request->input('email');

	    if (property_exists($this, 'resetView')) {
	        return view($this->resetView)->with(compact('token', 'email', 'clubs_id'));
	    }

	    if (view()->exists('auth.passwords.reset')) {
	        return view('auth.passwords.reset')->with(compact('token', 'email'));
	    }

	    return view('auth.reset')->with(compact('token', 'email'));
	}

	protected function getResetValidationRules()
	{
	    return [
	        'token' => 'required',
	        'email' => 'required|email',
	        'password' => 'required|confirmed|min:4',
	    ];
	}

	protected function getResetCredentials(Request $request)
	{
	    return $request->only(
	        'email', 'password', 'password_confirmation', 'token', 'clubs_id'
	    );
	}

	protected function getResetFailureResponse(Request $request, $response)
	{
	    return redirect()->back()
	        ->withInput($request->only('email'))
	        ->withErrors(['email' => trans($response)]);
	}
}
