<?php

namespace App\Http\Controllers\clubAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';
    protected $redirectAfterLogout = 'admin/iniciarSesion';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:club', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        $recuperarPass = "club-password/reset";
        $dashboard = 'dashboardClub';
        config(['app.timezone' => 'Europe/Madrid']);
        return view('club.login', compact(['recuperarPass','dashboard']));
    }

    public function login(Request $request)
    {

        $validar = $this->validate($request, [
                    'email'     => 'required|email',
                    'password'  => 'required'
                ]);

        //compruebo que el correo esta.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            
            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);

        //si todo es correcto paso a la vista del dashboard
        if($validar && Auth::guard('club')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
          return redirect()->intended(route('admin.dashboard'));
        }
        //si no es correcto redirijo a la dirección del login con el form data
        //return redirect()->back()->withInput($request->only('email','remember'));
        //return response()->json($request->only('email','remember'));
        
        $response = [
            'pass' => $this->sendFailedLoginResponse($request),
            'validar' => $validar,
            'recuperar' => $request->only('email','remember'),
        ];
        
        return response()->json($response);

        
    }

    public function logout()
    {
        Auth::guard('club')->logout();
        return redirect('/admin/iniciarSesion');
    }

    protected function guard()
    {
        return Auth::guard('club');
    }
}
