<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Image;
use App\Clubs;
use Carbon\Carbon;
use App\passwordReset;
use App\Mail\VerificationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Rules\Recaptcha;

//https://es.stackoverflow.com/questions/92223/c%C3%B3mo-personalizar-el-email-de-restablecimiento-de-contrase%C3%B1a-enviado-en-laravel
//https://stackoverflow.com/questions/29268071/how-to-make-a-restful-password-reminder-and-change-user-email-field-to-username
class AuthController extends Controller
{
    use SendsPasswordResetEmails;
    // use ResetsPasswords;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','logout', 'register','recover', 'reset']]);
    }

    public function recover(Request $request) {
        try {

            $this->validate($request, ['email' => 'required|email']);
            

            $response = $this->broker()->sendResetLink(
                $request->only('email', 'clubs_id')
            );
        } catch (\Exception $e) {
                    return response()->json(['status' => 'error', 'msg' => $e->getMessage()], 401);
                }
               
                return response()->json([
                    'status' => 'success', 'msg'=> '¡Se ha enviado un correo electrónico de reinicio! Por favor revise su correo electrónico.']);
    }

    public function reset(Request $request){

        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        
        $credentials = request(['clubs_id', 'email', 'password', 'password_confirmation', 'token']);
        
        $response = $this->broker()->reset(
            $credentials, function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response == \Password::PASSWORD_RESET
                    ? response()->json(['status' => 'success', 'msg' => 'Contraseña cambiada con éxito'], 200)
                    : response()->json(['status' => 'error', 'msg' => 'El validador no corresponde con ese correo','error' => ['email' => 'El validador no se corresponde para este correo']], 422);
    }
    
    protected function resetPassword($user, $password)
    {
        
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => str_random(60),
        ])->save();
 
        // GENERAR TOKEN PARA SATELLIZER AQUI ??
        // $this->guard()->login($user);
    }

    public function login(Request $request)
    {
        
        $login = $request->input('email');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'nick';
        
        // validación de los datos del formulario
        if($field == 'email'){
            $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:4|max:60',
            'clubs_id' => 'required'
            ],$messages=[]);
        $login = $request->input('email');
        }
        
        if($field == 'nick'){
            $request->merge(['nick' => $login]);
            $this->validate($request, [
                'nick'    => 'required',
                'password' => 'required|min:4|max:60',
                'clubs_id' => 'required'
            ],$messages=[]);
        $login = $request->input('nick');
            
        }
        

        $credentials = request(['clubs_id',$field, 'password']);
  
        // fecha/hora de expiración: en 5 min
        $expDate = Carbon::now()->addMinutes(5)->timestamp;

        // si el usuario marca la casilla "recuerdame/remember me"
        // cambiamos su fecha/hora de expiración
        $customClaims = $request->remember ? $customClaims = ['exp' => $expDate] : [];
  
        try {
            //if (!$token = JWTAuth::attempt($credentials, $customClaims)) {
            if (!$token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'Sin autorización'], 401);
            }
        } catch (JWTException $e) {
            return response()->json('could_not_create_token', $e->getStatusCode());
        }

        try{
            // verificamos que el usuario se haya registrado previamente
            $user = User::where($field, $login)->where('clubs_id','=', $request->clubs_id)->firstOrFail();
  
            // emitimos evento de Login
            event(new Login(config('auth.defaults.guard'), $user, true));

        } catch (Exception $e) {
            return response()->json('could_not_login_user', $e->getStatusCode());
        }
       
       

        return $this->respondWithToken($token);
    }

    public function logout(Request $request)
    {

        $user = $this->guard()->user();
    
        // emitimos evento de deslogado
        event(new Logout(config('auth.defaults.guard'), $user, true));
        try
        {
            $token = JWTAuth::getToken();
 
            JWTAuth::invalidate($token);
            auth('api')->logout();
            return response()->json(['status' => 'success', 'msg'=> '¡Vuelve pronto!']);

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'msg'=> '¡Fallo en la desconexión!'], $e->getStatusCode());
        };

    }


    public function register(Request $request, Recaptcha $recaptcha)
    {
        $club = $request->get('clubs_id');
       
        
        $validar = $this->validate($request, [
            'nombre' => 'required|string',
            'apellidos' => 'nullable|string',
            'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $club, //compruebo que el nick no esta registrado ya en ese club.
            'sexo' => 'required|in:HOMBRE,MUJER',
            'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
            'desplazamiento' => 'nullable|integer',
            'disponibilidad' => 'nullable|string',
            'telefono' => 'required|string|unique:users,telefono,null,id,clubs_id,' . $club, //compruebo que el nick no esta registrado ya en ese club.
            'poblacion_id' => 'required|integer|min:0',
            //'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,' . $request->get('nick'),
            'nivel_id' => 'required|integer|min:0',
            'comentarios' => 'nullable|string',
            'email' => 'required|string|email|max:255|unique:users,email,null,id,clubs_id,' . $club,
            'password' => 'required|string|min:4|confirmed',
            'recaptcha' => ['required', $recaptcha],
            ]);
        
                       
        if($validar){
            
            $create = User::create($request->except('avatar'));
            $user = User::find($create->id);
            $datosClub = Clubs::find($club);

            $user->update([
              'password' => bcrypt($request->get('password')),
              'remember_token' => base64_encode($request->get('email'))
            ]);

            if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(499,498, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $club . '/' . $create->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $club . '/' . $create->id . '/avatar/' . $filename;
                $user->update([
                   'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
                 ]); 

            }else{
                $avatar = ($request->get('sexo') == "Hombre" || $request->get('sexo') == "HOMBRE") ? '/img/avatar/avatarH.png' : '/img/avatar/avatarM.png';
                $user->update([
                    'avatar' => $avatar,
                ]);

            }
                 
                $newUser = new \stdClass();
                $newUser->club = $datosClub->nombre;
                $newUser->id = $datosClub->id;
                $newUser->nick = $request->get('nick');
                $newUser->sexo = $request->get('sexo');
                $newUser->email = $request->get('email');
                $newUser->password = $request->get('password');
                $newUser->nombre = $request->get('nombre');
                $newUser->logo = $datosClub->logo;
                $newUser->url = $datosClub->web;
                $newUser->email_web = $datosClub->email_web;
                $newUser->responsable = $datosClub->responsable;

                Mail::to($request->get('email'))->send(new VerificationEmail($newUser));

            return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);
        }
    }
    
    public function updateUser(Request $request, $id)
    {
      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,'. $id .',id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'required|string|unique:users,telefono,' . $id . ',id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,1',
        'nivel_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255|unique:users,email,' . $id . ',id,clubs_id,' . $request->get('clubs_id'),
      ]);

      $jugador = $this->guard()->user();
      
 
      if($jugador->count() && $validar){

        $jugador->update($request->except(['avatar', 'password']));

          //si el campo del password viene relleno, lo validamos y actualizamos datos
        if($request->get('password') != 'null' && $request->get('password') != '' && $request->get('password') != null){
          
          $validar = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
          ]);

          if($validar){
            $jugador->update(['password' => bcrypt($request->get('password'))]);
          };
         

          
        }

        if($request->hasFile('avatar')){

          $file = $request->file('avatar'); //recupero la imagen

          $filename = $file->getClientOriginalName();

          $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
          $img->resize(499,498, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
            $constraint->aspectRatio();                 
          });
          $img->stream(); //convierto la nueva imagen
          
          $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
          $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename;
          $jugador->update([
             'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
           ]); 

        }

        return response()->json(['status'=>'success','msg'=> $jugador['nombre'] . ' actualizada con éxito']);
      }else{
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $jugador['nombre'] ]);
      }
    }
    
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    protected function respondWithToken($token)
    {
        $user = DB::table('users')
                ->join('niveles','niveles.id','users.nivel_id')
                ->join('poblaciones','poblaciones.id','users.poblacion_id')
                ->select('users.apellidos','users.avatar','users.clubs_id','users.desplazamiento','users.disponibilidad','users.email','users.id','users.nick','niveles.nombre as nivel','users.nivel_id','users.nombre','users.notificaciones','users.poblacion_id','users.posicion','poblaciones.postal as postal', 'poblaciones.nombre as poblacion','users.rol_id','users.sexo','users.telefono')
                ->where('users.id', $this->guard()->user()->id)
                ->first();

        $user->avatar = ($user->avatar === null || $user->avatar === '') ? $user->sexo == 'HOMBRE'? "../img/avatar/avatarH.png":"../img/avatar/avatarM.png" : $user->avatar;
    
        return response()->json([
            'access_token' => $token,
            'user'		=> $user,	
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);

    }

    public function guard(){
    	return \Auth::Guard('api');
    }
    
    public function broker()
    {
        return \Password::broker('users');
    }

}