<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ControllerCleaner extends Controller
{
    public function index(){
        return view('layouts.clean');
    }
    public function cache(){
    	$exitCode = \Artisan::call('cache:clear');
    	return redirect()->back()->with('status', 'Cache vacia'); 
    }
    public function optimize(){
    	$exitCode = \Artisan::call('optimize');
    	return redirect()->back()->with('status', 'Optimizado'); 
    }
    public function routeCache(){
    	$exitCode = \Artisan::call('route:cache');
    	return redirect()->back()->with('status', 'Cache de ruta reiniciada'); 
    }
    public function routeClear(){
    	$exitCode = \Artisan::call('route:clear');
    	return redirect()->back()->with('status', 'Rutas reiniciadas'); 
    }
    public function view(){
    	$exitCode = \Artisan::call('view:clear');
    	return redirect()->back()->with('status', 'Vistas reiniciadas'); 
    }
    public function config(){
    	$exitCode = \Artisan::call('config:cache');
    	return redirect()->back()->with('status', 'Configuracion reiniciada'); 
    }
    

}
