<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Entrenador;
use Auth;
use Image;

class EntrenadorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request)
    {
       
        if (!$request->ajax()) return redirect('/');
        $busqueda = $request->get('busqueda');
        $activos = $request->get('activos');
        $club = Auth::user()->id;
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        $entrenadores = Entrenador::where('club_id',1);

        if($busqueda != '' || $busqueda != null){
            $entrenadores = $entrenadores->where('nombre','like','%'. $busqueda . '%');
        }

        if($activos == 'true'){
            $entrenadores = $entrenadores->where('activo',1);
        }

        $entrenadores = $entrenadores->orderBy('nombre',$orden)->paginate(10);
        $response = [
            'pagination' => [
            'total' => $entrenadores->total(),
            'per_page' => $entrenadores->perPage(),
            'current_page' => $entrenadores->currentPage(),
            'last_page' => $entrenadores->lastPage(),
            'from' => $entrenadores->firstItem(),
            'to' => $entrenadores->lastItem()
            ],

            'items' => $entrenadores,
        ];   

        return response()->json($response);


    }

    public function store(Request $request)
    {
     
        $club = Auth::user();
        $request->merge([
            'club_id' => $club->id,
            'activo' => 1,
            ]);
        
        
        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:entrenadores,nombre,null,id,club_id,' . $club->id,
        ]);
        
        if($validar){
            $create = Entrenador::create($request->except('avatar'));
            $entrenador = Entrenador::findOrFail($create->id);

            if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen
      
                $filename = $file->getClientOriginalName();
      
                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(400,360, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' .  $club->id . '/entrenadores/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = '../image' . '/club/' .  $club->id . '/entrenadores/' . $filename;
                
                $entrenador->update(['avatar' => $rutaImagen]);
            }
            
            return response()->json(['status'=>'success','msg'=> $entrenador['nombre'] . ' creado con éxito']);
        }else{
            return response()->json(['status'=>'error','msg'=>'Error creando ' . $entrenador['nombre'] ]);
        }

    }

    public function update(Request $request, $id){
        $club = Auth::user();
        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:entrenadores,nombre,'. $id .',id,club_id,' . $request->get('clubs_id'),
        ]);

        $entrenador = Entrenador::findOrFail($id);

        

        if($validar && $entrenador->count()){
            $entrenador->update($request->except('avatar'));
            
            if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen
      
                $filename = $file->getClientOriginalName();
      
                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(400,360, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $request->get('club_id') . '/entrenadores/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = '../image' . '/club/' . $request->get('club_id') . '/entrenadores/' . $filename;
                
                $entrenador->update(['avatar' => $rutaImagen]);
            }
            
            return response()->json(['status'=>'success','msg'=> $entrenador['nombre'] . ' actualizado con éxito']);
        }else{
            return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $entrenador['nombre'] ]);
        }
        
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Entrenador::findOrFail($request->id);
        $user->activo = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Entrenador::findOrFail($request->id);
        $user->activo = '1';
        $user->save();
    }
  

}
