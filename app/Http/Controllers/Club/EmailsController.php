<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
//imagen
use Carbon\Carbon; 
use Image;
use Illuminate\Support\Facades\Storage;

//email
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\Emails;

class EmailsController extends Controller
{
   	public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request)
    {
        $club = Auth::user();
        $busqueda = $request->get('busqueda');
        $desde = $request->get('desde');
        $hasta = $request->get('hasta');
        
    	$userMails = User::where('clubs_id',$club->id)
    				->where(function($query) use ($busqueda){
    					$query->where('nick', 'like', '%'. $busqueda .'%')
    						  ->orWhere('email', 'like', '%'. $busqueda .'%');
    				})
                    ->whereBetween('nivel_real_id',array($desde,$hasta))
    				->select('email','nick')
    				->orderBy('nick', 'ASC')
    				->paginate(30);
    	$todos = User::where(function($query) use ($busqueda){
                        $query->where('nick', 'like', '%'. $busqueda .'%')
                              ->orWhere('email', 'like', '%'. $busqueda .'%');
                    })
                    ->whereBetween('nivel_real_id',array($desde,$hasta))
                    ->select('email')
                    ->get();
        $plantillas = Emails::where('club_id',$club->id)->orderBy('titulo','ASC')->get();

        $response = [
	        'pagination' => [
	          'total' => $userMails->total(),
	          'per_page' => $userMails->perPage(),
	          'current_page' => $userMails->currentPage(),
	          'last_page' => $userMails->lastPage(),
	          'from' => $userMails->firstItem(),
	          'to' => $userMails->lastItem()
	        ],

        	'items' => $userMails,
        	'todos' => $todos,
        	'plantillas' => $plantillas,
      	];   

        return response()->json($response);
    }

    public function store(Request $request){
    	$club = Auth::user();
    	$request->merge(['club_id' => $club->id]);

        

        $plantilla = Emails::find($request->get('id'));
        
    	if($plantilla){

            $validar = $this->validate($request, [
                'titulo' => 'required|string|unique:emails,titulo,' . $request->get('id') . ',id,club_id,' . $club->id
            ]);
            
            if($validar){
                if($plantilla->titulo == $request->get('titulo')){
                    $plantilla->update($request->all());
                    return response()->json(['msg' => $plantilla->titulo . ' actualizado con éxito']);
                }else{
                     $create = Emails::create($request->all());
                    return response()->json(['msg' => 'Plantilla creada con éxito']);
                }
                
            }
    		
    	}else{

            $validar = $this->validate($request, [
                'titulo' => 'required|string|unique:emails,titulo,null,id,club_id,' . $club->id
            ]);

            if($validar){
                $create = Emails::create($request->all());
                return response()->json(['msg' => 'Plantilla creada con éxito']);
            }
    		
    	}
    }
    public function destroy($id){
    	$plantilla = Emails::find($id);

      	if($plantilla->count()){
        	$plantilla->delete();
        	return response()->json(['status'=>'success','msg'=> $plantilla['titulo'] . ' eliminada con éxito']);
      	} else {
        	return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $plantilla['titulo'] ]);
      	}
    }
    
    public function subirImagen(Request $request){
        $club = Auth::user();
        $date = Carbon::now()->toDateString();  
        $img = '';
        $path = '';
        if($request->hasFile('image')){
            //$patrocinador = Patrocinadores::find($create->id); 
            $file = $request->file('image'); //recupero la imagen

            //$filename = $file->getClientOriginalName() . str_replace("-","", $date);
            $filename = str_replace("-","", $date) . "-" . $file->getClientOriginalName();

            $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
            $img->resize(300,null, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
            });
            $img->stream(); //convierto la nueva imagen

            $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/emails/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
            $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/emails/' . $filename;
        };
        
        $response = [
            'imagen' => $filename,
            'url'    => substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/emails'
        ];   
        return response()->json($rutaImagen);
    }

    public function enviar(Request $request){
		$club = Auth::user();
    	//$mensaje = new \stdClass();
  		$mensaje = $request->get('mensaje');
   		$emails = $request->get('emails');
        $enviar = sizeof($emails);
		$send = 0;
   		$str = 'sincorreo';
        foreach($emails as $email){
            if (strpos($email, $str) == false) {
                Mail::to($email)->send(new SendEmail($club, $mensaje)); 
                $send = $send + 1;
            }
        }
    	
        return response()->json(['msg' => $enviar == 1 ? $send . ' Email enviado con éxito': $send .'/' . $enviar . ' Emails enviados con éxito']);
    }
    public function queue(){
        $activateQeue = \Artisan::call('queue:work');
        //$command = 'php ' . base_path() . '/artisan queue:work --timeout=60 --sleep=5 --tries=3 > /dev/null & echo $!'; // 5.6 - see comments
        //$pid = exec($command);
        //return $pid;
    }
    
    public function plantillas(){
        $club = Auth::user();

        $plantillas = Emails::where('club_id',$club->id)->orderBy('titulo','ASC')->get();

        return response()->json($plantillas);
    }
}
