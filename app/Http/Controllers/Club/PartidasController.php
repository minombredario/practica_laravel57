<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Auth;
use App\Clubs;
use App\Partidas;
use App\Poblaciones;
use App\Provincias;
use App\Torneos;
use App\PartidasFijas;
use Image;
use App\PartidasUsuario;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PartidasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }


    public function index(Request $request)
    {
        $busqueda = $request->get('busqueda');
        $id = $request->get('user_id');
        $tipo = $request->get('tipo');

        if ($tipo == 'todo' || is_numeric($id)) {

            $semana = $request->get('semana');
            $club_id = Auth::user()->id;
            $fecha = $request->get('dia');
            $vista = $request->get('vista');

            $partidas = Partidas::BusquedaPartidaClub2($fecha, $vista, $club_id, $id);

            return response()->json($partidas);

        } else {

            $sortBy = $request->get('sortBy') == '' ? 'nombre' : $request->get('sortBy');
            $orden = $request->get('orden') == '' ? 'ASC' : $request->get('orden');
            $semana = $request->get('semana');
            $id = Auth::user()->id;

            $partidas = Partidas::busqueda($busqueda, $sortBy, $orden, $semana, $id);

            $response = [
                'pagination' => [
                    'total' => $partidas->total(),
                    'per_page' => $partidas->perPage(),
                    'current_page' => $partidas->currentPage(),
                    'last_page' => $partidas->lastPage(),
                    'from' => $partidas->firstItem(),
                    'to' => $partidas->lastItem(),
                ],

                'items' => $partidas,

            ];

            return response()->json($response);

        }

    }

    public function store(Request $request)
    {

        //'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        $club = Auth::user()->id;
        $request->merge(['clubs_id' => $club]);

        $validar = $this->validate($request, [
            'clubs_id' => 'required|integer|unique:partidas,clubs_id,null,id,pistas_id,'.$request->get(
                    'pistas_id'
                ).',inicio_alquiler,'.$request->get('inicio_alquiler'),
            'pistas_id' => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'.$club.',inicio_alquiler,'.$request->get(
                    'inicio_alquiler'
                ),
            'nivelPartida_id' => 'required|integer',
            'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'.$club.',pistas_id,'.$request->get(
                    'pistas_id'
                ),
            'fin_alquiler' => 'required|date',
            'status' => 'required|in:ABIERTA,CERRADA',
            'abierta_por' => 'required|in:CLUB,USUARIO,INVITADO',
            'partida_fija' => 'required|in:SI,NO',
            'equipo' => 'required|in:SI,NO',
            'torneo' => 'required|in:SI,NO',
            'clase' => 'required|in:SI,NO',
        ]);

        if ($validar) {

            try {
                DB::beginTransaction();
                $rutaImagen = '';

                $partida = Partidas::create($request->all());

                if ($request->hasFile('cartel')) {

                    $file = $request->file('cartel'); //recupero la imagen

                    $filename = $file->getClientOriginalName();

                    $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                    $img->resize(
                        400,
                        400,
                        function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                            $constraint->aspectRatio();
                        }
                    );
                    $img->stream(); //convierto la nueva imagen

                    $path = Storage::disk('public')->put(
                        'image'.'/club/'.$club.'/ligas/'.$filename,
                        $img
                    ); // almaceno la ruta de la imagen en una variable
                    $rutaImagen = substr(asset($path), 0, -1).'image'.'/club/'.$club.'/ligas/'.$filename;

                    $partida->update([
                        'cartel' => $rutaImagen,
                    ]);
                };

                $jugadoresPartida = $request->get('jugadoresPartida');

                if ($rutaImagen == '' && $request->get('torneo') == 'NO') {
                    $partidaUsers = PartidasUsuario::create([
                        'club_id' => $club,
                        'partida_id' => $partida->id,
                        'pala_id' => $jugadoresPartida['pala_id'] == 0 ? null : $jugadoresPartida['pala_id'],
                        'pala' => $jugadoresPartida['pala'],
                        'pala2_id' => $jugadoresPartida['pala2_id'] == 0 ? null : $jugadoresPartida['pala2_id'],
                        'pala2' => $jugadoresPartida['pala2'],
                        'pala3_id' => $jugadoresPartida['pala3_id'] == 0 ? null : $jugadoresPartida['pala3_id'],
                        'pala3' => $jugadoresPartida['pala3'],
                        'pala4_id' => $jugadoresPartida['pala4_id'] == 0 ? null : $jugadoresPartida['pala4_id'],
                        'pala4' => $jugadoresPartida['pala4'],
                    ]);
                }

                if ($request->get('partida_fija') == 'SI' && !$request->get('partidaVirtual') && !$request->get('fija_id')) {

                    $fija = PartidasFijas::create([
                        'club_id' => $club,
                        'partida_id' => $partida->id,
                        'jugadores_fijos' => json_encode($jugadoresPartida),
                    ]);

                      $partida->update([
                          'fija_id' => $fija->id,
                      ]);
                }

                if ($request->get('partida_fija') == 'SI' && $request->get('partidaVirtual')) {

                    /** en este caso no hacemos nada con las partidas fijas*/
                    $this->updateFijos($request->get('fija_id'), $jugadoresPartida );
                }

                if ($request->get('torneo') == 'SI' && $request->get('torneo_id') == 0) {

                    $torneo = Torneos::create([
                        'club_id' => $partida->clubs_id,
                        'partida_id' => $partida->id,
                        'nombre' => "Torneo ".$partida->inicio_alquiler." | pista ".$partida->pistas_id,
                        'fecha' => $partida->inicio_alquiler,
                        'visible' => 1,
                    ]);
                    $partida->update([
                        'torneo_id' => $torneo->id,
                    ]);
                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();

                return response()->json($e);
            }
        }

        return response()->json(['status' => 'success', 'msg' => $request->get('nombre').' creada con éxito']);

    }

    public function show($id)
    {

        $partida = Partidas::BuscarPartida($id);

        return response()->json($partida);

    }

    public function update(Request $request, $id)
    { //unique:users,clubs_id,null,id,nick,1',

        $club = Auth::user()->id;
        $request->merge(['clubs_id' => $club]);
        $date = Carbon::today();

        $validar = $this->validate($request, [
            'pistas_id' => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'.$club.',inicio_alquiler,'.$id,
            'nivelPartida_id' => 'required|integer',
            'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'.$club.',pistas_id,'.$id,
            'fin_alquiler' => 'required|date',
            'status' => 'required|in:ABIERTA,CERRADA',
            'abierta_por' => 'required|in:CLUB,USUARIO,INVITADO',
            'partida_fija' => 'required|in:SI,NO',
            'equipo' => 'required|in:SI,NO',
            'torneo' => 'required|in:SI,NO',
            'clase' => 'required|in:SI,NO',
        ]);

        $jugadoresPartida = $request->get('jugadoresPartida');
        $partida = Partidas::find($id);

        if ($partida->count() && $validar) {
            try {
                DB::beginTransaction();
                $rutaImagen = '';
                if ($request->hasFile('cartel')) {

                    $file = $request->file('cartel'); //recupero la imagen

                    $filename = $file->getClientOriginalName();

                    $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                    $img->resize(
                        400,
                        400,
                        function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                            $constraint->aspectRatio();
                        }
                    );
                    $img->stream(); //convierto la nueva imagen

                    $path = Storage::disk('public')->put(
                        'image'.'/club/'.$club.'/ligas/'.$filename,
                        $img
                    ); // almaceno la ruta de la imagen en una variable
                    $rutaImagen = substr(asset($path), 0, -1).'image'.'/club/'.$club.'/ligas/'.$filename;
                };

                $partida->update($request->except('cartel', 'id'));

                if ($request->get('torneo') == 'SI' && $rutaImagen != '') {
                    $partida->update([
                        'cartel' => $rutaImagen,
                    ]);
                }

                if ($request->get('torneo') == 'NO' && $request->get('equipo') == 'NO' && $request->get(
                        'clase'
                    ) == 'NO') {
                    $userPartida = PartidasUsuario::where('partida_id', $partida->id)->first();

                    if ($userPartida) {

                        $userPartida->update([
                            'partida_id' => $partida->id,
                            'pala_id' => $jugadoresPartida['pala_id'] == 0 ? null : $jugadoresPartida['pala_id'],
                            'pala' => $jugadoresPartida['pala'],
                            'pala2_id' => $jugadoresPartida['pala2_id'] == 0 ? null : $jugadoresPartida['pala2_id'],
                            'pala2' => $jugadoresPartida['pala2'],
                            'pala3_id' => $jugadoresPartida['pala3_id'] == 0 ? null : $jugadoresPartida['pala3_id'],
                            'pala3' => $jugadoresPartida['pala3'],
                            'pala4_id' => $jugadoresPartida['pala4_id'] == 0 ? null : $jugadoresPartida['pala4_id'],
                            'pala4' => $jugadoresPartida['pala4'],
                        ]);
                    }
                }

                if ($request->get('partida_fija') == 'NO' && ($request->get('fija_id') != null || $request->get(
                            'fija_id'
                        ) != "")) {
                    $fija = PartidasFijas::find($request->get('fija_id'));

                    if ($fija->count()) {
                        $fija->delete();

                        $partida->update([
                            'fija_id' => null,
                        ]);
                    }
                }

                if ($request->get('partida_fija') == 'SI' && $request->get('fija_id') == null) {

                    $fija = PartidasFijas::create([
                        'club_id' => $club,
                        'partida_id' => $partida->id,
                        'jugadores_fijos' => json_encode($jugadoresPartida),
                    ]);
                    $partida->update([
                        'fija_id' => $fija->id,
                    ]);

                }

                if ($request->get('partida_fija') == 'SI' && $request->get('fija_id') != null) {
                    $this->updateFijos($request->get('fija_id'),$jugadoresPartida );
                }

                if ($request->get('torneo') == 'SI' && $request->get('torneo_id') == 0) {

                    $torneo = Torneos::create([
                        'club_id' => $partida->clubs_id,
                        'partida_id' => $partida->id,
                        'nombre' => "Torneo ".$partida->inicio_alquiler." | pista ".$partida->pistas_id,
                        'fecha' => $partida->inicio_alquiler,
                        'visible' => 1,
                    ]);
                    $partida->update([
                        'torneo_id' => $torneo->id,
                    ]);

                    $userPartida = PartidasUsuario::where('partida_id', $partida->id)->first();
                    $userPartida->count() ? $userPartida->delete() : '';
                }

                DB::commit();

                return response()->json(
                    ['status' => 'success', 'msg' => $partida['inicio_alquiler'].' actualizada con éxito']
                );
            } catch (Exception $e) {
                DB::rollBack();

                return response()->json($e);
            }
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Error actualizando '.$partida['inicio_alquiler']]);
        }


    }

    public function destroy($id)
    {

        $partida = Partidas::find($id);
        $torneo = DB::table('lista_espera')->where('user_id', $id)->get();
        $fija = PartidasFijas::where('partida_id', $partida->id)->where('id', $partida->fija_id)->first();

        if ($partida->count()) {
            try {
                DB::beginTransaction();

                ($partida->partida_fija == 'NO' && $partida->fija_id != null && $fija->count()) ? $fija->delete() : '';

                if ($partida->torneo == 'SI') {
                    $torneo = Torneos::find($partida->torneo_id);
                    $userEspera = DB::table('lista_espera')->where('torneo_id', $torneo->id)->get();
                    $userTorneo = DB::table('torneo_users')->where('torneo_id', $torneo->id)->get();
                    $userAnonimoTorneo = DB::table('torneo_users_anonimos')->where('torneo_id', $torneo->id)->get();

                    if ($userEspera->count()) {
                        DB::table('lista_espera')->where('torneo_id', $torneo->id)->delete();
                    }

                    if ($userTorneo->count()) {
                        DB::table('torneo_users')->where('torneo_id', $torneo->id)->delete();
                    }

                    if ($userAnonimoTorneo->count()) {
                        DB::table('torneo_users_anonimos')->where('torneo_id', $torneo->id)->delete();
                    }

                    $arrayCartel = explode("/", $partida->cartel);
                    if (count($arrayCartel)) {
                        $cartel = array_pop($arrayCartel);
                        $path = $arrayCartel[3]."/".$arrayCartel[4]."/".$arrayCartel[5]."/".$arrayCartel[6]."/".$cartel;
                        @unlink($path);
                    }
                };


                $partida->delete();

                //DB::delete("DELETE FROM torneos WHERE partida_id='" . $id . "';");


                DB::commit();

                return response()->json(['status' => 'success', 'msg' => $partida['nombre'].' eliminada con éxito']);
            } catch (Exception $e) {
                DB::rollBack();
            }
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Error eliminando '.$partida['nombre']]);
        }
    }

    public function updatePos(Request $request, $id)
    {
        //unique:users,clubs_id,null,id,nick,1',
        $club = Auth::user();

        $validar = $this->validate($request, [
            'pistas_id' => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'.$club->id.',inicio_alquiler,'.$id,
            'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'.$club->id.',pistas_id,'.$id,
            'fin_alquiler' => 'required|date',
        ]);


        $partida = Partidas::find($id);

        if ($partida->count() && $validar && !$request->get('partidaVirtual')) {
            try {
                DB::beginTransaction();
                $partida->update([
                    'pistas_id' => $request->get('pistas_id'),
                    'inicio_alquiler' => $request->get('inicio_alquiler'),
                    'fin_alquiler' => $request->get('fin_alquiler'),
                ]);
                DB::commit();

                return response()->json(
                    ['status' => 'success', 'msg' => $partida['inicio_alquiler'].' actualizada con éxito']
                );

            } catch (Exception $e) {
                DB::rollBack();

                return response()->json(['status' => 'error', 'msg' => $e]);
            }
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Error actualizando '.$partida['inicio_alquiler']]);
        }
    }

    public function getUsers()
    {
        $usuarios = \Users::get();

        return response()->json($usuarios);
    }

    private function updateFijos($fija_id, $jugadoresPartida){
        $fija = PartidasFijas::find($fija_id);
        $jugadoresFijos = json_decode($fija->jugadores_fijos, true);
        $actualizarFijos = false;
        if ($jugadoresFijos) {

            // Verifica si el jugador tiene pala_fija y actualiza en la base de datos si es necesario
            foreach (['pala', 'pala2', 'pala3', 'pala4'] as $palaKey) {
                if ($jugadoresPartida[$palaKey.'_fija'] &&
                    ($jugadoresPartida[$palaKey.'_id'] !== $jugadoresFijos[$palaKey.'_id'] ||
                        $jugadoresPartida[$palaKey] !== $jugadoresFijos[$palaKey])
                ) {
                    $actualizarFijos = true;
                    $jugadoresFijos[$palaKey.'_id'] = $jugadoresPartida[$palaKey.'_id'];
                    $jugadoresFijos[$palaKey] = $jugadoresPartida[$palaKey];
                    $jugadoresFijos[$palaKey.'_fija'] = true;
                }
            }


            if ($actualizarFijos) {
                $fija->update([
                    'jugadores_fijos' => json_encode($jugadoresFijos),
                ]);
            }

        }
    }
}
