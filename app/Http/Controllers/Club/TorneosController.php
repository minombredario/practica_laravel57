<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Torneos;
use App\TorneoUsers;
use App\TorneoUsersAnonimos;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class TorneosController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request)
    {
        $club = Auth::user();
        $busqueda = $request->get('busqueda');
        $torneos = DB::table('torneos')->where('club_id', '=', $club->id)->where('nombre', 'like', '%' . $busqueda . '%')->orderBy('fecha', 'DESC')->paginate(8);

        foreach($torneos as $torneo){
            $users = DB::table('torneo_users')
                ->join('users', 'users.id','torneo_users.user_id')
                //->join('niveles', 'users.nivel_id', '=', 'niveles.id')
                ->select('torneo_users.id', 'users.nick', 'users.posicion', 'torneo_users.user_id')
                ->where('torneo_users.torneo_id', '=', $torneo->id)
                ->get();
                $torneo->users = $users;
        }

        $response = [
            'pagination' => [
                'total' => $torneos->total(),
                'per_page' => $torneos->perPage(),
                'current_page' => $torneos->currentPage(),
                'last_page' => $torneos->lastPage(),
                'from' => $torneos->firstItem(),
                'to' => $torneos->lastItem()
            ],
            
            'items' => $torneos,
            
        ];

        return response()->json($response);
    }

    public function getTorneos()
    {
        $date = Carbon::today();
        $date = Carbon::parse($date, 'Europe/Madrid')->addHours(Carbon::parse($date, 'Europe/Madrid')->offsetHours);
        $club = Auth::user();
        $torneos = Torneos::where('club_id', $club->id)->whereDate('fecha', '>=', $date->format('Y-m-d'))->get();

        return response()->json($torneos);
    }

    public function store(Request $request)
    {
        $club = Auth::user();
        $request->merge(['club_id' => $club->id]);

        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:torneos,nombre,null,id,club_id,' . $club->id,
        ]);

        if($validar){
            
            $create = Torneos::create($request->all());
            
            return response()->json(['status' => 'success','msg'=> 'Torneo ' . $request->get('nombre') . ' creado con éxito']);
        }   

    }

    public function update(Request $request, $id)
    {
        $club = Auth::user();
        $request->merge(['club_id' => $club->id]);
        $torneo = Torneos::find($id); 

        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:torneos,nombre,'. $id .',id,club_id,' . $club->id, 
        ]);

        if($validar){
            $torneo->update($request->all());

            return response()->json(['status' => 'success','msg'=> 'Torneo ' . $request->get('nombre') . ' actualizada con éxito']);
        }
        
    }

    public function destroy($id)
    {
      $torneo = Torneos::find($id);
      $userEspera = DB::table('lista_espera')->where('torneo_id', $torneo->id)->get();   
      $userTorneo = DB::table('torneo_users')->where('torneo_id', $torneo->id)->get();  
      $userAnonimoTorneo = DB::table('torneo_users_anonimos')->where('torneo_id', $torneo->id)->get(); 
      $partidas = DB::table('partidas')->where('torneo_id', $torneo->id)->get();  

      if($torneo->count()){
        try{
            DB::beginTransaction();

            if($userEspera->count()){
                DB::table('lista_espera')->where('torneo_id', $torneo->id)->delete();
            }

            if($userTorneo->count()){
            DB::table('torneo_users')->where('torneo_id', $torneo->id)->delete();
            }

            if($userAnonimoTorneo->count()){
            DB::table('torneo_users_anonimos')->where('torneo_id', $torneo->id)->delete();
            }

            if($partidas->count()){
                  
                $partida = DB::table('partidas')->where('torneo_id', $torneo->id)->first(); 
                $arrayCartel = explode("/", $partida->cartel);
                if($arrayCartel){
                    $cartel = array_pop($arrayCartel);
                    $path = $arrayCartel[3] . "/" . $arrayCartel[4] . "/" . $arrayCartel[5] . "/" . $arrayCartel[6] . "/" . $cartel;
                    @unlink($path);
                }
                DB::table('partidas')->where('torneo_id', $torneo->id)->delete();
            }

            $torneo->delete();

            DB::commit();
            return response()->json(['status'=>'success','msg'=> $torneo['nombre'] . ' eliminada con éxito']);
        } catch (Exception $e){
            DB::rollBack();
        }
      } else {
        return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $torneo['nombre'] ]);
      }
    }

    public function destroyUserRegistrado($id){

        $user = TorneoUsers::find($id);
       
        if($user->count()){
        $user->delete();
            return response()->json(['status'=>'success','msg'=> $user['nick'] . ' eliminada con éxito']);
        } else {
            return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $user['nick'] ]);
        }
    }

    public function destroyUserAnonimo($id){
        
        $user = TorneoUsersAnonimos::find($id);
       
        if($user->count()){
        $user->delete();
            return response()->json(['status'=>'success','msg'=> $user['nick'] . ' eliminada con éxito']);
        } else {
            return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $user['nick'] ]);
        }
    }

    public function usersTorneo($id){
        $jugadores = [];

        $users = DB::table('torneo_users')
                ->join('users', 'users.id', 'torneo_users.user_id')
                ->where('torneo_id',$id)
                ->select('torneo_users.id', 'users.nick', 'users.posicion','torneo_users.user_id')
                ->get();
        $anonimos = DB::table('torneo_users_anonimos')
                ->where('torneo_id',$id)
                ->select('id', 'user as nick', 'posicion')
                ->get();
        foreach ($users as $user){ 
            array_push($jugadores, $user);
        }
        foreach ($anonimos as $user){ 
            array_push($jugadores, $user);
        }  
        return response()->json($jugadores);
    }

    public function addUserTorneo(Request $request){
        
        $jugador = $request->get('jugador');
        $club = Auth::user();

        if($jugador == 'anonimo'){
            $validar = $this->validate($request, [
                'user' => 'required|string|unique:torneo_users_anonimos,user,'. $request->get('user') .',id,club_id,' . $club->id, 
            ]);
            $usersTorneo = TorneoUsersAnonimos::create([
                'torneo_id' => $request->get('torneo_id'),
                'club_id'   => $club->id,
                'user'    => $request->get('user'),
                'posicion'    => $request->get('posicion')
            ]);

            return response()->json(['status'=>'success','msg'=> $request->get('user') . ' agregado con éxito']);

        }else if($jugador == 'registrado'){
            $validar = $this->validate($request, [
                'user' => 'required|unique:torneo_users,user_id,'. $request->get('user') .',id,club_id,' . $club->id, 
            ]);
            $usersTorneo = TorneoUsers::create([
                'torneo_id' => $request->get('torneo_id'),
                'club_id'   => $club->id,
                'user_id'    => $request->get('user')
            ]);

            $user = User::find($request->get('user'));

            return response()->json(['status'=>'success','msg'=> $user['nick'] . ' agregado con éxito']);
        }

        
    }
}
