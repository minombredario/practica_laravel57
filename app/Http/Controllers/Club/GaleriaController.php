<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Auth;

class GaleriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function listaGalerias(){
    	$club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias';
        $carpetas = [];
        if(!$exists = file_exists($path)){
            @mkdir($path);
        }
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($path);
        
        // Recorre todos los elementos del directorio
	    while (($archivo = readdir($gestor)) !== false)  {
	    	if ($archivo != "." && $archivo != "..") {
	    		$carpeta = ['nombre' => $archivo];
       			array_push($carpetas, $carpeta);
       		}
       	};
        return response()->json($carpetas);	
    }

    public function crearGaleria(Request $request){
        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias';
        $fd = 'public/image/club/' . $club->id . '/galerias/';
        if($exists = file_exists( $path . "/" . $request->get('galeria'))){
            return response()->json(['status' => 'warning','msg'=>'Ya existe una galería con ese nombre']);
        }else{
            @mkdir($path . "/" . $request->get('galeria'));
        
            return response()->json(['status' => 'success','msg'=>'Galería creada con exito']);
        }
        
    }

    public function galeria($galeria){
    	$club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias/' . $galeria;
        $pathImag = '../image/club/' . $club->id . '/galerias/' . $galeria;
        $imagenes = [];

        // Abre un gestor de directorios para la ruta indicada
        $gestor = @opendir($path);
        
        // Recorre todos los elementos del directorio
	    while (($archivo = readdir($gestor)) !== false)  {
	    	if ($archivo != "." && $archivo != "..") {
	    		array_push($imagenes, $pathImag . "/" . $archivo);
       		}
       	};
        return response()->json($imagenes);	
    }

    public function eliminarImagen($galeria, $imagen){
        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias/' . $galeria;
       
        @unlink($path . '/' . $imagen);
        
        return response()->json(['status' => 'success','msg'=>'Imágen eliminada']);
    }

    public function eliminarGaleria($galeria){
        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias/' . $galeria;
       
        $this->deleteDirectory($path);
        
        return response()->json(['status' => 'success','msg'=>'Galería eliminada']);
    }

    public function subirImagenes(Request $request){
        $club = Auth::user();
        $imagenes = $request->file('items');
        $galeria = $request->get('folder');
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/galerias/' . $galeria;

        foreach ($imagenes as $key => $imagen) {

            $imagen->move($path, $imagen->getClientOriginalName());
        }

        
        try{
            return response()->json(array(
                'success' => true,
                'data' => [],
                'errors' => [],
                'msg' => 'Imágen subida con exito'
            ),200);
        } catch(\Exception $e){
            return response()->json(array(
                'success' => false,
                'data'  => "Error en el servidor",
                'errors' => $e
            ),200);
        };
        
    }

    public function update(Request $request, $galeria){
        $old = $request->get('old')['nombre'];
        $new = $request->get('new')['nombre'];

        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path_old = $ruta . 'image/club/' . $club->id . '/galerias/' . $old;
        $path_new = $ruta . 'image/club/' . $club->id . '/galerias/' . $new;
        
        @rename ($path_old, $path_new);

        return response()->json(['status' => 'success','msg'=>'Galería actualizada']);
    }

    function deleteDirectory($path) {
        $gestor = @opendir($path);

        if(!$gestor) return;

        while (($archivo = readdir($gestor)) !== false)  {
            if ($archivo != "." && $archivo != "..") {
                if (!@unlink($path.'/'.$archivo)){
                    deleteDirectory($path.'/'.$archivo);
                }
            }      
        }
        closedir($gestor);

        @rmdir($path);
       
    }

}
