<?php

namespace App\Http\Controllers\Club;

use Auth;
use App\ListaEspera;
use App\User;
use App\PartidasUsuario;
use App\Partidas;
use App\Torneos;
use App\TorneoUsers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EsperaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth:club');
    }

    public function store(Request $request){
     

        $partida = PartidasUsuario::where('partida_id',$request->get('partida_id'));
        $partida_ = Partidas::find($request->get('partida_id'));
        

        if($request->get('torneo_id') || $partida_->torneo == 'SI'){

            if(!$request->get('torneo_id')){
              $request->merge(['nombre'=> "Torneo " . $partida_->inicio_alquiler]);

              $validar = $this->validate($request, [
              'nombre'   => 'required|string|unique:torneos,nombre,null,id,club_id,' . $request->get('club_id'),      
              ]);
              if($validar){
                $torneo = Torneos::create([
                        'club_id'   => $partida_->clubs_id,
                        'nombre'    => "Torneo " . $partida_->inicio_alquiler,
                        'fecha'     => $partida_->inicio_alquiler,
                        'visible'   => 1
                      ]);
                $usersTorneo = TorneoUsers::create([
                          'torneo_id' => $torneo->id,
                          'club_id'   => $torneo->club_id,
                          'user_id'    => $request->get('user_id')
                          ]);

              }


            }else{
              $usersTorneo = TorneoUsers::create([
                  'torneo_id' => $request->get('torneo_id'),
                  'club_id'   => $request->get('club_id'),
                  'user_id'    => $request->get('user_id')
                ]);
            }



        }




        
        if($partida->count() && $partida_->torneo == 'NO'){
          $partida->update($request->only(['pala_id', 'pala','pala2_id', 'pala2','pala3_id', 'pala3','pala4_id', 'pala4']));
        }else if(!$partida->count() && $partida_->torneo == 'NO'){
          $partida->create($request->only(['pala_id', 'pala','pala2_id', 'pala2','pala3_id', 'pala3','pala4_id', 'pala4','partida_id','club_id']));
        }
        

        $user = listaEspera::find($request->get('id'));
        $user_ = User::find($request->get('user_id'));
        $user->delete();

        return response()->json(['status'=>'success','msg'=> $request->get('nick') .' añadido a la partida.', 'movil' => $user_->telefono, 'partida' => $partida_->inicio_alquiler, 'pista' => $partida_->pistas_id]);

    }

    public function destroy($id){
      $user = listaEspera::find($id);
      $datosUsuario = User::find($user->user_id);
      $usersPartida = PartidasUsuario::find($user->partida_id);
     
      if($user->count()){
        $user->delete();
        if($usersPartida){
          if($usersPartida->pala == null && $usersPartida ->pala2 == null && $usersPartida ->pala3 == null && $usersPartida ->pala4 == null){
            $partida = Partidas::find($user->partida_id);
            $partida->delete();
          }
        }
        
       
        return response()->json(['status'=>'success','msg'=> $datosUsuario->nick .' eliminado con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error al eliminar jugador']);
      }
    }

}
