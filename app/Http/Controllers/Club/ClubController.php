<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Auth;

use App\Utilities\GoogleMaps;
use Image;
use Config;

use App\ConfigWeb;
use App\Clubs;
use App\Poblaciones;
use App\Provincias;
use App\Columnas;
use App\Paralax;
use App\Jumbotron;
use App\Social;
use App\Welcome;
use Carbon\Carbon;

class ClubController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:club');
        
    }

    public function index(){
    	$auth = Auth::user();
      Config::set('app.name',$auth->nombre);
      //$activateQeue = \Artisan::call('queue:listen');
    	$club = Clubs::mostrar($auth->id);
        return view('club.dashboard', compact('club'));
    }

    public function show($id)
    {
        

        $club = Clubs::mostrar($id);
       
        return response()->json($club);
            
    }
    
    public function update(Request $request, $id)
    {
      $club = Clubs::find($id); 
       
      $validar = $this->validate($request, [
          'nombre' => 'required|unique:clubs,nombre,'. $id,
          'slug' => 'required',
          'cif' => 'required|size:9',
          'telefono' => 'nullable',
          'movil' => 'required|integer|max:999999999',
          'email' => 'required|string|email|max:255|unique:clubs,email,'. $id,
          'email_web' => 'string|email|max:255',
          'responsable' => 'required',
          'pistas' => 'required|integer|min:1',
          'horario' => 'nullable|string',
          'web' => 'required|unique:clubs,web,'. $id,
          'direccion' => 'required',
          'poblacion_id' => 'required|numeric',
          //'latitud' => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
          //'longitud' => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ]);
        
            
        if($club->count() && $validar){
            
            if(!is_array($request->get('instalaciones'))){
              $instalaciones = explode(",", $request->get('instalaciones'));
              $club->instalaciones()->sync($instalaciones);
            }
           
            $poblacion = Poblaciones::where('id', '=', $request->get('poblacion_id'))->first();
            $coordenadas = GoogleMaps::geocodeAddress( $request->get('direccion'),  $poblacion->nombre, $poblacion->provincia->nombre, $poblacion->postal );
            
            $telefono = ($request->get('telefono') == 'null' || $request->get('telefono') == '') ? null: $request->get('telefono');
            //$club->update($request->except(['logo', 'password','longitud']));
            if($request->hasFile('logo')){
              $file = $request->file('logo'); //recupero la imagen

              $filename = $file->getClientOriginalName();

              $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
              $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
              });
              $img->stream(); //convierto la nueva imagen

              $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/logo/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
              $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/logo/' . $filename;
              $club->update(['logo' => $rutaImagen]); //guardo la ruta en la base de datos 
            }

            $club->update([
                  'nombre' => $request->get('nombre'),
                  'slug' => $request->get('slug'),
                  'cif' => $request->get('cif'),
                  'telefono' => $telefono,
                  'movil' => $request->get('movil'),
                  'email' => $request->get('email'),
                  'email_web' => $request->get('email_web'),
                  'responsable' => $request->get('responsable'),
                  'pistas' => $request->get('pistas'),
                  'horario' => $request->get('horario'), 
                  'web' => $request->get('web'),
                  'direccion' => $request->get('direccion'),
                  'poblacion_id' => $request->get('poblacion_id'),
                  'nosotros' => $request->get('nosotros'),
                  'latitud' =>  $coordenadas['lat'] == null ? $request->get('latitud'): $coordenadas['lat'],//$request->get('latitud') == null? 0.00: $request->get('latitud'),
                  'longitud' => $coordenadas['lng'] == null ? $request->get('longitud'): $coordenadas['lng'],//$request->get('longitud') == null? 0.00: $request->get('longitud'),
              ]);

            //$club->update(['latitud' => $coordenadas['lat'],'longitud' => $coordenadas['lng']]);
            //si el campo del password viene relleno, lo validamos y actualizamos datos
            
            if($request->get('password') != 'null' && $request->get('password') != ''){
              $validar = $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
              ]);

              $validar? $club->update([ 'password' => bcrypt($request->get('password'))]) : '';

            }

            
          return response()->json(['status'=>'success','msg'=> $club['nombre'] . ' actualizada con éxito']);
        }else {
            return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $club['nombre'] ]);
      }
    }

    //funciones para la configuracion de la web
    public function updateMod(Request $request, $id){
        $auth = Auth::user();

        $club = ConfigWeb::where('club_id',$auth->id);

        switch ($request->get('modulo')) {
          case 'jumbotron':
            $club->update(['jumbotron' => $request->get('valor')]);
            break;
          case 'columnas':
             $club->update(['columnas' => $request->get('valor')]);
            break;
          case 'paralax':
             $club->update(['paralax' => $request->get('valor')]);
            break;
          case 'welcome':
             $club->update(['welcome' => $request->get('valor')]);
            break;
          case 'noticias':
             $club->update(['noticias' => $request->get('valor')]);
            break;
          case 'patrocinadores':
            $club->update(['patrocinadores' => $request->get('valor')]);
            break;
          case 'brsocial':
            $club->update(['brsocial' => $request->get('valor')]);
            break;
          case 'bqsocial':
            $club->update(['bqsocial' => $request->get('valor')]);
            break;
        }
       
        if($estado = $request->get('valor') == 1){
          $response = ['status' => 'success','msg'=> $request->get('modulo') .' activado.'];
        }else{
          $response = ['status' => 'warning','msg'=> $request->get('modulo') .' desactivado.'];
        }
        return response()->json($response);
    }

   public function indexJumbotron(){
        $auth = Auth::user();

        $jumbotron = Jumbotron::where('club_id', '=', $auth->id)->get();

        return response()->json($jumbotron);

    } 

    public function jumbotron(Request $request){
        $club = Auth::user();

        $validar = $this->validate($request, [
             'titulo'    => 'required|string',
             'contenido' => 'required|string',
             'footer'    => 'nullable|string',
             'nombre'    => 'nullable|string',
             'web'       => 'nullable|string',
             'visible'   => 'required|in:1,0',
          ]); 

        $jumbotron = Jumbotron::find($request->get('id'));

        if($validar){
          if(empty($jumbotron)){
              $create = Jumbotron::create([
                  'club_id'   => $club->id,
                  'titulo'    => $request->get('titulo'),
                  'contenido' => $request->get('contenido'),
                  'footer'    => $request->get('footer'),
                  'nombre'    => $request->get('nombre'),
                  'web'       => $request->get('web'),
                  'visible'   => (boolean)$request->get('visible'),
                ]);

              return response()->json(['status' => 'success','msg'=> 'Jumbotron creado con éxito']);
              

            }else if($jumbotron->count()){

              $update = $jumbotron->update([
                  'club_id'   => $club->id,
                  'titulo'    => $request->get('titulo'),
                  'contenido' => $request->get('contenido'),
                  'footer'    => $request->get('footer'),
                  'nombre'    => $request->get('nombre'),
                  'web'       => $request->get('web'),
                  'visible'   => (boolean)$request->get('visible'),
                ]);     

              return response()->json(['status' => 'success','msg'=> 'Jumbotron actualizado con éxito']);

            }
       }

    }


    public function getWelcome(){
        $auth = Auth::user();

        $welcome = Welcome::where('club_id', '=', $auth->id)->get();

        return response()->json($welcome);
    }

    public function welcome(Request $request){
        $club = Auth::user();

        $validar = $this->validate($request, [
             'titulo'    => 'required|string',
             'contenido' => 'required|string',
             'visible'     => 'required|in:1,0',
             'status'      => 'required|in:BORRADOR,PUBLICADO',
          ]); 

        $welcome = Welcome::find($request->get('id'));

        if($validar){
          if(empty($welcome)){
              $create = Welcome::create([
                  'club_id'   => $club->id,
                  'titulo'    => $request->get('titulo'),
                  'contenido' => $request->get('contenido'),
                  'visible'   => (boolean)$request->get('visible'),
                  'status'    => $request->get('status'),
                ]);

              return response()->json(['status' => 'success','msg'=> 'Welcome creado con éxito']);
              

            }else if($welcome->count()){

              $update = $welcome->update([
                  'club_id'   => $club->id,
                  'titulo'    => $request->get('titulo'),
                  'contenido' => $request->get('contenido'),
                  'visible'   => (boolean)$request->get('visible'),
                  'status'    => $request->get('status'),
                ]);     

              return response()->json(['status' => 'success','msg'=> 'Welcome actualizado con éxito']);

            }
       }
    }


    public function getSocial(){
        $auth = Auth::user();

        $social = Social::where('club_id', '=', $auth->id)->get();

        return response()->json($social);
    }

    public function social(Request $request){
       $club = Auth::user();

        $validar = $this->validate($request, [
             'facebook'   => 'nullable|string',
             'twitter'    => 'nullable|string',
             'google'     => 'nullable|string',
             'instagram'  => 'nullable|string',
             'youtube'    => 'nullable|string',
             'visible'     => 'required|in:1,0',
             'status'      => 'required|in:BORRADOR,PUBLICADO',
          ]); 

        $social = Social::find($request->get('id'));

        if($validar){
          if(empty($social)){
              $create = Social::create([
                  'club_id'   => $club->id,
                  'facebook'    => $request->get('facebook'),
                  'twitter' => $request->get('twitter'),
                  'google' => $request->get('google'),
                  'instagram' => $request->get('instagram'),
                  'youtube' => $request->get('youtube'),
                  'visible'   => (boolean)$request->get('visible'),
                  'status'    => $request->get('status'),
                ]);

              return response()->json(['status' => 'success','msg'=> 'Bloque social creado con éxito']);
              

            }else if($social->count()){

              $update = $social->update([
                  'club_id'   => $club->id,
                  'facebook'    => $request->get('facebook'),
                  'twitter' => $request->get('twitter'),
                  'google' => $request->get('google'),
                  'instagram' => $request->get('instagram'),
                  'youtube' => $request->get('youtube'),
                  'visible'   => (boolean)$request->get('visible'),
                  'status'    => $request->get('status'),
                ]);     

              return response()->json(['status' => 'success','msg'=> 'Bloque social actualizado con éxito']);

            }
       }
    }


    public function getColumnas(){
        $auth = Auth::user();

        $columnas = Columnas::where('club_id', '=', $auth->id)->get();

        return response()->json($columnas);
    }

    public function columnas(Request $request){
        $club = Auth::user();
        $columnas = Columnas::find($request->get('id'));
        $rutaImagen = '';

      if($request->hasFile('imagen')){
        //$patrocinador = Patrocinadores::find($create->id); 
        $file = $request->file('imagen'); //recupero la imagen
           
        $filename = $file->getClientOriginalName();

        $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
        $img->resize(600,400, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
          $constraint->aspectRatio();                 
        });
        $img->stream(); //convierto la nueva imagen

        $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/columnas/' . $filename, $img); // almaceno la ruta de la imagen en una variable
        $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/columnas/' . $filename;
        
      };

      if(empty($columnas)){
        
        $validar = $this->validate($request, [
         'titulo'    => 'required|string|unique:columnas,titulo,' . $club->id,
         'contenido' => 'required|string',
         'nombre'    => 'string|nullable',
         'web'       => 'string|nullable',
         'visible'     => 'required|in:1,0',
        ]);


        if($validar){
          $create = Columnas::create([
            'club_id'     => $club->id,
            'titulo'      => $request->get('titulo'),
            'contenido'   => $request->get('contenido'),
            'nombre'      => $request->get('nombre'),
            'web'         => $request->get('web'),
            'imagen'      => $rutaImagen == ''? $request->get('imagen') : $rutaImagen,
            'visible'     => $request->get('visible'),
          ]);

        return response()->json(['status' => 'success','msg'=> 'Bloque columnas creado con éxito']);
        }
        
        
      }else if($columnas->count()){

        $validar = $this->validate($request, [
         'titulo'    => 'required|string|unique:columnas,titulo,' . $columnas->id,
         'contenido' => 'required|string',
         'nombre'    => 'string|nullable',
         'web'       => 'string|nullable',
         'visible'     => 'required|in:1,0',
        ]); 
        
        //$rutaImagen = $columnas->imagen;
       
        $update = $columnas->update([
            'club_id'     => $club->id,
            'titulo'      => $request->get('titulo'),
            'contenido'   => $request->get('contenido'),
            'nombre'      => $request->get('nombre'),
            'web'         => $request->get('web'),
            'imagen'      => $rutaImagen == ''? $columnas->imagen : $rutaImagen,
            'visible'     => (boolean)$request->get('visible'),
          ]);     

        return response()->json(['status' => 'success','msg'=> 'Columnas actualizado con éxito']);

      }
       
    }

    public function paralax(Request $request){
      $auth = Auth::user();
      $club = Clubs::find($auth->id); 
      $rutaImagen = $club->imgparalax;
      if($request->hasFile('imagen')){
        $file = $request->file('imagen'); //recupero la imagen

        $filename = $file->getClientOriginalName();

        $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
      
        $img->stream(); //convierto la nueva imagen

        $path = Storage::disk('public')->put('image'. '/club/' . $auth->id . '/paralax/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
        $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $auth->id . '/paralax/' . $filename;
        $club->update(['imgparalax' => $rutaImagen == ''? $request->get('imagen') : $rutaImagen]); //guardo la ruta en la base de datos 
        return response()->json(['status'=>'success','msg'=> 'Imagen paralax actualizada con éxito']);
      }

    }
    public function uploadImage(Request $request){
        $club = Auth::user();
        $date = Carbon::now()->toDateString();  
        $img = '';
        $path = '';
        if($request->hasFile('image')){
            
            $file = $request->file('image'); //recupero la imagen

            //$filename = $file->getClientOriginalName() . str_replace("-","", $date);
            $filename = str_replace("-","", $date) . "-" . $file->getClientOriginalName();

            $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
            $img->resize(300,null, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
            });
            $img->stream(); //convierto la nueva imagen

            $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/varios/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
            $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/varios/' . $filename;
        };
        
        $response = [
            'imagen' => $filename,
            'url'    => substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id
        ];   
        return response()->json($rutaImagen);
    }

    




}
