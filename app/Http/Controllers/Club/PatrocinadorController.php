<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Auth;
use Image;
use App\Patrocinadores;

class PatrocinadorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    } 

    public function listaPatrocinadores(){
    	$club = Auth::user();
        
        $patrocinadores = DB::table('patrocinadores')->select('*')->where('club_id','=',$club->id)->paginate(8);

        $response = [
            'pagination' => [
                'total' => $patrocinadores->total(),
                'per_page' => $patrocinadores->perPage(),
                'current_page' => $patrocinadores->currentPage(),
                'last_page' => $patrocinadores->lastPage(),
                'from' => $patrocinadores->firstItem(),
                'to' => $patrocinadores->lastItem()
            ],
            
            'items' => $patrocinadores,
            
        ];

        return response()->json($response);
        

    }
    
    public function crearPatrocinador(Request $request){
        $club = Auth::user();
        $rutaImagen = '';

        if($request->get('patrocina') >= 1){
            $validar = $this->validate($request, [
              'nombre'    => 'required|string|unique:patrocinadores,nombre,null,id,club_id,' . $club->id,
              'slug'      => 'required',
              'web'       => 'nullable',
              'imagen'    => 'nullable',
              'visible'   => 'required|in:1,0',
              'status'    => 'required|in:BORRADOR,PUBLICADO',
              'patrocina' => 'required|integer|unique:patrocinadores,patrocina,null,id,club_id,' . $club->id,
            ]); 
        }else{
            $validar = $this->validate($request, [
              'nombre'    => 'required|string|unique:patrocinadores,nombre,null,id,club_id,' . $club->id,
              'slug'      => 'required',
              'web'       => 'nullable',
              'imagen'    => 'nullable',
              'visible'   => 'required|in:1,0',
              'status'    => 'required|in:BORRADOR,PUBLICADO',
            ]); 
        }
        

        if($validar){
            if($request->hasFile('imagen')){
                //$patrocinador = Patrocinadores::find($create->id); 
                $file = $request->file('imagen'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                      $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen

                $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/patrocinadores/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/patrocinadores/' . $filename;
            };
            $create = Patrocinadores::create([
                'club_id'       => $club->id,
                'nombre'        => $request->get('nombre'),
                'slug'          => $request->get('slug'),
                'imagen'        => $rutaImagen,
                'web'           => $request->get('web'),
                'status'        => $request->get('status'),
                'visible'       => $request->get('visible'),
                'patrocina'     => $request->get('patrocina'),
              ]);
          
        
          
          return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);

        }
        
    }
    public function destroy($id)
    {
        $club = Auth::user();
        $patrocinador = Patrocinadores::find($id);

        if($patrocinador->count()){
            $patrocinador->delete();
            $imagen = explode('/',$patrocinador->imagen);
            $this->eliminarImagen(end($imagen));
          return response()->json(['status'=>'success','msg'=> $patrocinador['nombre'] . ' eliminado con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $patrocinador['nombre'] ]);
        }
    }

    public function update(Request $request, $id){
        $club = Auth::user();
        $rutaImagen = '';
        $patrocinador = Patrocinadores::find($id);
       
        if($request->get('patrocina') >= 1){
            
            $validar = $this->validate($request, [
              'nombre'    => 'required|string|unique:patrocinadores,nombre,' . $id,
              'slug'      => 'required',
              'web'       => 'nullable',
              'imagen'    => 'nullable',
              'visible'   => 'required|in:1,0',
              'status'    => 'required|in:BORRADOR,PUBLICADO',
              'patrocina' => 'required|integer|unique:patrocinadores,patrocina,' . $id,
            ]); 
            //return response()->json($validar);
        }else{

            $validar = $this->validate($request, [
              'nombre'    => 'required|string|unique:patrocinadores,nombre,' . $id,
              'slug'      => 'required',
              'web'       => 'nullable',
              'imagen'    => 'nullable',
              'visible'   => 'required|in:1,0',
              'status'    => 'required|in:BORRADOR,PUBLICADO',
            ]); 
           
        }

        if($patrocinador->count() && $validar){
        
            if($request->hasFile('imagen')){
                //$patrocinador = Patrocinadores::find($create->id); 
                $file = $request->file('imagen'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(400,null, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                      $constraint->aspectRatio();
                      $constraint->upsize();               
                });
                $img->stream(); //convierto la nueva imagen

                $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/patrocinadores/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/patrocinadores/' . $filename;
                $oldImagen = explode('/',$patrocinador->imagen);
                
                if(end($oldImagen) != $filename){
                    $this->eliminarImagen(end($oldImagen));
                }
                
            };
            $update = $patrocinador->update([
                'club_id'   => $club->id,
                'nombre'    => $request->get('nombre'),
                'slug'      => $request->get('slug'),
                'imagen'    => $rutaImagen == ''? $patrocinador->imagen : $rutaImagen,
                'web'       => $request->get('web'),
                'status'    => $request->get('status'),
                'visible'   => $request->get('visible'),
                'patrocina'   => $request->get('patrocina'),
              ]);           
          
            return response()->json(['status' => 'success','msg'=>'Patrocinador actualizado']);

        }

        
    }

    public function eliminarImagen($imagen){
        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image\club\\' . $club->id . '\patrocinadores\\';
        @unlink($path . '/' . $imagen);
    }

}
