<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Auth;
use Image;
use App\Slider;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request){
    	$club = Auth::user();
        
        $slider = Slider::where('club_id','=',$club->id)->orderBy('id', 'DESC')->paginate(2);

        $response = [
            'pagination' => [
                'total' => $slider->total(),
                'per_page' => $slider->perPage(),
                'current_page' => $slider->currentPage(),
                'last_page' => $slider->lastPage(),
                'from' => $slider->firstItem(),
                'to' => $slider->lastItem()
            ],
            
            'items' => $slider,
            
        ];

        return response()->json($response);
        

    }
    
    public function slider(Request $request){
        $club = Auth::user();
        $rutaImagen = '';
        $validar = $this->validate($request, [
             'visible' => 'required|in:1,0',
             'status' => 'required|in:BORRADOR,PUBLICADO',
        	]); 

        $slider = Slider::find($request->get('id'));

        if($validar){
        

            if($request->hasFile('imagen')){
                //$patrocinador = Patrocinadores::find($create->id); 
                $file = $request->file('imagen'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(1920,null, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                      $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen

                $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/slider/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/slider/' . $filename;
            };

            if(empty($slider)){
            	$create = Slider::create([
            		'club_id'   => $club->id,
	                'text'    	=> $request->get('text') != null ? $request->get('text') : '',
	                'alt'   	=> $request->get('alt') != null ? $request->get('alt') : '',
	                'caption'   => $request->get('caption') != null ? $request->get('caption') : '',
	                'web'   	=> $request->get('web') != null ? $request->get('web') : '#',
	                'imagen'    => $rutaImagen == ''? $request->get('imagen') : $rutaImagen,
	                'status'    => $request->get('status'),
	                'visible'   => (boolean)$request->get('visible'),
                ]);

            	return response()->json(['status' => 'success','msg'=> 'Slider creado con éxito']);
            	

            }else if($slider->count()){

            	$update = $slider->update([
	                'club_id'   => $club->id,
	                'text'     => $request->get('text') != null ? $request->get('text') : '',
                    'alt'       => $request->get('alt') != null ? $request->get('alt') : '',
                    'caption'   => $request->get('caption') != null ? $request->get('caption') : '',
                    'web'       => $request->get('web') != null ? $request->get('web') : '#',
	                'imagen'    => $rutaImagen == ''? $slider->imagen : $rutaImagen,
	                'status'    => $request->get('status'),
	                'visible'   => (boolean)$request->get('visible'),
	              ]);     

            	return response()->json(['status' => 'success','msg'=> 'Slider actualizado con éxito']);

            }
       }
        
    }
    public function destroy($id)
    {
        $club = Auth::user();
        $slider = Slider::find($id);
        
        if($slider->count()){
            $slider->delete();
            $imagen = explode('/',$slider->imagen);
            $this->eliminarImagen(end($imagen));
          return response()->json(['status'=>'success','msg'=> 'Eliminado con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando la imágen']);
        }
    }

   
    public function eliminarImagen($imagen){
        $club = Auth::user();
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club->id . '/slider/';
        @unlink($path . '/' . $imagen);
    }

}

