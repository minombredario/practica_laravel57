<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ligas;
use App\LigasResultado;
use App\LigasClasificacion;
use Auth;
use Illuminate\Support\Facades\DB;

class LigasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request)
    {
        $club = Auth::user();
        $busqueda = $request->get('busqueda');
        $ligas = LigasResultado::LigasClub($club->id,$busqueda);

        $response = [
            'pagination' => [
                'total' => $ligas->total(),
                'per_page' => $ligas->perPage(),
                'current_page' => $ligas->currentPage(),
                'last_page' => $ligas->lastPage(),
                'from' => $ligas->firstItem(),
                'to' => $ligas->lastItem()
            ],
            
            'items' => $ligas,
            
        ];
        //$columns = DB::getSchemaBuilder()->getColumnListing('ligas');
        return response()->json($response);
    }
    
    public function store(Request $request)
    {
        $club = Auth::user();
        $request->merge(['clubs_id' => $club->id]);

        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:ligas,nombre,null,id,clubs_id,' . $club->id,
        ]);

        if($validar){
            
            $create = Ligas::create($request->all());
            
            return response()->json(['status' => 'success','msg'=> 'Liga ' . $request->get('nombre') . ' creada con éxito']);
        }   

    }

    public function update(Request $request, $id)
    {
        $club = Auth::user();
        $request->merge(['clubs_id' => $club->id]);
        $liga = Ligas::find($id); 

        $validar = $this->validate($request, [
        'nombre' => 'required|string|unique:ligas,nombre,'. $id .',id,clubs_id,' . $club->id, 
        ]);

        if($validar){
            $liga->update($request->only(['nombre', 'visible']));

            return response()->json(['status' => 'success','msg'=> 'Liga ' . $request->get('nombre') . ' actualizada con éxito']);
        }
        
    }

    public function updateResultados(Request $request, $id)
    {
        $club = Auth::user();
        $contJuegosA = 0;
        $contJuegosB = 0;
        $request->get('juego11') > $request->get('juego12') ? $contJuegosA = $contJuegosA + 1 : $contJuegosB + 1;
        $request->get('juego21') > $request->get('juego22') ? $contJuegosA = $contJuegosA + 1 : $contJuegosB + 1;
        $request->get('juego31') > $request->get('juego22') ? $contJuegosA = $contJuegosA + 1 : $contJuegosB + 1;
       
        $ganador = $contJuegosA > $contJuegosB ? $request->get('a') : $request->get('b');
        

        $liga = LigasResultado::find($id)
                ->update(array(
                        'juego1' => $request->get('juego1'),
                        'juego2' => $request->get('juego2'),
                        'juego3' => $request->get('juego3'),
                        'juego11' => $request->get('juego11'),
                        'juego12' => $request->get('juego12'),
                        'juego21' => $request->get('juego21'),
                        'juego22' => $request->get('juego22'),
                        'juego31' => $request->get('juego31'),
                        'juego32' => $request->get('juego32'),
                        'ganador' => $ganador
                ));
           

         return response()->json(['status' => 'success','msg'=> 'Partida de ' . $request->get('a') . ' contra ' . $request->get('b') . ' actualizada con éxito']);
         
        
        
    }

    public function jornadas(Request $request, $id){
        
        $club = Auth::user();
        $liga_id = $id;
        $jornadas = $request;
        
        foreach ($jornadas as $jornada){
            foreach ($jornada as $jornada1){
                $cont = 0;
                foreach ($jornada1 as $jornada2){
                    $encontrado = DB::table('ligas_resultado')->where('liga_id','=',$liga_id)->where('club_id','=',$club->id)->where('jornada','=', $jornada2['jornada'])->where('a','=',$jornada2['a'])->where('b','=',$jornada2['b'])->get();
                    if($encontrado->count() == 1){
                        $cont = 1;
                        return response()->json(['status' => 'error','msg'=> 'Jordanas de liga ya creadas']);
                    }
                    if($cont == 0){
                        DB::table('ligas_resultado')->insert(['liga_id' => $liga_id, 'club_id' => $club->id, 'jornada' => $jornada2['jornada'], 'a' => $jornada2['a'], 'b' => $jornada2['b']]);
                    }else{
                        return response()->json(['status' => 'error','msg'=> 'Jordanas de liga ya creadas']);
                    }
                   
                }
            }
        }

        return response()->json($request);
    }

    public function destroy($id)
    {
      $liga = Ligas::find($id);

      if($liga->count()){
        $liga->delete();
        return response()->json(['status'=>'success','msg'=> $liga['nombre'] . ' eliminada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $liga['nombre'] ]);
      }
    }
}
