<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use Auth;
use App\User;
use App\Niveles;
use Image;

class JugadoresController extends Controller 
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function viewIndex(Request $request)
    {
      $club = Auth::user();
      $jugadores =DB::table('users')
                ->join('niveles','niveles.id','users.nivel_id')
                ->select(DB::raw('users.id, nick, posicion, CONCAT(nick," | ", users.nombre, " | ", niveles.nombre) AS nombre'))
                ->where('clubs_id', $club->id)
                ->orderBy('users.nick', 'ASC')->get();
      
      return response()->json($jugadores);
    }

    public function index(Request $request)
    {
      
      $busqueda = $request->get('busqueda');
      $club = Auth::user()->id;
      $nivel = ($request->get('nivel') == 'undefined' || $request->get('nivel') == '' || $request->get('nivel') == 'null') ? '': $request->get('nivel');
      $posicion = ($request->get('posicion') == 'undefined' || $request->get('posicion') == '' || $request->get('posicion') == 'null') ? '': $request->get('posicion');
      $dispon = ($request->get('dispon') == 'undefined' || $request->get('dispon') == '' || $request->get('dispon') == 'null') ? '' : $request->get('dispon');
      $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
      $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');
      
      if($busqueda != '' || $busqueda != null){
        $jugadores = User::busquedaClub($busqueda,$sortBy,$orden,$club);
      }else{
        $jugadores = User::busquedaExtendidaClub($club,$nivel,$posicion,$dispon,$sortBy,$orden);
      }
      

      $response = [
        'pagination' => [
          'total' => $jugadores->total(),
          'per_page' => $jugadores->perPage(),
          'current_page' => $jugadores->currentPage(),
          'last_page' => $jugadores->lastPage(),
          'from' => $jugadores->firstItem(),
          'to' => $jugadores->lastItem()
        ],

        'items' => $jugadores,
      ];   

      return response()->json($response);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IlluminateHttpRequest  $request
     * @return IlluminateHttpResponse
     */
    public function store(Request $request)
    {
     
      $club = Auth::user();
      $request->merge(['clubs_id' => $club->id]);

      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,null,id,clubs_id,' . $club->id, //compruebo que el nick no esta registrado ya en ese club.
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'required|string|unique:users,telefono,null,id,clubs_id,' . $club->id, //compruebo que el nick no esta registrado ya en ese club.
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,' . $request->get('nick'),
        'nivel_id' => 'required|integer|min:0',
        'nivel_real_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6|confirmed',
        ]);

      if($validar){
      	
        $create = User::create($request->except('avatar'));
        $user = User::find($create->id);
        $user->update([
          'password' => bcrypt($request->get('password')),
          'remember_token' => base64_encode($request->get('email'))
        ]);

        if($request->hasFile('avatar')){

                $file = $request->file('avatar'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(499,498, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen
                
                $path = Storage::disk('public')->put('image' . '/club/' . $club->id . '/' . $create->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $club->id . '/' . $create->id . '/avatar/' . $filename;
                $user->update([
                   'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
                 ]); 

              }else{
                $avatar = ($request->get('sexo') == "Hombre" || $request->get('sexo') == "HOMBRE") ? '../img/avatar/avatarH.png' : '../img/avatar/avatarM.png';
                $user->update([
                  'avatar' => $avatar,
                ]);

              }


              return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);
            }


          }

    public function update(Request $request, $id)
    {
      //$request->get('clubs_id') = Auth::user()->id;
      
      $club = Auth::user();
      $validar = $this->validate($request, [
        'nombre' => 'required|string',
        'apellidos' => 'nullable|string',
        'nick' => 'required|string|unique:users,nick,'. $id .',id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya
        'sexo' => 'required|in:HOMBRE,MUJER',
        'posicion' => 'required|in:REVÉS,DERECHA,INDIFERENTE',
        'desplazamiento' => 'nullable|integer',
        'disponibilidad' => 'nullable|string',
        'telefono' => 'required|integer|unique:users,telefono,' . $id . ',id,clubs_id,' . $request->get('clubs_id'), //compruebo que el nick no esta registrado ya en ese club.
        'poblacion_id' => 'required|integer|min:0',
        'clubs_id' => 'required|integer|unique:users,clubs_id,null,id,nick,1',
        'nivel_id' => 'required|integer|min:0',
        'nivel_real_id' => 'required|integer|min:0',
        'comentarios' => 'nullable|string',
        'email' => 'required|string|email|max:255|unique:users,email,' . $id . ',id,clubs_id,' . $request->get('clubs_id'),
      ]);

      $jugador = User::find($id);
      
 
      if($jugador->count() && $validar){

        if(!$request->hasFile('avatar') && strtolower($request->get('sexo')) != strtolower($jugador['sexo']) ){

          $avatar =  strtolower($request->get('sexo')) == "hombre" ? '../img/avatar/avatarH.png' : '../img/avatar/avatarM.png';

            $jugador->update([
                'avatar' => $avatar, //guardo la ruta en la base de datos
            ]);

        }

        $jugador->update($request->except(['avatar', 'password']));

          //si el campo del password viene relleno, lo validamos y actualizamos datos
        if($request->get('password') != 'null' && $request->get('password') != '' && $request->get('password') != null){
          
          $validar = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
          ]);

          if($validar){
            $jugador->update(['password' => bcrypt($request->get('password'))]);
          };
         

          
        }

        if($request->hasFile('avatar')){

          $file = $request->file('avatar'); //recupero la imagen

          $filename = $file->getClientOriginalName();

          $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
          $img->resize(499,498, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
            $constraint->aspectRatio();                 
          });
          $img->stream(); //convierto la nueva imagen
          
          $path = Storage::disk('public')->put('image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
          $rutaImagen = substr(asset($path), 0, -1) . 'image' . '/club/' . $request->get('clubs_id') . '/' . $jugador->id . '/avatar/' . $filename;
          $jugador->update([
             'avatar' => $rutaImagen, //guardo la ruta en la base de datos 
           ]); 

        }



        return response()->json(['status'=>'success','msg'=> $jugador['nombre'] . ' actualizada con éxito', 'user'=>$jugador]);
      }else{
        return response()->json(['status'=>'error','msg'=>'Error actualizando ' . $jugador['nombre'] ]);
      }
    }
  
    public function destroy($id)
    {
      $jugador = User::find($id);

      if($jugador->count()){
        $jugador->delete();
        return response()->json(['status'=>'success','msg'=> $jugador['nombre'] . ' eliminada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $jugador['nombre'] ]);
      }
    }
  }


