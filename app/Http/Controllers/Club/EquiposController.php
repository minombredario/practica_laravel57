<?php

namespace App\Http\Controllers\Club;

use App\Equipos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class EquiposController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request)
    {
        $club = Auth::user();
        $busqueda = $request->get('busqueda');
        $liga_id = $request->get('liga');

        $liga = Equipos::where('club_id', '=', $club->id)
                            ->where('liga_id','=', $liga_id)
                            ->get();

        //$columns = DB::getSchemaBuilder()->getColumnListing('ligas');
        return response()->json($liga);
    }
    
    public function store(Request $request)
    {
        $club = Auth::user();
        $request->merge(['club_id' => $club->id]);

        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:ligas_equipos,nombre,null,id,club_id,' . $club->id . ',liga_id,' . $request->get('liga_id'),
        ]);

        if($validar){
            $create = Equipos::create($request->only('nombre', 'liga_id', 'club_id'));
            
            return response()->json(['status' => 'success','msg'=> 'Liga ' . $request->get('nombre') . ' creada con éxito']);
        }   
    }

    public function update(Request $request, $id)
    {
        $club = Auth::user();
        
        $equipo = Equipos::find($id); 

        $validar = $this->validate($request, [
            'nombre' => 'required|string|unique:ligas_clasificacion,nombre,'. $id .',id,club_id,' . $club->id . ',liga_id,' . $request->get('liga_id'),
        ]);

        if($validar){
            $equipo->update($request->only(['nombre']));

            return response()->json(['status' => 'success','msg'=> 'Equipo ' . $request->get('nombre') . ' actualizada con éxito']);
        }
        
    }

    public function destroy($id)
    {
      $equipo = Equipos::find($id);

      if($equipo->count()){
        $equipo->delete();
        return response()->json(['status'=>'success','msg'=> $equipo['nombre'] . ' eliminada con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $equipo['nombre'] ]);
      }
    }
}
