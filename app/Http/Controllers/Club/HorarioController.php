<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use App\HorarioPista;

class HorarioController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:club');
        
    }

    public function index(){
    	$club = Auth::user()->id;

    	$horario = DB::table('horario_pistas')->where('club_id', $club)->select('*')->get();
        return response()->json($horario);
    }

    public function store(Request $request){
    	$club = Auth::user();
    	
		for($i = 0; $i < $club->pistas; $i++){
    		HorarioPista::create([
    			'club_id' => $club->id,
    			'pista_id' => $request[$i]['pista_id'],
    			'lunes' => $request[$i]['lunes'],
    			'martes' => $request[$i]['martes'],
    			'miercoles' => $request[$i]['miercoles'],
    			'jueves' => $request[$i]['jueves'],
    			'viernes' => $request[$i]['viernes'],
    			'sabado' => $request[$i]['sabado'],
    			'domingo' => $request[$i]['domingo'],
			]);
    			
    	};
    	return response()->json(['status' => 'success','msg'=> 'Horario creado con éxito']);
	}

    public function update(Request $request, $id){

    	$club = Auth::user();
    	
		for($i = 0; $i < $club->pistas; $i++){
			$horaPista = HorarioPista::find($request[$i]['id']);
			
    		$horaPista->update([
    			'club_id' => $club->id,
    			'pista_id' => $request[$i]['pista_id'],
    			'lunes' => $request[$i]['lunes'],
    			'martes' => $request[$i]['martes'],
    			'miercoles' => $request[$i]['miercoles'],
    			'jueves' => $request[$i]['jueves'],
    			'viernes' => $request[$i]['viernes'],
    			'sabado' => $request[$i]['sabado'],
    			'domingo' => $request[$i]['domingo'],
			]);
    			
    	};
    	return response()->json(['status' => 'success','msg'=> 'Horario actualizado con éxito']);
    	
    }
}
