<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Clubs;
use App\Poblaciones;
use App\Provincias;
use App\Columnas;
use App\Paralax;
use App\Jumbotron;
use App\Social;
use Auth;
use Image;


class ContentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function indexJumbotron(){
        $auth = Auth::user();

        $jumbotron = Jumbotron::where('club_id', '=', $auth->id)->get();

        return response()->json($jumbotron);

    } 

    public function jumbotron(Request $request){
        $auth = Auth::user();
    }


    public function getWelcome(){
        $auth = Auth::user();

        $welcome = Welcome::where('club_id', '=', $auth->id)->get();

        return response()->json($welcome);
    }

    public function welcome(Request $request){
        $auth = Auth::user();
    }


    public function getSocial(){
        $auth = Auth::user();

        $social = Social::where('club_id', '=', $auth->id)->get();

        return response()->json($social);
    }

    public function social(Request $request){
        $auth = Auth::user();
    }


    public function getParalax(){
        $auth = Auth::user();

        $paralax = Paralax::where('club_id', '=', $auth->id)->get();

        return response()->json($paralax);
    }

    public function paralax(Request $request){
        $auth = Auth::user();
    }
}
