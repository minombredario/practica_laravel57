<?php

namespace App\Http\Controllers\Club;

use Auth;
use App\Clases;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index()
    {
        $club = Auth::user();

        $clases = Clases::where('club_id', '=', $club->id)->get();

        return response()->json($clases);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function clases(Request $request)
    {
        $club = Auth::user();
        
        $clase = Clases::find($request->get('id'));
        
        if(empty($clase)){
            $create = Clases::create([
                'club_id'       => $club->id,
                'clases'        => $request->get('clases'),
                'visible'       => $request->get('visible')
              ]);
            return response()->json(['status' => 'success','msg'=> 'Tabla creada con éxito']);
        }else if($clase->count()){

            $update = $clase->update([
                'clases'        => $request->get('clases'),
                'visible'       => $request->get('visible')
            ]);     

            return response()->json(['status' => 'success','msg'=> 'Tabla actualizada con éxito']);

        }

       
    }

}
