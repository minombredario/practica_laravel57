<?php

namespace App\Http\Controllers\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Http\File;

use Auth;
use Image;
use App\Noticias;
use Carbon\Carbon; 
class NoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index(Request $request){
    	$club = Auth::user();
        $busqueda = $request->get('busqueda');
        $sortBy = $request->get('sortBy') == ''? 'nombre': $request->get('sortBy');
        $orden =  $request->get('orden') == ''? 'ASC': $request->get('orden');

        $noticias = Noticias::where('club_id','=',$club->id)->where('titulo','like', '%'.$busqueda.'%')->orderBy($sortBy, $orden)->paginate(8);

        $response = [
            'pagination' => [
                'total' => $noticias->total(),
                'per_page' => $noticias->perPage(),
                'current_page' => $noticias->currentPage(),
                'last_page' => $noticias->lastPage(),
                'from' => $noticias->firstItem(),
                'to' => $noticias->lastItem()
            ],
            
            'items' => $noticias,
            
        ];

        return response()->json($response);
        

    }

    public function store(Request $request){
        $club = Auth::user();
        
        $rutaImagen = '';
        $validar = $this->validate($request, [
              'titulo'      => 'required|unique:noticias,titulo,null,id,club_id,' . $club->id,
              'slug'        => 'required',
              'titular'     => 'required',
              'contenido'   => 'nullable',
              'web'         => 'nullable',
              'visible'     => 'required|in:1,0',
              'status'      => 'required|in:BORRADOR,PUBLICADO',
            ]); 

        if($validar){
        
            if($request->hasFile('imagen')){
                //$patrocinador = Patrocinadores::find($create->id); 
                $file = $request->file('imagen'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                      $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen

                $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/noticias/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/noticias/' . $filename;
            };
            $create = Noticias::create([
            	 
                'club_id'   => $club->id,
                'titulo'    => $request->get('titulo'),
                'slug'      => $request->get('slug'),
                'titular'   => $request->get('titular'),
                'contenido' => $request->get('contenido'),
                'imagen'    => $rutaImagen,
                'web'   	=> $request->get('web'),
                'status' 	=> $request->get('status'),
                'visible' 	=> (boolean)$request->get('visible'),
              ]);
          
        
          
          return response()->json(['status' => 'success','msg'=> $request->get('nombre') . ' creada con éxito']);

        }
        
    }
    
    public function update(Request $request, $id){
        $club = Auth::user();
        $rutaImagen = '';
        $noticia = Noticias::find($id);

        $validar = $this->validate($request, [
              'titulo'      => 'required|unique:noticias,titulo,' . $id,
              'slug'        => 'required',
              'titular'     => 'required',
              'contenido'   => 'nullable',
              'web'         => 'nullable',
              'visible'     => 'required|in:1,0',
              'status'      => 'required|in:BORRADOR,PUBLICADO',
            ]); 

        if($noticia->count() && $validar){
        
            if($request->hasFile('imagen')){
                //$patrocinador = Patrocinadores::find($create->id); 
                $file = $request->file('imagen'); //recupero la imagen

                $filename = $file->getClientOriginalName();

                $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
                $img->resize(600,600, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                      $constraint->aspectRatio();                 
                });
                $img->stream(); //convierto la nueva imagen

                $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/noticias/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
                $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/noticias/' . $filename;

                $update = $noticia->update([
                'titulo'    => $request->get('titulo'),
                'slug'      => $request->get('slug'),
                'titular'   => $request->get('titular'),
                'contenido' => $request->get('contenido'),
                'imagen'    => $rutaImagen,
                'web'       => $request->get('web'),
                'status'    => $request->get('status'),
                'visible'   => (boolean)$request->get('visible'),
              ]);  
            }else{
                $update = $noticia->update([
                'titulo'    => $request->get('titulo'),
                'slug'      => $request->get('slug'),
                'titular'   => $request->get('titular'),
                'contenido' => $request->get('contenido'),
                'imagen'    => $request->get('imagen'),
                'web'       => $request->get('web'),
                'status'    => $request->get('status'),
                'visible'   => (boolean)$request->get('visible'),
              ]);  
            };        
          
            return response()->json(['status' => 'success','msg'=>'Noticia actualizada']);

        }

        
    }

    public function destroy($id){
        $noticia = Noticias::find($id);
        if($noticia->count()){
          $noticia->delete();
          return response()->json(['status'=>'success','msg'=> $noticia['titulo'] . ' eliminada con éxito']);
        } else {
          return response()->json(['status'=>'error','msg'=>'Error eliminando ' . $noticia['titulo'] ]);
        }
    }
    public function subirImagen(Request $request){
        $club = Auth::user();
        $date = Carbon::now()->toDateString();  
        $img = '';
        $path = '';
        if($request->hasFile('image')){
            //$patrocinador = Patrocinadores::find($create->id); 
            $file = $request->file('image'); //recupero la imagen

            //$filename = $file->getClientOriginalName() . str_replace("-","", $date);
            $filename = str_replace("-","", $date) . "-" . $file->getClientOriginalName();

            $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
            $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                  $constraint->aspectRatio();                 
            });
            $img->stream(); //convierto la nueva imagen

            $path = Storage::disk('public')->put('image'. '/club/' . $club->id . '/noticias/' . $filename, $img); // almaceno la ruta de la imagen en una variable 
            $rutaImagen = substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/noticias/' . $filename;
        };
        
        $response = [
            'imagen' => $filename,
            'url'    => substr(asset($path), 0, -1) . 'image'. '/club/' . $club->id . '/noticias'
        ];   
        return response()->json($rutaImagen);
    }

    public function base64($titulo,$desc){
        $dom_desc = new Crawler($desc);
        $images = $dom_desc->filterXPath('//img')->extract(array('src')); // extract images
        $date = Carbon::now()->toDateString();  
        $ruta = 'image/club/' . $club->id . '/noticias';
         
        foreach ($images as $key => $value) {
            if (strpos($value, 'base64') !== false) { // leave alone not base64 images
                $data = explode(',', $value); // split image mime and body
                $tmp_file = tempnam('/tmp', 'items'); // create tmp file path
                file_put_contents($tmp_file, base64_decode($data[1])); // fill temp file with image
                $path = Storage::disk('public')->putFileAs($ruta, new File($tmp_file),$titulo . $key . "-" . str_replace("-","", $date) . '.png'); // put file to final destination
                $desc = str_replace($value, $path, $desc); // replace src of converted file to fs path

                unlink($tmp_file); // delete temp file
                return response()->json($images[$key]);   
            }
        }
    }

}
