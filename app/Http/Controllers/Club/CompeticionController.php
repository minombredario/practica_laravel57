<?php

namespace App\Http\Controllers\Club;

use Auth;
use App\Competiciones;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:club');
    }

    public function index()
    {
        $club = Auth::user();

        $competicion = Competiciones::where('club_id', '=', $club->id)->get();

        return response()->json($competicion);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function competicion(Request $request)
    {
        $club = Auth::user();
        
        $competicion = Competiciones::find($request->get('id'));
        
        if(empty($competicion)){
            $create = Competiciones::create([
                'club_id'       => $club->id,
                'competicion' => $request->get('competicion'),
                'visible'       => $request->get('visible')
              ]);
            return response()->json(['status' => 'success','msg'=> 'Tabla competiciones creada con éxito']);
        }else if($competicion->count()){

            $update = $competicion->update([
                'competicion'   => $request->get('competicion'),
                'visible'       => $request->get('visible')
            ]);     

            return response()->json(['status' => 'success','msg'=> 'Tabla competiciones actualizada con éxito']);

        }

       
    }

}
