<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class uploadCSV extends Controller
{
    public function index(){
        return view('uploadCSV');
    }

    public function upload(Request $request){
    /*	$upload = $request->file('CSV');
    	$filePath = $upload->getRealPath();
		
		$datos_salida = [];
		$fila = 1;
		if (($gestor = fopen($filePath, "r")) !== FALSE) {
		    while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
		        $numero = count($datos);
		        $fila++;
		        for ($c=0; $c < $numero; $c++) {
		            array_push($datos_salida, explode(";",utf8_encode($datos[$c])));
		        }
		    }
		    fclose($gestor);
		}
		
        set_time_limit(0);
    	for($i = 0; $i < sizeof($datos_salida); $i++) {
                if($datos_salida[$i][0] != ""){
        			$create = User::create([
            			'nombre' => $datos_salida[$i][0],
            			'apellidos' => $datos_salida[$i][1],
            			'sexo' => $datos_salida[$i][2],
            			'poblacion_id' => $datos_salida[$i][3],
            			'telefono' => $datos_salida[$i][4],
            			'email' => $datos_salida[$i][5],
            			'password' => bcrypt($datos_salida[$i][6]),
            			'nick' => $datos_salida[$i][7],
            			'avatar' => $datos_salida[$i][8],
            			'clubs_id' => $datos_salida[$i][9],
            			'posicion' => $datos_salida[$i][10],
            			'desplazamiento' => $datos_salida[$i][11],
        			    'disponibilidad' => $datos_salida[$i][12],
        			    'nivel_id' => $datos_salida[$i][13],
        			    'rol_id' => $datos_salida[$i][14],
        			    'notificaciones' => $datos_salida[$i][15],
        			    'comentarios' => $datos_salida[$i][16],
        			    'remember_token' => base64_encode($datos_salida[$i][5]),
            		]);
                }else{
                    return redirect()->back()->with('status', 'Datos subidos con éxito!');  
                }
		}
		return redirect()->back()->with('status', 'Datos subidos con éxito!');  */
        return redirect()->back()->with('error', 'NO TIENES PERMISOS'); 
    }
}
