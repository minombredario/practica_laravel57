<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Auth;
use App\Clubs;
use App\Partidas;
use App\Users;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index(){
    	$id_club = 1;
    	$club = Clubs::mostrarHome($id_club);
        //$resetConfig = \Artisan::call('config:cache');
        //$clearConfig = \Artisan::call('config:clear');
        
    	Config(['app.timezone' => 'Europe/Madrid']);
        Config::set('app.name', $club['club'][0]->nombre);
    	
        $partidas = Partidas::BusquedaClubPartida($id_club);
    	
        return view('index', compact('partidas', 'club'));
       
    }
}
