<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Image;

//crear tabla en base de datos
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Database\Migrations\Migration;

use App\User;
use App\Clubs;
use App\Instalaciones;
use App\Partidas;
use App\Provincias;
use App\Niveles;
use App\Noticias;
use App\Poblaciones;
use App\Columnas;
use App\Paralax;
use App\Jumbotron;
use App\Social;
use App\Slider;
use App\Welcome;
use App\Patrocinadores;
use Carbon\Carbon;
use App\ListaEspera;
use App\Ligas;
use App\TorneoUsers;
use App\Torneos;
use App\LigasResultado;
use App\PartidasUsuario;
use Faker\Factory as Faker;

//email
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;
use App\Emails;

class PageController extends Controller
{
    public function getLigas($club_id){

      $ligas = Ligas::where([ ['clubs_id', '=', $club_id], ['visible', '=', 1] ])->get();

      return response()->json($ligas);

    }

    public function getClasificacionLiga($liga_id){

        $clasificacion = LigasResultado::LigasClasificacion($liga_id);

        return response()->json($clasificacion);

    }

    public function getResultadosLiga($liga_id){

        $jornadas = DB::table('ligas_resultado')
                ->where('liga_id', '=', $liga_id)
                ->orderBy('jornada','ASC')
                ->get();

        return response()->json($jornadas);

    }

    public function getJumbotron($club_id){

        $jumbotron = Jumbotron::where([ ['club_id', '=', $club_id], ['visible', '=', 1] ])->get();

        return response()->json($jumbotron);

    }

    public function getWelcome($club_id){
       
        $welcome = Welcome::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($welcome);
    }

    public function getSocial($club_id){
       
        $social = Social::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($social);
    }

    public function getColumnas($club_id){
        
        $columnas = Columnas::where('club_id', '=', $club_id)->where('visible', '=', 1)->get();

        return response()->json($columnas);
    }

    public function getPartidas(Request $request){
      
          $id = 'club';
          $club_id = $request->get('club_id');
          $fecha = $request->get('dia');
          $vista = $request->get('vista');

          $partidas = Partidas::BusquedaPartidaClub2($fecha,$vista,$club_id,$id);              
          
          return response()->json($partidas);
    }

    public function misPartidas(Request $request){
          $club_id = $request->get('club_id');
          $user_id = $request->get('user_id');
          
          $partidasUser = DB::select('select pu1.partida_id from partidas_user as pu1 where pu1.pala_id =' . $user_id . ' and pu1.club_id = ' . $club_id . ' UNION ALL select pu2.partida_id from partidas_user as pu2 where pu2.pala2_id = ' . $user_id . ' and pu2.club_id = ' . $club_id . ' UNION ALL select pu3.partida_id from partidas_user as pu3 where pu3.pala3_id = ' . $user_id . ' and pu3.club_id = ' . $club_id . ' UNION ALL select pu4.partida_id from partidas_user as pu4 where pu4.pala4_id = ' . $user_id . ' and pu4.club_id = ' . $club_id. ' order by partida_id DESC');
          
          $partidas = [];

          foreach($partidasUser as $id){
            
            $partida = DB::table('partidas')->join('partidas_user','partidas.id','partidas_user.partida_id')->select('partidas.*',DB::raw('CONCAT(COALESCE(partidas_user.pala,""), ", ", COALESCE(partidas_user.pala2,"") , ", " , COALESCE(partidas_user.pala3,""), ", ", COALESCE(partidas_user.pala4,"")) as jugadoresPartida'))->where('partidas.id',$id->partida_id)->where('inicio_alquiler', ">=", Carbon::now())->first();
            $partida? array_push($partidas, $partida) : '';

          }

      return response()->json($partidas);
    }

    public function eliminarPartida(Request $request){
      $id = $request->get('partida');
      $us = $request->get('user'); 
      $partidaUser = PartidasUsuario::where('partida_id',$id)->first();
      $user = User::find($us);

      $partidaUser->pala == $user->nick? $partidaUser->update(['pala' => null]) : '';
      $partidaUser->pala_id == $user->id? $partidaUser->update(['pala_id' => null]) : '';
      $partidaUser->pala2 == $user->nick? $partidaUser->update(['pala2' => null]) : '';
      $partidaUser->pala2_id == $user->id? $partidaUser->update(['pala2_id' => null]) : '';
      $partidaUser->pala3 == $user->nick? $partidaUser->update(['pala3' => null]) : '';
      $partidaUser->pala3_id == $user->id? $partidaUser->update(['pala3_id' => null]) : '';
      $partidaUser->pala4 == $user->nick? $partidaUser->update(['pala4' => null]) : '';
      $partidaUser->pala4_id == $user->id? $partidaUser->update(['pala4_id' => null]) : '';




      return response()->json(['status' => 'success', 'msg' => 'Te has eliminado de la partida']);

    }

    public function misEsperas(Request $request){
          $date = Carbon::today();
          $club_id = $request->get('club_id');
          $user_id = $request->get('user_id');
      
         //$partidas = Partidas::MisEsperas($club_id, $user_id);
          $listaEspera = DB::table('lista_espera')
            ->join('partidas', 'lista_espera.partida_id', '=','partidas.id')
            ->join('niveles','partidas.nivelPartida_id', '=', 'niveles.id')
            ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
            ->select('lista_espera.*', 'partidas.inicio_alquiler', 'partidas.fin_alquiler', 'partidas.pistas_id', 'pistas.nombre as pista',  'niveles.nombre as nivel')
            ->Where('lista_espera.user_id','=', $user_id)
            ->where('lista_espera.clubs_id','=', $club_id)
            ->whereDate('partidas.inicio_alquiler', '>=', $date)
            ->get();

      return response()->json($listaEspera);

        
    }

    public function apuntadoEspera(Request $request){
      $userId = $request->get('user_id');
      $partida_id = $request->get('partida_id');
      $torneo_id = $request->get('torneo_id');


      if($partida_id){//si la partida ya esta abierta
        
        $partidaUser = PartidasUsuario::where('partida_id',$partida_id)->first();//busco si ya hay usuarios apuntados
        $listEspera = ListaEspera::where(function($query) use ($partida_id, $torneo_id){
          $query->where('partida_id',$partida_id)
                ->orWhere('torneo_id', $torneo_id);
        })
        ->where('user_id',$userId)->first();//busco al usario en la lista de espera
        $torneo = Torneos::find($torneo_id);

        $torneoUser = $torneo ? TorneoUsers::where('torneo_id',$torneo_id)->where('user_id',$userId)->first() : '';//busco al usario en la lista de espera
        if($partidaUser){//si hay usuarios apuntados
          if($partidaUser->pala_id == $userId || $partidaUser->pala2_id == $userId || $partidaUser->pala3_id == $userId || $partidaUser->pala4_id == $userId) {//compruebo que el usario no este aceptado y no este tampoco en lista de espera.
                
            return response()->json(['status' => false, 'msg' => 'Ya te han aceptado en esta partida']); //si esta en algun sitio devuelvo false
          
          }else{//si no esta en nigun sitio devuelvo true
           return response()->json(['status' => true, 'msg' => '']);
          }
        }else if($listEspera || $torneoUser){//si el usuario ya esta en la lista de espera
          $msg = $listEspera ? 'Ya estas a la espera en esta partida' : 'Ya estas apuntado al torneo';

          return response()->json(['status' => false, 'msg' => $msg]);
        }else{
          return response()->json(['status' => true, 'msg' => '']);
        }
        

      }else{
       return response()->json(['status' => true, 'msg' => '']);
      }
    }

    public function listaEspera(Request $request){



      $partida = Partidas::find($request->get('partida_id'));
    
            
      
        if(empty($partida)){


          $validar = $this->validate($request, [
          'clubs_id'        => 'required|integer|unique:partidas,clubs_id,null,id,pistas_id,'. $request->get('pistas_id') . ',inicio_alquiler,' . $request->get('inicio_alquiler'), 
          'pistas_id'       => 'required|integer|unique:partidas,pistas_id,null,id,clubs_id,'. $request->get('clubs_id') . ',inicio_alquiler,' . $request->get('inicio_alquiler'),
          'nivelPartida_id' => 'required|integer',
          'inicio_alquiler' => 'required|date|unique:partidas,inicio_alquiler,null,id,clubs_id,'. $request->get('clubs_id') . ',pistas_id,' . $request->get('pistas_id'),
          'fin_alquiler'    => 'required|date',
          'status'          => 'required|in:ABIERTA,CERRADA',
          'abierta_por'     => 'required|in:CLUB,USUARIO,INVITADO',
          'partida_fija'    => 'required|in:SI,NO',
          'equipo'    => 'required|in:SI,NO'

        ]);
  
          if($validar){

            $create = Partidas::create($request->all());
            $partida = Partidas::find($create->id);

            listaEspera::create([
              'partida_id' => $partida->id,
              'clubs_id'  => $request->get('clubs_id'),
              'user_id'   => $request->get('user_id'),
              'comentario' => $request->get('comentario'),  
            ]);  
          
          return response()->json(['status' => 'success','msg'=> 'Quedas a la espera de confirmación de la partida']);
          }
          
        }else if($partida->count()){
          $validarEspera = $this->validate($request, [
            'user_id'         => 'required|integer|unique:lista_espera,user_id,null,id,partida_id,' . $request->get('partida_id'),
            'partida_id'      => 'required|integer|unique:lista_espera,partida_id,null,id,user_id,' . $request->get('user_id'),
            
            
          
          ]);
          
          if($validarEspera){
            
            //busco la partida en la tabla de usuarios por partida
            $partidaUser = PartidasUsuario::where('partida_id',$request->get('partida_id'))->first();
            
            //busco el nombre del usuario que ha pulsado en la partida
            $userId = $request->get('user_id');
            
            if($partidaUser){
              
              if($partidaUser->pala_id == $userId || $partidaUser->pala2_id == $userId || $partidaUser->pala3_id == $userId || $partidaUser->pala4_id == $userId){
                
                return response()->json(['status' => 'error','msg'=> 'Ya te han aceptado en esta partida']);
              }
              
            }
            
            $create = listaEspera::create($request->all());            
            
            return response()->json(['status' => 'success','msg'=> 'Quedas a la espera de confirmación de la partida']);
          }
          
        }
      
      
    }
    
    public function eliminarEspera($id){
      $espera = listaEspera::find($id); //busco al usuario en la lista de espera
      $datosUsuario = User::find($espera->user_id); //busco los datos del usuario en la partida para dar el nick en el mensaje
      
      if($espera->count()){
        $espera->delete();//elimino al usuario de la lista de espera
        $partidaUsers = PartidasUsuario::where('partida_id', $espera->partida_id)->get(); //busco si hay usuarios en la partida
        $listaEspera = listaEspera::where('partida_id',$espera->partida_id)->get(); //busco si hay mas usuarios en la lista de espera
        
        if(!$listaEspera->count() && !$partidaUsers->count()){ //si no encuentra ningun usuario en las busquedas anteriores
          $partida = Partidas::find($espera->partida_id); //busco la partida y la elimino
          $partida->delete();
        }
        
        return response()->json(['status'=>'success','msg'=> $datosUsuario->nick .' eliminado con éxito']);
      } else {
        return response()->json(['status'=>'error','msg'=>'Error al eliminar jugador']);
      }
    }

    public function getNoticias($club_id){
      
      $noticias = Noticias::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->orderBy('id', 'DESC')->take(3)->get();
            
      return response()->json($noticias);
    }

    public function indexNoticias(Request $request){
      
      $busqueda = $request->get('busqueda');
      $id = $request->get('id');
      $sortBy = $request->get('sortBy') == ''? 'id': $request->get('sortBy');
      $orden =  $request->get('orden') == ''? 'DESC': $request->get('orden'); 
      
      if($busqueda != '' || $busqueda != null){
         $noticias = Noticias::where([ ['club_id', '=', $id], ['titulo', 'like', '%'. $busqueda .'%'],['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])
            ->orWhere([ ['club_id', '=', $id], ['contenido', 'like', '%'. $busqueda .'%'],['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])
            ->orderBy($sortBy, $orden)
            ->paginate(3);
      }else{
         $noticias = Noticias::where([ ['club_id', '=', $id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->orderBy($sortBy, $orden)->paginate(3);
      }
      

      $response = [
        'pagination' => [
          'total' => $noticias->total(),
          'per_page' => $noticias->perPage(),
          'current_page' => $noticias->currentPage(),
          'last_page' => $noticias->lastPage(),
          'from' => $noticias->firstItem(),
          'to' => $noticias->lastItem()
        ],

        'items' => $noticias,                    
      ];   

      return response()->json($response);


    }

    public function getSlider($club_id){
       
        $slider = Slider::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($slider);
    }

    public function getGalerias($club_id){
        
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' . $club_id . '/galerias';
        $carpetas = [];
        
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($path);
        
        // Recorre todos los elementos del directorio
      while (($archivo = readdir($gestor)) !== false)  {
        if ($archivo != "." && $archivo != "..") {
          $carpeta = ['nombre' => $archivo];
            array_push($carpetas, $carpeta);
          }
        };
        return response()->json($carpetas); 
    }

    public function getPatrocinadores($club_id){
        
        $patrocinadores = Patrocinadores::where([ ['club_id', '=', $club_id], ['status', '=', 'PUBLICADO'], ['visible', '=', 1] ])->get();

        return response()->json($patrocinadores);
    }

    public function galeria($galeria){
        $ruta = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $path = $ruta . 'image/club/' .'1/galerias/' . $galeria;
        $pathImag = '../image/club/' . '1/galerias/' . $galeria;
        $imagenes = [];

        // Abre un gestor de directorios para la ruta indicada
        $gestor = @opendir($path);
        
        // Recorre todos los elementos del directorio
      while (($archivo = readdir($gestor)) !== false)  {
        if ($archivo != "." && $archivo != "..") {
          array_push($imagenes, $pathImag . "/" . $archivo);
          }
        };
        return response()->json($imagenes); 
    }

    public function contacto(Request $request){
      $to = $request->get('to');

      $contacto = new \stdClass();
      $contacto->asunto = $request->get('asunto');
      $contacto->mensaje = $request->get('mensaje');
      $contacto->from = $request->get('from');
      $contacto->telf = $request->get('telf');

      Mail::to($to)->send(new ContactEmail($contacto)); 

      return response()->json(['status'=>'success','msg'=> 'Mensaje enviado con éxito']);
    }

    public function addUserTorneo(Request $request){

      $userTorneo = DB::table('torneo_users')->where('user_id', $request->get('user_id'))->where('torneo_id', $request->get('torneo_id'))->get();
      $userEspera = DB::table('lista_espera')->where('user_id', $request->get('user_id'))->where('torneo_id', $request->get('torneo_id'))->get();
      
      if($userTorneo->count() && !$userEspera->count()){
        return response()->json(['status'=>'warning','msg'=> 'Ya estabas apuntado a este torneo']);
      }
      if(!$userTorneo->count() && $userEspera->count()){
        return response()->json(['status'=>'warning','msg'=> 'Estas a la espera de confirmación para el torneo']);
      }

      if(!$userTorneo->count() && !$userEspera->count()){
        listaEspera::create($request->all());
        return response()->json(['status'=>'success','msg'=> 'Te acabas de apuntar al torneo']);
      }

      
    }

    public function delUserTorneo($id){
      $userTorneo = DB::table('torneo_users')->where('user_id', $id)->get();
      $userEspera = DB::table('lista_espera')->where('user_id', $id)->get();
      
      if($userTorneo->count()){
        DB::delete("DELETE FROM  torneo_users WHERE user_id='" .$id . "';");
        return response()->json(['status'=>'success','msg'=> 'Te has eliminado del torneo']);
      }
      if($userEspera->count()){
        DB::delete("DELETE FROM  lista_espera WHERE user_id='" .$id . "';");
        return response()->json(['status'=>'success','msg'=> 'Te has eliminado de la lista de espera para el torneo']);
      }
      
    }
    
   
}
