<?php
 
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Provincias;
use App\Poblaciones;
use App\Instalaciones;
use App\Niveles;
use App\User;
use App\Clubs;
use App\Partidas;
use App\Entrenador;
use Image;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class CombosController extends Controller
{
    public function comboProvincias(){
        $provincias = Provincias::orderBy('nombre','ASC')->get();
        
        return response()->json($provincias);
    }

    private function fecha(){
        CarbonInterval::setLocale('es');
        
        $ano   = Carbon::now()->startOfYear();
        $from = Carbon::now()->startOfMonth();
        $to  = Carbon::now()->endOfMonth();

        return ["anyo" => $ano, "mes" => $from, "to" => $to ];
    }

    private function Npartidas($from, $to, $id){
        return Partidas::join('partidas_user','partida_id','partidas.id')
                    ->where('clubs_id', '=', $id)
                    ->whereBetween('inicio_alquiler', [$from, $to])
                    ->where('clase','NO')
                    ->where('equipo','NO')
                    ->where('torneo','NO')
                    ->whereRaw('((pala_id is not null and pala_id != 0) or (pala != "" and pala is not null))
                                and ((pala2_id is not null and pala2_id != 0) or (pala2 != "" and pala2 is not null))
                                and ((pala3_id is not null and pala3_id != 0) or (pala3 != "" and pala3 is not null))
                                and ((pala4_id is not null and pala4_id != 0) or (pala4 != "" and pala4 is not null))'
                    )
                    ->count();
    }

    private function Nsocios($from, $to, $id){
       return User::where('clubs_id', '=', $id)
                ->whereBetween('created_at', [$from,$to])
                ->count();
    }

    private function Top5Hora($from, $to, $id){
       return DB::table('partidas')
                ->join('partidas_user','partida_id','partidas.id')
                ->groupBy('label')
                ->selectRaw('SUBSTRING(inicio_alquiler,12,19) as label, count(SUBSTRING(inicio_alquiler,12,19)) as value')
                ->where('clubs_id', '=', $id)
                ->where('clase','NO')
                ->where('equipo','NO')
                ->where('torneo','NO')
                ->whereBetween('inicio_alquiler', [$from, $to])
                ->whereRaw('((pala_id is not null and pala_id != 0) or (pala != "" and pala is not null))
                            and ((pala2_id is not null and pala2_id != 0) or (pala2 != "" and pala2 is not null))
                            and ((pala3_id is not null and pala3_id != 0) or (pala3 != "" and pala3 is not null))
                            and ((pala4_id is not null and pala4_id != 0) or (pala4 != "" and pala4 is not null))'
                )
                ->orderBy('value', 'DESC')
                ->limit(5)
                ->get();
                
    }

    private function top5Jugadores($from, $to, $id){
        return DB::select('select label, count(label) as value from(
                select pt1.partida_id as partida, pt1.pala_id as id_, pala as label from partidas_user as pt1 where pt1.pala_id is not null and pt1.club_id = ' . $id . '
                UNION ALL 
                select pt2.partida_id as partida, pt2.pala2_id as id_, pala2 as label from partidas_user as pt2 where pt2.pala2_id is not null and pt2.club_id = ' . $id . '
                UNION ALL 
                select pt3.partida_id as partida, pt3.pala3_id as id_, pala3 as label from partidas_user as pt3 where pt3.pala3_id is not null and pt3.club_id = ' . $id . '
                UNION ALL
                select pt4.partida_id as partida, pt4.pala4_id as id_, pala4 as label from partidas_user as pt4 where pt4.pala4_id is not null and pt4.club_id = ' . $id . '
                ) as palas
                ,partidas join partidas_user on (partidas.id = partidas_user.partida_id and ((pala_id is not null and pala_id != 0) or (pala != "" and pala is not null))
                and ((pala2_id is not null and pala2_id != 0) or (pala2 != "" and pala2 is not null))
                and ((pala3_id is not null and pala3_id != 0) or (pala3 != "" and pala3 is not null))
                and ((pala4_id is not null and pala4_id != 0) or (pala4 != "" and pala4 is not null)))
                where partidas.id = partida
                and inicio_alquiler between "'. $from . '" and "' . $to . '" and SUBSTRING(label,1,8) != "invitado" and label != ' . "' '" . ' group by label order by value DESC limit 5'
            );

           
    }

    private function partidasPorMes($anyo, $id){
        return DB::table('partidas as p')
                ->join('partidas_user','partida_id','p.id')
                ->select(DB::raw('MONTH(p.inicio_alquiler) as mes'),
                        DB::raw('YEAR(p.inicio_alquiler) as anyo'),
                        DB::raw('count(p.inicio_alquiler) as total')
                        
                )
                ->whereYear('p.inicio_alquiler', $anyo)
                ->where('p.clubs_id', '=', $id)
                ->where('p.clase','NO')
                ->where('p.equipo','NO')
                ->where('p.torneo','NO')
                ->whereRaw('((pala_id is not null and pala_id != 0) or (pala != "" and pala is not null))
                            and ((pala2_id is not null and pala2_id != 0) or (pala2 != "" and pala2 is not null))
                            and ((pala3_id is not null and pala3_id != 0) or (pala3 != "" and pala3 is not null))
                            and ((pala4_id is not null and pala4_id != 0) or (pala4 != "" and pala4 is not null))'
                )
                ->groupBy(DB::raw('MONTH(p.inicio_alquiler)'),
                        DB::raw('YEAR(p.inicio_alquiler)')
                )
                
                ->get();
    }

    private function partidasPorAnyo($anyo, $id){
        $date = Carbon::now();

        return DB::table('partidas as p')
                ->join('partidas_user','partida_id','p.id')
                ->select(DB::raw('YEAR(p.inicio_alquiler) as anyo'),DB::raw('count(p.inicio_alquiler) as total'))
                ->where('p.clubs_id', '=', $id)
                ->where('p.clase','NO')
                ->where('p.equipo','NO')
                ->where('p.torneo','NO')
                ->whereBetween(DB::raw('YEAR(p.inicio_alquiler)'), [$date->subYears(5),$anyo])
                ->whereRaw(
                    '((pala_id is not null and pala_id != 0) or (pala != "" and pala is not null))
                    and ((pala2_id is not null and pala2_id != 0) or (pala2 != "" and pala2 is not null))
                    and ((pala3_id is not null and pala3_id != 0) or (pala3 != "" and pala3 is not null))
                    and ((pala4_id is not null and pala4_id != 0) or (pala4 != "" and pala4 is not null))'
                )
                ->groupBy(DB::raw('YEAR(p.inicio_alquiler)'))
                ->get();
    }

    public function estadisticas(Request $request){
        $datosFecha = $this->fecha();
        $id = $request->get('club_id');
        $from = $request->get('from');

        $response = [
                'Nsocio'        => $this->Nsocios($from == "mensual" ? $datosFecha["mes"] : $datosFecha["anyo"],$datosFecha["to"], $id),
                'Npartidas'     => $this->Npartidas($from == "mensual" ? $datosFecha["mes"] : $datosFecha["anyo"],$datosFecha["to"], $id),
                'top5hora'      => $this->top5hora($from == "mensual" ? $datosFecha["mes"] : $datosFecha["anyo"],$datosFecha["to"], $id),
                'top5jugadores' => $this->top5jugadores($from == "mensual" ? $datosFecha["mes"] : $datosFecha["anyo"],$datosFecha["to"], $id),
                'partidasPorMes' => $from == "mensual" ? $this->partidasPorMes($datosFecha["anyo"], $id) : [],
                'partidasPorAnyo' => $from == "anual" ? $this->partidasPorAnyo($datosFecha["anyo"], $id) : [],
            ];
    
        return response()->json($response);
    }
    
    public function comboInstalaciones(){
        $instalaciones = Instalaciones::orderBy('nombre','ASC')->get();

        return response()->json($instalaciones);
    }

    public function comboNiveles(){
        $niveles = Niveles::orderBy('id','ASC')->get();

        return response()->json($niveles);
    }

    public function comboClubs(){
        $clubs = Clubs::orderBy('nombre','ASC')->get();

        return response()->json($clubs);
    }

    public function poblacionProvincia(Request $request){
        $busqueda = $request->get('busqueda');
    
        $poblaciones = Poblaciones::with('provincia')->where('postal','=',$busqueda)->orderBy('nombre','ASC')->get();
        
        return response()->json($poblaciones);
    }

    public function pistasDisponibles(Request $request){
        //YYYY-MM-DD
        CarbonInterval::setLocale('es');
        $carbon = Carbon::now('Europe/London');

        $hora = $carbon->addHour(1)->second(0)->toTimeString();
        $hoy = $carbon->toDateString();
        
        $club_id = $request->get('club');
        $year = substr($request->get('fecha'),6,10);
        $mes = substr($request->get('fecha'), 3,2);
        $dia = substr($request->get('fecha'),0,2);
        $fecha = $year . '-' . $mes . '-' . $dia;

        //si la fecha de entrada es la misma que el dia actual busco las partidas que hay entre la hora actual de la consulta y el final del dia
        //si la fecha es distinta, busco las partidas del dia completo.
        $fechaFrom = $hoy == $fecha ? ($fecha . ' ' . $hora) : ($fecha . ' 00:00:00');

        $fechaTo = $fecha . ' 23:00:00';
        
        $pistas = DB::table('partidas')
         ->join('pistas', 'partidas.pistas_id', '=', 'pistas.id')
         ->select('partidas.*', 'pistas.nombre as pista')
         ->where('clubs_id', '=', $club_id)
         ->where('inicio_alquiler','like', $fecha . '%')
         ->get();
            
        return response()->json($pistas);
 

        
    }

    public function clubUser(Request $request){
        
        $user = $request->get('userEmail');
        $club = $request->get('clubId');
       

        $jugadores = DB::table('users')
                    ->join('poblaciones', 'poblaciones.id', '=','users.poblacion_id')
                    ->join('niveles', 'niveles.id', '=', 'users.nivel_id')
                    ->join('clubs','clubs.id', '=', 'users.clubs_id')
                    ->select('users.id', 'users.nombre', 'users.apellidos', 'users.sexo', 'users.telefono', 'users.email','users.nick', 'users.avatar', 'users.clubs_id', 'users.posicion', 'users.nivel_id', 'users.desplazamiento', 'users.disponibilidad', 'users.notificaciones', 'users.comentarios', 'users.poblacion_id', 'poblaciones.nombre as poblacion','poblaciones.postal as postal', 'niveles.nombre as nivel', 'clubs.nombre as club')
                    ->where('users.email','=', $user)
                    ->where('users.clubs_id', '=', $club)
                    ->get();    
        foreach ($jugadores as $jugador){
            $clubs = DB::table('clubs_user')
                ->join('clubs', 'clubs_user.clubs_id', '=', 'clubs.id')
                ->select('clubs.id','clubs.nombre')
                ->where('clubs_user.user_email', '=', $user)
                ->get();
                $jugador->clubs = $clubs;
        }; 

        foreach ($jugadores as $jugador){
            $partidas = DB::table('partidas')
                ->select('*')
                ->where('user_id', '=', $user)
                ->get();
                $jugador->partidas = $partidas;
        };              
        
        return response()->json($jugadores);

    }

    public function upload(Request $request){
    	return $request;
        if($request->hasFile('image')){
            $file = $request->file('image'); //recupero la imagen

            $filename = $file->getClientOriginalName();
           
            $img = Image::make($file->getRealPath()); // creo el objeo Image para cambiarle el tamaño
            $img->resize(300,300, function ($constraint) { ///modifico el tamaño de la imagen conservando la relacion de aspecto
                $constraint->aspectRatio();                 
            });
            $img->stream(); //convierto la nueva imagen

            $path = Storage::disk('public')->put('image'. "/" . $filename, $img); // almaceno la ruta de la imagen en una variable 

            //$post->fill(['file' => substr(asset($path), 0, -1) . "image/" . $filename])->save(); //guardo la ruta en la base de datos
            return response()->json(['status' => 'success','msg'=> 'Imágen subida con éxito']);
        }else{
        	 return response()->json(['status' => 'error','msg'=> 'No se ha podido subir la imágen']);
        };
    }

    public function selectEntrenador(Request $request){
        $entrenadores = Entrenador::select('id','nombre','avatar')->where('activo',1)->orderBy('nombre', 'ASC')->get();
        return response()->json($entrenadores);
    }

}
