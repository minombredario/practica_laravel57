<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class clubUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nombre' => 'required|unique:clubs,nombre,' . $this->club,
          'slug' => 'required',
          'cif' => 'required|size:9',
          'telefono' => 'nullable',
          'movil' => 'required|integer|max:999999999',
          'email' => 'required|string|email|max:255|unique:clubs,email,'. $this->club,
          'password' => 'required|string|min:6|confirmed',
          'responsable' => 'required',
          'pistas' => 'required|integer|min:1',
          'horario' => 'nullable',
          'web' => 'required|unique:clubs,web,'. $this->club,
          'direccion' => 'required',
          'poblacion_id' => 'required|numeric',
          'latitud' => 'numeric|nullable',
          'longitud' => 'numeric|nullable',
        ];
    }
}
