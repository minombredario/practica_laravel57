<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pistas extends Model
{
    protected $table = 'pistas';
    
    protected $fillable = [
    	'nombre', 'slug'
    ];
    
    public $timestamps = false;

    public function clubs(){
        return $this->belongsToMany(Clubs::class);
    }

    public function pistas(){

        return $this->belongsToMany(clubs_partidas_users::class);
    }

}
