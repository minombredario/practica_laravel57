<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LigasResultado extends Model
{
    protected $table = 'ligas_resultado';

    protected $fillable = [
       'juego1','juego2', 'juego3', 'juego11','juego12', 'juego21','juego22','juego31', 'juego32', 'ganador'
    ];
    
    
    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }


    public function calcPartidos($equipo_id){
        $sets = DB::table('ligas_resultado')
            ->orWhere(function($query) use ($equipo_id){
                $query->where('a', $equipo_id)
                     ->orWhere('b', $equipo_id);
            })
            ->where('juego1', '!=', '0-0')
            ->orderBy('jornada','ASC')
            ->get();

        $setGanados = 0;
        $setPerdidos = 0;
        $juegosGanados = 0;
        $juegosPerdidos = 0;
        foreach($sets as $set){
            
            if($set->a == $equipo_id){
                $set->juego11 > $set->juego12 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $set->juego21 > $set->juego22 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $set->juego31 > $set->juego32 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $juegosGanados = $juegosGanados + $set->juego11 + $set->juego21 + $set->juego31;
                $juegosPerdidos = $juegosPerdidos + $set->juego12 + $set->juego22 + $set->juego32;
            }
            if($set->b == $equipo_id){
                $set->juego11 < $set->juego12 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $set->juego21 < $set->juego22 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $set->juego31 < $set->juego32 ? $setGanados = $setGanados + 1 : $setPerdidos = $setPerdidos + 1 ;
                $juegosPerdidos = $juegosPerdidos + $set->juego11 + $set->juego21 + $set->juego31;
                $juegosGanados = $juegosGanados + $set->juego12 + $set->juego22 + $set->juego32;
            }
        }

        
        return ['pj' => $sets->count(), 'sg' => $setGanados, 'sp' => $setPerdidos, 'jg' => $juegosGanados, 'jp' => $juegosPerdidos];
        
    }

    public function calcPartidosGanados($equipo_id){
        $pj = DB::table('ligas_resultado')
            ->orWhere(function($query) use ($equipo_id){
                $query->where('a', $equipo_id)
                     ->orWhere('b', $equipo_id);
            })
            ->where('juego1', '!=', '0-0')
            ->where('ganador', $equipo_id)
            ->orderBy('jornada','ASC')
            ->get();
        
        return $pj->count();
    }

    public function buscarNombre($equipo_id, $pos){
        $nombre = '';
            if($equipo_id != null){
                if($pos == 'a'){
                    $nombre = DB::table('ligas_equipos')
                        ->where('ligas_equipos.id', '=', $equipo_id)
                        ->pluck('nombre');
                }
           
            
                if($pos == 'b'){
                    $nombre = DB::table('ligas_equipos')
                        ->where('ligas_equipos.id', '=', $equipo_id)
                        ->pluck('nombre');
                }
                return $nombre[0];
            }else{
                return $nombre;
            }
    }

/* ---------------------------------- SCOPES --------------------------------------------*/
    public function scopeLigasClub($query,$club_id,$busqueda){
    	$puntosPorVictoria = 3;

    	$ligas = Ligas::where('clubs_id', '=', $club_id)->where('nombre', 'like', '%' . $busqueda . '%')->orderBy('id', 'DESC')->paginate(8);

    	foreach ($ligas as $liga){
            $equipos = DB::table('ligas_equipos')
                ->where('liga_id', '=', $liga->id)
                ->where('club_id', '=', $club_id)
                ->get();

                foreach ($equipos as $equipo){
                    $equipo->pj = $this->calcPartidos($equipo->id)['pj'];
                    $equipo->pg = $this->calcPartidosGanados($equipo->id);
                    $equipo->pp =  $equipo->pj - $equipo->pg;
                    $equipo->sg = $this->calcPartidos($equipo->id)['sg'];
                    $equipo->sp = $this->calcPartidos($equipo->id)['sp'];
                    $equipo->jg = $this->calcPartidos($equipo->id)['jg'];
                    $equipo->jp = $this->calcPartidos($equipo->id)['jp'];
                    $equipo->pts = $this->calcPartidosGanados($equipo->id) * $puntosPorVictoria;

                    
                };
                $equiposSort = [];
                foreach($equipos as $equipo){
                    array_push($equiposSort,$equipo);
                };
                //$equiposSort = (array)$equipos;
                usort($equiposSort, function($a,$b){
                    return $b->pts - $a->pts // puntos descending
                    ?: $b->jg - $a->jg // juegos ganados descending
                    ?: $a->jp - $b->jp // juegos perdidos ascending
                    ;
                });
            

            $jornadas = DB::table('ligas_resultado')
                ->where('liga_id', '=', $liga->id)
                ->where('club_id', '=', $club_id)
                ->orderBy('jornada','ASC')
                ->get();
                foreach ($jornadas as $equipo){
                    $equipo->equipoA = $this->buscarNombre($equipo->a,'a');
                    $equipo->equipoB = $this->buscarNombre($equipo->b,'b');
                };

            $liga->jornadas = $jornadas;
            $liga->equipos = $equiposSort;
        };

        return $ligas;
    }

    public function scopeLigasClasificacion($query, $id){
		$puntosPorVictoria = 3;

    	$equipos = DB::table('ligas_equipos')
                ->where('liga_id', '=', $id)
                ->get();

        foreach ($equipos as $equipo){
            $equipo->pj = $this->calcPartidos($equipo->id)['pj'];
            $equipo->pg = $this->calcPartidosGanados($equipo->id);
            $equipo->pp =  $equipo->pj - $equipo->pg;
            $equipo->sg = $this->calcPartidos($equipo->id)['sg'];
            $equipo->sp = $this->calcPartidos($equipo->id)['sp'];
            $equipo->jg = $this->calcPartidos($equipo->id)['jg'];
            $equipo->jp = $this->calcPartidos($equipo->id)['jp'];
            $equipo->pts = $this->calcPartidosGanados($equipo->id) * $puntosPorVictoria;

            
        };

        $equiposSort = [];
        
        foreach($equipos as $equipo){
            array_push($equiposSort,$equipo);
        };

        $jornadas = DB::table('ligas_resultado')
                ->where('liga_id', '=', $id)
                ->orderBy('jornada','ASC')
                ->get();
        
        foreach ($jornadas as $equipo){
            $equipo->equipoA = $this->buscarNombre($equipo->a,'a');
            $equipo->equipoB = $this->buscarNombre($equipo->b,'b');
        };
        
        usort($equiposSort, function($a,$b){
            return $b->pts - $a->pts // puntos descending
            ?: $b->jg - $a->jg // juegos ganados descending
            ?: $a->jp - $b->jp // juegos perdidos ascending
            ;
        }); 

        $response = [
            'jornadas' => $jornadas,            
            'clasificacion'  => $equipos,
            
        ];   
        
    	return $response;
    }
}
