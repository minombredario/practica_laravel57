<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Clubs;
use Config;

class UserResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $club = Clubs::find($notifiable->clubs_id);
        $actionUrl = $club->web . "/#/reset/". $this->token;
        //$logo = $club->web . "/image/club/" . $notifiable->clubs_id . "/logo/";
        $logo = $club->logo;
        $estilo = "width: 75px; float: left; padding: 5px;";
        Config::set('app.name', $club->nombre);

        //$actionUrl = "http://127.0.0.1:8000/#/reset/". $this->token;
        //$logo = "http://127.0.0.1:8000/image/club/" . $notifiable->clubs_id . "/logo/logo.png";
        
        return (new MailMessage)
            ->from($club->email_web , $club->nombre . ', ')
            ->subject('Solicitud de reestablecimiento de contraseña') //agregamos el asunto
            ->greeting('Hola ' . $notifiable->nombre)// titulo del mensaje
            ->line('Recibes este email porque se solicitó el reestablecimiento de contraseña para tu cuenta')
            // Action : Texto del botón , url(app.url) la tomará desde el .env  , la ruta reset con el token respectivo
            ->action('Restablecer contraseña', $actionUrl)
            ->line('Si no realizaste esta petición puedes ignorar este correo')
            
//            ->attach(public_path('/image/club/'). $notifiable->clubs_id . "/logo/logo.png", [
//                        'mime' => 'image/jpeg,image/png,image/jpg,image/gif,image/svg',
//                    ])
            ->markdown('mails.recovery', ['logo' => $logo, 'estilo' => $estilo, 'url' => $club->web])
            ->salutation('Saludos'); // Saludo Final            
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
