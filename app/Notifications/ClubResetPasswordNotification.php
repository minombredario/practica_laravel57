<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Clubs;
use Config;

class ClubResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        $actionUrl = $notifiable->web . "/#/reset/". $this->token;
        //$logo = $notifiable->web . "/image/club/" . $notifiable->id . "/logo/logo.png";
        $logo = $notifiable->logo;
        $estilo = "width: 75px; position: absolute; left: 10px; top: 0";
        Config::set('app.name', $notifiable->nombre);

        return (new MailMessage)
            ->subject('Solicitud de reestablecimiento de contraseña') //agregamos el asunto
            ->greeting('Hola ' . $notifiable->responsable)// titulo del mensaje
            ->line('Recibes este email porque se solicito el reestablecimiento de contraseña para tu cuenta')
            // Action : Texto del botón , url(app.url) la tomará desde el .env  , la ruta reset con el token respectivo
            ->action('Recuperar contraseña', url(config('app.url').route('club.password.reset', $this->token, false)))
            ->line('Si no realizaste esta petición puede ignorar este correo')
            
//            ->attach(public_path('/image/club/'). $notifiable->id . "/logo/logo.png", [
//                        'mime' => 'image/jpeg,image/png,image/jpg,image/gif,image/svg',
//                    ])
            ->markdown('mails.recoveryClub', ['logo' => $logo, 'estilo' => $estilo, 'url' => $notifiable->web])
            ->salutation('Saludos'); // Saludo Final       
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
