<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ligas extends Model
{
    protected $table = 'ligas';

    protected $fillable = [
       'clubs_id','nombre', 'visible'
    ];
    
    
    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
