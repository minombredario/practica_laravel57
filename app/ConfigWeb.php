<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigWeb extends Model
{
    protected $table = 'config_web';

    protected $fillable = [
       'club_id','cards','paralax','imgparalax','galeria','jumbotron','imageslider','bqsocial','patrocinadores','brsocial','columnas','welcome','noticias',
    ];
}
