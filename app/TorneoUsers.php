<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TorneoUsers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 
       'club_id', 'torneo_id', 'user_id'
    ];

    protected $table = 'torneo_users';
    public $timestamps = false;


}