<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Clubs;

class ListaEspera extends Model
{
     protected $fillable = [ 
       'user_id', 'clubs_id', 'partida_id','torneo_id', 'comentario'
    ];

    protected $table = 'lista_espera';
    public $timestamps = false;
    
    public function users(){

        return $this->belongsToMany(User::class,'partidas', 'id','user_id');
    }

    public function club(){

        return $this->belongsToMany(Clubs::class, 'partidas', 'id','clubs_id');
    }

	public function partidas(){
        return $this->belongsToMany(Clubs::class, 'partidas')
            ->withPivot('partida_id','inicio_alquiler', 'fin_alquiler', 'status');
    }
    
    public function partidaEspera(){
        return $this->belongsToMany(Partidas::class, 'lista_espera')
            ->withPivot('user_id', 'clubs_id', 'partida_id', 'comentario');
            
    }

    
}
