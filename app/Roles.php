<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
   	protected $fillable = [
       'nombre', 'slug'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->hasMany(User::class);
    }


    public function club()
    {
        return $this->hasMany(Clubs::class);
    }
}
 