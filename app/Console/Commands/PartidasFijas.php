<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\PartidasUsuario;
use App\Partidas;

use Maatwebsite\Excel\Facades\Excel;
use App\Utilities\ExportExcelCsv;

use Carbon\Carbon;

class PartidasFijas extends Command
{
    /** composer require maatwebsite/excel

        config/app.php

        'providers' => [
            Maatwebsite\Excel\ExcelServiceProvider::class,
        ],

        'aliases' => [
            'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        ],

        php artisan vendor:publish
    */

     /*
       en local
       php artisan partidas:fijas

       en servidor 
       /opt/plesk/php/7.2/bin/php  /var/www/vhosts/practicapadel.es/httpdocs/artisan queue:work
       /opt/plesk/php/7.2/bin/php  /var/www/vhosts/practicapadel.es/httpdocs/artisan partidas:fijas
       /opt/plesk/php/7.2/bin/php  /var/www/vhosts/practicapadel.es/httpdocs/artisan schedule:run >> /dev/null 2>&1
     */   
    protected $signature = 'partidas:fijas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear las partidas fijas para la semana próxima';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $partidasFijas = DB::select("select p.clubs_id, p.pistas_id, p.nivelPartida_id, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.inicio_alquiler)), '%Y-%m-%d %H:%i:%s') as inicio, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.fin_alquiler)), '%Y-%m-%d %H:%i:%s') as fin, p.status, p.abierta_por, p.partida_fija, p.equipo, p.texto, p.mensaje, p.torneo, p.cartel, p.observaciones,CURRENT_TIMESTAMP as created_at,CURRENT_TIMESTAMP as updated_at, pu.club_id, pu.pala_id, pu.pala, pu.pala2_id, pu.pala2, pu.pala3_id, pu.pala3, pu.pala4_id, pu.pala4
//            from partidas as p inner join partidas_fijas on p.id = partidas_fijas.partida_id inner join partidas_user as pu on p.id = pu.partida_id where WEEKDAY(p.inicio_alquiler) = WEEKDAY(now())-1");
        $partidasFijas = DB::select("select p.clubs_id, p.pistas_id, p.nivelPartida_id, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.inicio_alquiler)), '%Y-%m-%d %H:%i:%s') as inicio, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.fin_alquiler)), '%Y-%m-%d %H:%i:%s') as fin, p.status, p.abierta_por, p.partida_fija, p.equipo, p.mensaje, p.torneo, p.cartel, p.observaciones,CURRENT_TIMESTAMP as created_at,CURRENT_TIMESTAMP as updated_at, pu.club_id, pf.jugadores_fijos
            from partidas as p inner join partidas_fijas as pf on p.id = pf.partida_id inner join partidas_user as pu on p.id = pu.partida_id where WEEKDAY(p.inicio_alquiler) = WEEKDAY(now())-1");

        
        $partidas_csv = fopen('partidas.csv', 'c'); //Abrir el fichero para sólo escritura. Si el fichero no existe, se crea. Si existe no es truncado
        fputcsv($partidas_csv, array ('Fecha','consulta','partida_id','inicio','fin','pista', 'mensaje'));
        fclose($partidas_csv);
        $partidas_csv = fopen('partidas.csv', 'a+'); //Apertura para lectura y escritura; coloca el puntero del fichero al final del mismo. Si el fichero no existe, se intenta crear. 
        fgets($partidas_csv);//coloco el puntero al final del documento
        fseek($partidas_csv,-2,SEEK_END);

        $usuarios_csv = fopen('usuarios.csv', 'c');
        fputcsv($usuarios_csv, array ('Fecha','consulta','partida_id', 'mensaje'));
        fclose($usuarios_csv);
        $usuarios_csv = fopen('usuarios.csv', 'a+');
        fgets($partidas_csv);
        fseek($partidas_csv,-2,SEEK_END);

        /*$fecha = new DateTime();
        $hora_registro = date('m/d/Y H:i:s', $fecha->getTimestamp());*/

        $date = Carbon::today();
        $date = Carbon::parse($date, 'Europe/Madrid')->addHours(Carbon::parse($date, 'Europe/Madrid')->offsetHours);

        foreach ($partidasFijas as $partida){

            try {
                DB::beginTransaction();

                $create = Partidas::firstOrCreate([
                    'clubs_id'        => $partida->clubs_id,
                    'pistas_id'       => $partida->pistas_id,
                    'nivelPartida_id' => $partida->nivelPartida_id,
                    'inicio_alquiler' => $partida->inicio,
                    'fin_alquiler'    => $partida->fin,
                    'status'          => $partida->status,
                    'abierta_por'     => $partida->abierta_por,
                    // 'partida_fija'    => $partida->partida_fija,
                    'equipo'          => $partida->equipo,
                    'observaciones'   => $partida->observaciones,
                    'mensaje'         => $partida->mensaje,
                    'torneo'          => $partida->torneo,
                    'cartel'          => $partida->cartel,
                ]);

                $fijos = json_decode($partida->jugadores_fijos);
                $create2 = PartidasUsuario::firstOrCreate([
                    'club_id'    => $partida->club_id,
                    'partida_id' => $create->id,
                    'pala_id'    => isset($fijos->pala_id) ? $fijos->pala_id : "",
                    'pala'       => isset($fijos->pala) ? $fijos->pala : "",
                    'pala2_id'   => isset($fijos->pala2_id) ? $fijos->pala2_id : "",
                    'pala2'      => isset($fijos->pala2) ? $fijos->pala2 : "",
                    'pala3_id'   => isset($fijos->pala3_id) ? $fijos->pala3_id : "",
                    'pala3'      => isset($fijos->pala3) ? $fijos->pala3 : "",
                    'pala4_id'   => isset($fijos->pala4_id) ? $fijos->pala4_id : "",
                    'pala4'      => isset($fijos->pala4) ? $fijos->pala4 : "",
                ]);

                if ($create){
                    fputcsv($partidas_csv, [$date->format('Y-m-d H:i:s'),$create->id, $partida->inicio , $partida->fin, $partida->clubs_id, "Resgistros ingresados correctamente"]);
                    //echo "Records inserted successfully.";
                }else{
                    fputcsv($partidas_csv, [$date->format('Y-m-d H:i:s'), $create->id , $partida->inicio, $partida->fin, $partida->clubs_id , "ERROR: Could not able to execute $create. " . $create->error]);
                    //echo "ERROR: Could not able to execute $sql2. " . $conn->error . '<br>';
                }
                if ($create2){
                    fputcsv($usuarios_csv, [$date->format('Y-m-d H:i:s'), $create->id, "Resgistros ingresados correctamente"]);
                    //echo "Records inserted successfully.";
                }else{
                    fputcsv($usuarios_csv, [$date->format('Y-m-d H:i:s'), $create->id, "ERROR: Could not able to execute $create2. " . $create2->error]);
                    //echo "ERROR: Could not able to execute $sql2. " . $conn->error . '<br>';
                }

                DB::commit();
            } catch (\Exception $e) {
                // Manejar la excepción (puedes hacer un rollback si es necesario)
                DB::rollBack();
                // También puedes lanzar la excepción nuevamente si deseas propagarla
                throw $e;
            }

        }
        /*$partidas = Partidas::all();
        $headers = DB::getSchemaBuilder()->getColumnListing('partidas');
        Excel::store(new ExportExcelCsv($partidas, $headers), 'export.xlsx');*/

       

        $this->info(sizeof($partidasFijas));
        
        
       
    }
}
