<?php

namespace App\Policies;

use App\User;
use App\Clubs;
use App\Admin;

use Illuminate\Auth\Access\HandlesAuthorization;

class ClubPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function passClub(Clubs $club, User $user)
    {
        foreach ($user->clubs_id as $club_id){
           if($club_id == $club){
            return true;
           }
        };
        //return 1 == $club->id;
    }
    
}
    