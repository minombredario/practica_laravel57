<?php

namespace App\Rules;
 
use Illuminate\Contracts\Validation\Rule;

 
class Recaptcha implements Rule
{
    const URL = 'https://www.google.com/recaptcha/api/siteverify';
     
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

       $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. env('RECAPTCHAV3_SECRET', false) .'&response='.$value);
    
        $body = json_decode((string)$verifyResponse);
        $score = $body->score;
        if($score > 0.7) {
            return $body->success;
        } elseif($score > 0.3) {
            return $body->success;
        } else {
            return abort(400, '¡Asegúrate de ser un humano!');
        }
        

    }
 
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '¡Asegúrate de ser un humano!';
    }
 
    /**
     * Determine if Recaptcha's keys are set to test mode.
     *
     * @return bool
     */
    public static function isInTestMode()
    {
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='. env('RECAPTCHAV3_SECRET', false) .'&response='.$value);
    
        $body = json_decode((string)$verifyResponse);
        $score = $body->score;
        if($score > 0.7) {
            return $body->success;
        } elseif($score > 0.3) {
            return $body->success;
        } else {
            return abort(400, '¡Asegúrate de ser un humano!');
        }
    }
}