<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\notifications\AdminResetPasswordNotification;
//use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'administradores';

    protected $guard = 'admin';

    protected $fillable = [
       'nombre','email', 'password' 
    ];
    
    public $timestamps = false;

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }
}
