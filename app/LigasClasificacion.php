<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LigasClasificacion extends Model
{
	protected $table = 'ligas_clasificacion';

    protected $fillable = [
       'club_id','liga_id', 'nombre','pos','equipo','pj', 'pg', 'pp', 'sg', 'sp', 'jg', 'jp', 'pts'
    ];
    
    
    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }

    public function liga()
    {
        return $this->belongsTo(Ligas::class);
    }