<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class ClubUser extends Authenticatable implements JWTSubject
{
    use Notifiable;
    
    protected $table = 'clubs_user';

    protected $fillable = [
       'clubs_id','user_id','email', 'password' 
    ];
    
    public $timestamps = false;

    /*public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }*/

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }
}