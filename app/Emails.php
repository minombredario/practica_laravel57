<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
   protected $fillable = [ 
       'club_id', 'titulo', 'mensaje'
    ];

    protected $table = 'emails';
}
