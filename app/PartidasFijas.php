<?php 
//https://davidburgos.blog/como-activar-y-consultar-los-eventos-de-mysql/

/* eventos partidas fijas mysql

DROP EVENT IF EXISTS partidasFijas;

CREATE EVENT partidasFijas
ON SCHEDULE EVERY '1' DAY
STARTS '2018-11-30 16:40:00' -- should be in the future
DO
INSERT INTO partidas (clubs_id, pistas_id, nivelPartida_id, inicio_alquiler, fin_alquiler, status, abierta_por, partida_fija, equipo, texto, mensaje, torneo, cartel, observaciones)
select p.clubs_id, p.pistas_id, p.nivelPartida_id, STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.inicio_alquiler)), '%Y-%m-%d %H:%i:%s'), STR_TO_DATE(CONCAT(date(DATE_ADD(now(), INTERVAL 6 DAY)), ' ', time(p.fin_alquiler)), '%Y-%m-%d %H:%i:%s'), p.status, p.abierta_por, p.partida_fija, p.equipo, p.texto, p.mensaje, p.torneo, p.cartel, p.observaciones
from partidas as p inner join partidas_fijas on p.id = partidas_fijas.partida_id where WEEKDAY(p.inicio_alquiler) = WEEKDAY(now())-1;
	

*/

/*https://codepen.io/naps62/pen/MwVRXZ atomo sccs*/
/*https://codepen.io/Turqueso/pen/vAIBb sistema solar */
namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidasFijas extends Model
{
    protected $fillable = [
       'club_id', 'partida_id', 'jugadores_fijos'
    ];

    public $timestamps = false;

    public function partidaFija(){
        return $this->belongsToMany(Partidas::class, 'partidas_fijas', 'id','partida_id');
    }

}
