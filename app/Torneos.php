<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torneos extends Model
{
    protected $table = 'torneos';

    protected $fillable = [
       'club_id','partida_id','nombre', 'visible', 'fecha'
    ];
    
    
    public function club()
    {
        return $this->belongsTo(Clubs::class);
    }
}
