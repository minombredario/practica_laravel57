import layout					from '../components/index.vue';
import componentes			    from '../components/layouts/home.vue';
import registro 				from '../components/DashBoardUser/auth/registerForm.vue'
import emailPass				from '../components/DashBoardUser/auth/emailForm.vue'
import emailReset				from '../components/DashBoardUser/auth/resetForm.vue'
import PartidasView				from '../components/DashBoardUser/Partidas/PartidasCuad.vue'
import LigasPage				from '../components/DashBoardUser/Liga/ligaPage.vue'
import TorneoPage				from '../components/DashBoardUser/Torneo/torneoPage.vue'
import ClasesPage				from '../components/DashBoardUser/Clases/clasesPage.vue'
import GaleriasView				from '../components/DashBoardUser/Galeria/GaleriasView.vue'
import GaleriaView				from '../components/DashBoardUser/Galeria/Galeria.vue'
import ShowGaleria				from '../components/extras/galeria.vue'
import ShowPerfil				from '../components/DashBoardUser/User/ShowPerfil.vue'
import MisPartidas				from '../components/DashBoardUser/Partidas/MisPartidas.vue'
import NoticiasView 			from '../components/DashBoardUser/Noticias/NoticiasView.vue'
import Noticia 					from '../components/DashBoardUser/Noticias/Noticia.vue'
import error404 				from '../components/layouts/404.vue'
import politica					from '../components/DashBoardUser/Politicas/politica.vue'
import cookies					from '../components/DashBoardUser/Politicas/politicaCookies.vue'
import nosotrosPage				from '../components/layouts/nosotros.vue'

export const routesInvitado = [
	//{path: '/',		component: layout, name: 'layout'}
	{path: '/', 	redirect: { name: 'layout' },		component: layout
		,children: [
			{path: '/', 					component: componentes, 	name:'layout'},
			{path:'/registro', 				component: registro, 		name:'registrarUsuario'},
			{path:'/login', 				component: registro, 		name:'login'},
			{path:'/email', 				component: emailPass, 		name:'email'},
			{path:'/reset/:token', 			component: emailReset, 		name:'reset'},
			{path: '/partidas',				component: PartidasView, 	name:'partidas',   		meta:{title: 'Partidas', requiresAuth: false}, 		props: { ver: 'club' }},
			{path: '/ligas',				component: LigasPage, 		name:'ligas',			meta:{title: 'Ligas', requiresAuth: false} },
			{path: '/torneos',				component: TorneoPage, 		name:'torneos',			meta:{title: 'Torneos', requiresAuth: false} },
			{path: '/clases',				component: ClasesPage, 		name:'clases',			meta:{title: 'Clases', requiresAuth: false} },
			//{path: '/galeria',		component: GaleriaPage, name:'galeria',		meta:{title: 'Galeria'}},
			{path: '/galerias',					component: GaleriasView, 	name:'galerias',		meta:{title: 'Galerias', requiresAuth: false}},
			{path: '/galeria/:nombre',			component: GaleriaView, 	name:'galeria',			meta:{title: 'Galeria', requiresAuth: false}},
			{path: '/noticias',					component: NoticiasView, 	name:'noticias',		meta:{title: 'Noticias', requiresAuth: false} },
			{path: '/noticia/:slug', 			component: Noticia, 		name:'leerNoticia',		meta:{title: 'Noticia', requiresAuth: false} },		
			//{path: '/galeria/:galeria',		component: ShowGaleria, 	name:'galeria', 		meta:{title: 'Galeria', requiresAuth: false} },
			{path: '/perfil/:nick',				component: ShowPerfil,  	name:'perfil',  			meta:{title: 'Perfil',  requiresAuth: true} },
			{path: '/MisPartidas',				component: MisPartidas,  	name:'misPartidas',  		meta:{title: 'MisPartidas',  requiresAuth: true} },	
			{path: '/politica_de_privacidad',	component: politica,  		name:'politica',  		meta:{title: 'politica_de_privacidad',  requiresAuth: false} },
			{path: '/politica_de_cookies',		component: cookies,  		name:'cookies',  		meta:{title: 'politica_de_cookies',  requiresAuth: false} },
			{path: '/nosotros',					component: nosotrosPage,  	name:'nosotros',  		meta:{title: 'elClub',  requiresAuth: false} },
			{path: '*', 			 			component: error404}
		]
	},
];

