
import layout					from '../components/ClubDashBoard.vue'
//enlaces usuario club
import ListUsersClub	 		from '../components/DashBoardClub/Jugadores/ListUsersClub.vue'
import ShowUserClub	 			from '../components/DashBoardClub/Jugadores/ShowUserClub.vue'
import CreateUserClub	 		from '../components/DashBoardClub/Jugadores/CreateUserClub.vue'
//enlaces perfil club
import statsClub	 			from '../components/extras/stats.vue'
import ShowClubPerfil	 		from '../components/DashBoardClub/Clubs/ShowClub.vue'
//enlaces noticias
import ListNoticias				from '../components/DashBoardClub/Noticias/ListNoticias.vue'
import CreateNoticia			from '../components/DashBoardClub/Noticias/CreateNoticia.vue'
import ShowNoticia				from '../components/DashBoardClub/Noticias/ShowNoticia.vue'
//enlaces galeria
import ListGalerias				from '../components/DashBoardClub/Galerias/ListGalerias.vue'
import ShowGaleria				from '../components/extras/galeria.vue'
//enlaces patrocinadores
import ListPatrocinadores		from '../components/DashBoardClub/Patrocinadores/ListPatrocinadores.vue'
//enlaces customView
import customView				from '../components/DashBoardClub/CustomView/CustomView.vue'
//enlaces PartidasView
import PartidasView				from '../components/DashBoardClub/Partidas/PartidasView.vue'
import ListPartidas  			from '../components/DashBoardClub/Partidas/ListPartidas.vue'
import ListEspera  				from '../components/DashBoardClub/Partidas/ListEspera.vue'
//enlaces slide
import SliderView				from '../components/DashBoardClub/Carrusel/ShowSlider.vue'
//enlaces clases
import PageClase				from '../components/DashBoardClub/Clases/PageClase.vue'
//enlaces ligas y torneos
import PageLigasTorneos			from '../components/DashBoardClub/Ligas/PageLigasTorneos.vue'
import PageClasificacionLiga	from '../components/DashBoardClub/Ligas/PageClasificacionLiga.vue'
import PageResultadosLiga		from '../components/DashBoardClub/Ligas/PageResultadosLiga.vue'
//import PageTorneo				from '../components/DashBoardClub/Ligas/PageTorneo.vue'
import PageClasificacionTorneo	from '../components/DashBoardClub/Ligas/PageClasificacionTorneo.vue'
import PageResultadosTorneo		from '../components/DashBoardClub/Ligas/PageResultadosTorneo.vue'
import PageNormasLiga			from '../components/DashBoardClub/Ligas/NormasPuntuacio.vue'
//enlaces LOPD

import PageLOPD					from '../components/DashBoardClub/LOPD/LOPD.vue'

import ListHorarios				from '../components/DashBoardClub/HorarioPistas/ListHorarios.vue'
import envioMasivoEmail			from '../components/DashBoardClub/Emails/ListEmails.vue'

import ListEntrenador			from '../components/DashBoardClub/Entrenadores/ListEntrenadoresClub.vue'

//import store 					from '../store/store.js';

export const routesClub = [

	
	{path: '/', 	redirect: { name: 'layout'},		component: layout
		,children: [
			{path: '/', 	component: statsClub, name:'layout', meta:{title: 'stats'} },
			//perfil club   	
		   	{path: '/perfil', 	 					component: ShowClubPerfil, name:'ShowClubPerfil', meta:{title: 'Perfil'}},

		   	//usuarios club
		   	{path: '/users', 	 						component: ListUsersClub, 	name:'ListUsersClub', meta:{title: 'Jugadores'}},
				{path: '/ver/usuario/:nombre', 			component: ShowUserClub, 	 name: 'ShowUserClub', meta:{title: 'Jugador'}},
				{path: '/crear/usuario/', 				component: CreateUserClub, 	 name: 'CreateUserClub', meta:{title: 'Jugador'}},
				{path: '/partidas/usuario/:nombre', 	component: PartidasView, 	 name:'PartidasUser', props: { ver: 'jugador' },  meta:{title: 'Partidas'}},
			
			//noticias club
			{path: '/noticias',						component: ListNoticias, 	name:'ListNoticias', meta:{title: 'Noticias'}},
			{path: '/publicar/noticia', 			component: CreateNoticia, 	name:'CreateNoticia', meta:{title: 'Noticia'}},
			{path: '/noticia/:slug', 				component: ShowNoticia, 	name:'ShowNoticia', meta:{title: 'Noticia'}},

			//galeria club
			{path: '/galeria',						component: ListGalerias, 	name:'galeria', meta:{title: 'Galerías'}},
			{path: 'ver/galeria/:nombre',		component: ShowGaleria, 	    name:'ShowGaleria', meta:{title: 'Galería'}},

			//patrocinadores club
			{path: '/patrocinadores',				component: ListPatrocinadores, 	name:'patrocinadores', meta:{title: 'Patrocinadores'}},

			//customizar web club

			{path: '/customView',					component: customView, 	name:'customView', meta:{title: 'CustomView'}},

			//partidas club
			{path: '/partidas',						component: PartidasView, 	name:'partidas'		, props: { ver: 'club' },   meta:{title: 'Partidas'}},
			{path: '/listaPartidas', 				component: ListPartidas, 	name:'listaPartidas', props: { ver: 'club' },   meta:{title: 'Partidas'}},
			{path: '/listaEspera', 					component: ListEspera, 		name:'listaEspera'	, props: { ver: 'club' },   meta:{title: 'Partidas Espera'}},

			//slider club
			{path: '/slider',						component: SliderView, 	name:'ShowSlider', meta:{title: 'SliderView'}},

			//clases club
			{path: '/clases',						component: PageClase, 	name:'PageClase', meta:{title: 'Clases'}},

			//Ligas club
			{path: '/ligas',						component: PageLigasTorneos, 		name:'PageLigasTorneos', 		meta:{title: 'Ligas'}},
			{path: '/clas_liga/:liga',			component: PageClasificacionLiga, 	name:'PageClasificacionLiga', 	meta:{title: 'ClasificacionLiga'}},
			{path: '/res_liga/:liga',				component: PageResultadosLiga, 		name:'PageResultadosLiga', 		meta:{title: 'ResultadosLiga'}},

			{path: '/clas_torneo/:torneo',		component: PageClasificacionTorneo, name:'PageClasificacionTorneo', meta:{title: 'ClasificacionTorneo'}},
			{path: '/res_torneo/:torneo',			component: PageResultadosTorneo, 	name:'PageResultadosTorneo', 	meta:{title: 'ResultadosTorneo'}},
			//Torneos club
			//{path: '/torneo/:torneo',				component: PageTorneo, 				name:'PageTorneo', 				meta:{title: 'Torneo'}},
			{path: '/normasLiga',					component: PageNormasLiga, 			name:'PageNormasLiga', 			meta:{title: 'PageNormasLiga'}},
			
			//LOPD
			{path: '/lopd', 						component: PageLOPD, 				name:'lopd', 					meta:{title: 'politicaPrivacidad'}},	
			{path: '/horarios', 					component: ListHorarios, 			name:'horarios', 				meta:{title: 'horarios'}},
			{path: '/emails', 						component: envioMasivoEmail, 		name:'emails', 					meta:{title: 'emails'}},
		
			//Entrenador
			{path: '/ListEntrenador',				component: ListEntrenador,  		name: 'ListEntrenadoresClub',			meta:{title: 'Entrenodares'}},
		
		]
},

	

]