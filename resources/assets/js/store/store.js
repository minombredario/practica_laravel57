import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'; //almacena aunque se refresque la pagina
//import Cookies from 'js-cookie'; para almacenar datos en cookies
//npm install vuex-persistedstate --save
//npm install vuex --save
import { getLocalUser} from '../helpers/auth';
import { getLocalClub} from '../helpers/auth';
import { getLocalUserActive} from '../helpers/auth';
import { getLocalLigaActive} from '../helpers/auth';
import { getLocalTorneoActive} from '../helpers/auth';

const user = getLocalUser();
const club = getLocalClub();
const userActive = getLocalUserActive();
const ligaActive = getLocalLigaActive();
const torneoActive = getLocalTorneoActive();

Vue.use(Vuex, createPersistedState);

export const store = new Vuex.Store({
	state:{
    currentUser: user,
    isLoggedIn: !!user,
    loading: false,
    auth_error: null,
    //club
    currentClub: club,
    isLoggedInClub: !!club,
    loadingClub: false,
    auth_error_club: null,
    
    //liga
    currentLiga: ligaActive,
    currentTorneo: torneoActive,


    //club: [],
		noticia: [],
    //user: [],
    current_user_view: userActive,
    provincias: [],
    instalaciones: [],
    niveles: [],
    clubs: [],
    //ver paginas
    clases: false,
    noticias: false,
    torneos: false,
    ligas: false,
    //noticia: [],
   //galeria: [],
	},
	plugins: [createPersistedState()],
	mutations: {
    //autenticacion club
    loginClub(state){
      state.loadingClub = true;
      state.auth_error_club = null;
    },
    loginSuccessClub(state, payload){
      state.auth_error_club = null;
      state.isLoggedInClub = true;
      state.loadingClub = false;
      state.currentClub = Object.assign({}, payload.club, {token: payload.access_token});

      localStorage.setItem("club", JSON.stringify(state.currentClub));
    },
    resErrorAuthClub(state){
      state.auth_error = null;
    },
    clubSuccess(state, payload){
      state.auth_error_club = null;
      state.isLoggedInClub = true;
      state.loadingClub = false;
      state.currentClub = Object.assign({}, payload.club, {token: payload.access_token});

      localStorage.setItem("club", JSON.stringify(state.currentClub));
    },
    clubFailed(state, payload){
      state.loadingClub = false;
      state.auth_error_club = payload.error;
    },
    logoutClub(state){
      localStorage.removeItem("club");
      state.isLoggedInClub = false;
      state.currentClub = null;
    },


    //autenticacion usuario
		login(state){
      state.loading = true;
      state.auth_error = null;
    },
    resErrorAuth(state){
      state.auth_error = null;
    },
    loginSuccess(state, payload){
      state.auth_error = null;
      state.isLoggedIn = true;
      state.loading = false;
      state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

      localStorage.setItem("user", JSON.stringify(state.currentUser));
    },
    loginFailed(state, payload){
      state.loading = false;
      state.auth_error = payload.error;
    },
    logout(state){
      localStorage.removeItem("user");
      state.isLoggedIn = false;
      state.currentUser = null;
    },

    //acceso a las paginas vacias
    verPaginaNoticias(state, payload){
      state.noticias = payload;
    },
    verPaginaTorneos(state, payload){
      state.torneos = payload;
    },
    verPaginaLigas(state, payload){
      state.ligas = payload;
    },
    verPaginaClases(state, payload){
     state.clases = payload;
    },

    //combos de arrays
    provincias(state,payload){
      state.provincias = payload;
    },
    instalaciones(state,payload){
      state.instalaciones = payload;
    },
    niveles(state,payload){
      state.niveles = payload;
    },
    clubs(state,payload){
      state.clubs = payload;
    },
    noticia(state,payload){
      state.noticia = payload;
    },

    //activo
    user(state, payload){
      state.currentUser = payload;
      localStorage.setItem("user", JSON.stringify(payload));
    },
    userActive(state, payload){
      
      state.current_user_view = Object.assign({}, payload, {token: payload.access_token});
      localStorage.setItem("userActive", JSON.stringify(state.current_user_view));
    },
    ligaActive(state, payload){
      state.currentLiga =  payload;
      
      localStorage.setItem("ligaActive", JSON.stringify(state.currentLiga));
      
    },
    torneoActive(state, payload){
      state.currentTorneo =  payload;
      
      localStorage.setItem("torneoActive", JSON.stringify(state.currentTorneo));
      
    },
   
    /*club(state, payload){
      state.club = payload;
    },*/
    /*user(state,payload){
      state.user = payload;
    },*/
    /*noticia(state,payload){
      state.noticia = payload;
    },*/
    /*galeria(state,payload){
      state.galeria = payload;
    }*/
	},
  getters:{
    //club

    isLoadingClub(state){
      return state.loadingClub;
    },
    isLoggedInClub(state){
      return state.isLoggedInClub;
    },
    currentClub(state){
      return state.currentClub;
    },
    authErrorClub(state){
      return state.auth_error_club;
    },

    //user
    isLoading(state){
      return state.loading;
    },
    isLoggedIn(state){
      return state.isLoggedIn;
    },
    currentUser(state){
      return state.currentUser;
    },
    authError(state){
      return state.auth_error;
    },
    club(state){
      return state.club;
    },
    //paginas
    isVerPaginaClases(state){
     return state.clases;
    },
    //user activo
    currentUserView(state){
      return state.current_user_view;
    },
    //liga activa
    currentLiga(state){
      return state.currentLiga;
    },
    //torneo activa
    currentTorneo(state){
      return state.currentTorneo;
    },
  },
  actions:{
    login(context){
      context.commit("login");
    }
  },
});

//para almacenar en cookies
/*npm install js-cookie --save
export const store = new Vuex.Store({
  state: {
    count: 0
  },
  plugins: [createPersistedState({
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    }
  })],
  mutations: {
    increment: state => state.count++,
    decrement: state => state.count--
  }
});


y en el vue

new Vue({
  el: '#app',
  computed: {
    count() {
      return store.state.count;
    }
  },
  methods: {
    increment() {
      store.commit('increment');
    },
    decrement() {
      store.commit('decrement');
    }
  }
});



*/	

/*
"devDependencies": { 
    "axios": "^0.17",
    "bootstrap": "^4.0.0-beta.2",
    "cross-env": "^5.1",
    "jquery": "^3.2",
    "laravel-mix": "^1.0",
    "lodash": "^4.17.4",
    "pe7-icon": "^1.0.4",
    "popper.js": "^1.12.6",
    "toastr": "^2.1.4",
    "vue": "^2.5.16",
    "vue-html5-editor": "^1.1.1",
    "vue-template-compiler": "^2.5.16"
  },
  "dependencies": {
    "bootstrap-vue": "^2.0.0-rc.1",
    "bootstrap4-vue": "^0.3.12",
    "ckeditor": "^4.9.2",
    "ckeditor-full": "^4.7.3",
    "font-awesome": "^4.7.0",
    "iconfonts": "^0.12.0",
    "ionicons": "^3.0.0",
    "moment": "^2.22.1",
    "slug": "^0.9.1",
    "vue-awesome": "^2.3.5",
    "vue-ckeditor2": "^2.0.1",
    "vue-d2b": "^1.0.11",
    "vue-datepicker": "^1.3.0",
    "vue-moment": "^3.2.0",
    "vue-quill-editor": "^3.0.6",
    "vue-router": "^3.0.1",
    "vue-toastr": "^2.0.12",
    "vue-ueditor": "^0.1.3",
    "vue2-editor": "^2.4.4",
    "vuex": "^3.0.1",
    "vuex-persistedstate": "^2.4.2"
  }

  */