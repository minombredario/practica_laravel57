
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//url amigable
const slug = require('slug');
window.Slug = require('slug');
//importar iconos awesome
import 'vue-awesome/icons'

import BootstrapVue from 'bootstrap-vue';

window.Swal = require('sweetalert2');
window.toastr = require('toastr');

//importar libreria momentjs
import moment from "moment";
window.moment = require('moment');
moment.locale('es');


// filtros
Vue.filter('capitalize', value => {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
});

Vue.filter('lowercase', value => {
  if (!value) return ''
  value = value.toString()
  return value.toLowerCase()
});

Vue.filter('nif', value => {
  const letras="abcdefghyjklmnñopqrstuvwxyz";
  let nifMay = ''
  if(!value) return ''
    for(let i = 0; i < value.length; i++){
      if (letras.indexOf(value.charAt(i),0)!=-1){
        nifMay += value.charAt(i).toUpperCase();
      }else{
        nifMay += value.charAt(i);
      }
   }
  return nifMay;
})

Vue.filter('stringSlug', value =>{
  if (!value) return ''
  return slug(value); 
});

Vue.filter('formatearFecha', value => {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY');
  }
})
Vue.filter('formatearHora', value => {
  if (value) {
    return moment(String(value)).format('HH:mm');
  }
})

Vue.filter('formatearNivel', value =>{
  const patron = /[.,]/;
  if(patron.test(value)){
    return value;
  }else{
    return value + ".00";
  };
})


Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.config.silent = true;


Vue.directive('focus', {
  inserted(el) {
      el.focus()
    },
})

Vue.use(BootstrapVue,VueRouter,axios,toastr,slug, moment); 

//importo el archivo con las rutas
import {routesClub} from './rutas/routesClub';
import {routesInvitado} from './rutas/routesInvitado';

//importo el archivo almacen
import {store} from './store/store';

//instanciar el componente enrutador con las rutas importadas

let router = null;
let rol = null;
try {
  rol = window.Laravel.roles;
} catch (e) {

}

switch (rol) {
    case 3:
        
        router = new VueRouter({routes: routesClub});
        break;
    default:
      
        router = new VueRouter({routes: routesInvitado});
        break;
};


//importar google analytics
import VueAnalytics from 'vue-analytics';
import {ROAST_CONFIG} from './helpers/config';

const getGDPR = localStorage.getItem('GPDR:accepted');

if(typeof getGPDR !== 'undefined' && getGPDR === 'true'){
  Vue.use(VueAnalytics, {
    id: 'UA-129191991-1',
    router,
    disable: false,
    checkDuplicatedScript: true, //detecta si ya se ha agregado un script de análisis en su html
    debug:{
      enabled: true,
      trace: true,
      sendHitTask: true
    }
  })
}else if(typeof getGPDR !== 'undefined' && getGPDR === 'false'){
  Vue.use(VueAnalytics, {
    id: 'UA-129191991-1',
    router,
    disable: true,
    checkDuplicatedScript: true, //detecta si ya se ha agregado un script de análisis en su html
    debug:{
      enabled: true,
      trace: true,
      sendHitTask: true
    }
  })
}


import login            from './components/DashBoardClub/login.vue';
import {initialize}     from './helpers/general';

initialize(store, router);


new Vue({
  components: {
    'login-component': login,
    
    },
  router,
  store,
  el:'#app',
 
});


//google analytics SPA
gtag('set', 'page', router.currentRoute.path);
gtag('send', 'pageview');

router.afterEach(( to, from ) => {
  gtag('set', 'page', to.path);
  gtag('send', 'pageview');
});