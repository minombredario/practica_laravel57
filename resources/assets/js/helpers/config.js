/*
    Defines the API route we are using.
*/
let api_url = '';
/*const google_maps_js_api = 'AIzaSyCQknf1RSHZxhR_filPLtpkZjcqFC0XDyg';
const recaptchaV3_siteKey = '6LfEvoEUAAAAAP9_su75BXMs9-xhMVbGRoPF9VzC';
const recaptchaV3_secret = '6LfEvoEUAAAAAI0SgngSzuClpz-LDK-aJeJ2hKtv';*/
let google_maps_js_api = process.env.MIX_GOOGLE_MAPS_KEY_JS;
let recaptchaV3_siteKey = process.env.MIX_RECAPTCHAV3_SITEKEY;
let recaptchaV3_secret = process.env.MIX_RECAPTCHAV3_SECRET;
let google_analytics = process.env.MIX_GOOGLE_ANALYTICS;
   

switch( process.env.MIX_ENV_MODE ){
  
  case 'development':
    api_url = '127.0.0.1:8000/api/v1';
    break;
  case 'production':
    
    api_url = 'localhost:8000/api/v1';
  break;
  default:
    
    
  break;
  
}

export const ROAST_CONFIG = {
  API_URL: api_url,
  RECAPTCHAV3_SITEKEY: recaptchaV3_siteKey,
  GOOGLE_MAPS_JS_API: google_maps_js_api,
  G_ANALYTICS: google_analytics,
}