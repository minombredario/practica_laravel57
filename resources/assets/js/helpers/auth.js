import { setAuthorization } from "./general";

export function login(credenciales){
	return new Promise((res, rej) => {
		axios.post('auth/login' , credenciales).then( (response) => {
			setAuthorization(response.data.access_token);
            res(response.data);
            
		}, (error) => {
			
			rej("Email o contraseña incorrectos");
			console.log(error);
		}).catch((err) => {
			rej("Email o contraseña incorrectos");
			
		})
	})
}

export function getLocalUser(){
	const userStr = sessionStorage.getItem('user');
	
	if(!userStr){
		return null;
	}

	return JSON.parse(userStr)
}

export function getLocalClub(){
	const clubStr = sessionStorage.getItem('club');

	if(!clubStr){
		return null;
	}

	return JSON.parse(clubStr)
}

export function getLocalUserActive(){
	const userStr = sessionStorage.getItem('userActive');

	if(!userStr){
		return null;
	}

	return JSON.parse(userStr);
}

export function getLocalLigaActive(){
	const ligaStr = sessionStorage.getItem('ligaActive');
	if(!ligaStr){
		return null;
	}

	return JSON.parse(ligaStr);
}

export function getLocalTorneoActive(){
	const torneoStr = sessionStorage.getItem('torneoActive');
	if(!torneoStr){
		return null;
	}

	return JSON.parse(torneoStr);
}
