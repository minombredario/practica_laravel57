export default{
	created(){
		
	},
	methods:{
		//validadores
		validarFloat(numero){
      		const patron = /[.,]/;
      		return patron.test(numero)? true : false;
      	},
      	validarTexto(texto){
        	const soloLetras=/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/;
        	return soloLetras.test(texto)? true : false;
      	},
      	validateCIF(cif){
			//Quitamos el primer caracter y el ultimo digito
			let valueCif = cif.substr(1,cif.length-2);
		 	let suma = 0;
		 
			//Sumamos las cifras pares de la cadena
			for(let i = 1; i < valueCif.length; i = i+2){
				suma=suma+parseInt(valueCif.substr(i,1));
			}
		 
			let suma2=0;
		 
			//Sumamos las cifras impares de la cadena
			for(let i=0;i<valueCif.length;i=i+2){
				let result=parseInt(valueCif.substr(i,1))*2;
				if(String(result).length==1){
					// Un solo caracter
					suma2=suma2+parseInt(result);
				}else{
					// Dos caracteres. Los sumamos...
					suma2=suma2+parseInt(String(result).substr(0,1))+parseInt(String(result).substr(1,1));
				}
			}
		 
			// Sumamos las dos sumas que hemos realizado
			suma = suma + suma2;
		 
			let unidad = String(suma).substr(1,1)
			unidad = 10-parseInt(unidad);
		 
			let primerCaracter = cif.substr(0,1).toUpperCase();
		 
			if(primerCaracter.match(/^[FJKNPQRSUVW]$/)){
				//Empieza por .... Comparamos la ultima letra
				if(String.fromCharCode(64+unidad).toUpperCase() == cif.substr(cif.length-1,1).toUpperCase())
					this.errores.cif = '';
					return true;
			}else if(primerCaracter.match(/^[XYZ]$/)){
				//Se valida como un dni
				let newcif;
				if(primerCaracter=="X")
					newcif=cif.substr(1);
				else if(primerCaracter=="Y")
					newcif="1"+cif.substr(1);
				else if(primerCaracter=="Z")
					newcif="2"+cif.substr(1);
				return this.validateDNI(newcif);
			}else if(primerCaracter.match(/^[ABCDEFGHLM]$/)){
				//Se revisa que el ultimo valor coincida con el calculo
				if(unidad==10)
					unidad=0;
				if(cif.substr(cif.length-1,1)==String(unidad))
					this.errores.cif = '';
					return true;
			}else{
				//Se valida como un dni
				return this.validateDNI(cif);
			}
			this.errores.cif = "El Cif no es correcto";
			return false;
		},
		validateDNI(dni){
			let numero, letr, letra;
		    const expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

		    dni = dni.toUpperCase();

		    if(expresion_regular_dni.test(dni) === true){
		        numero = dni.substr(0,dni.length-1);
		        numero = numero.replace('X', 0);
		        numero = numero.replace('Y', 1);
		        numero = numero.replace('Z', 2);
		        letr = dni.substr(dni.length-1, 1);
		        numero = numero % 23;
		        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
		        letra = letra.substring(numero, numero+1);
		        if (letra != letr) {
		           this.errores.cif = 'Dni erroneo, la letra del NIF no se corresponde';
		            return false;
		        }else{
		        	this.errores.cif = '';
		            return true;
		        }
		    }else{
		       	this.errores.cif = 'Dni erroneo, formato no válido';
		        return false;
		    }
		},
		devolverFlotante(numero){
	    	const cuartos = ['25','50','75','00']
		    const cercano = Math.ceil(numero.toString().split('.')[1]*4/100)-1;
		 	if(cuartos.includes(numero.toString().split('.')[1]) || numero.toString().split('.')[1] == "5"){
				return true;
		    }else{
		    	if(numero.toString().length > 4){
		        	this.errores.nombre = "Máximo dos decimales";
		          	return false
		        }

		    	let entero = cuartos[cercano] == "00"? parseInt(numero.toString().split('.')[0]) + 1 : numero.toString().split('.')[0];
		        this.errores.nombre = `Multiplos de 0.25= ${entero}.${cuartos[cercano]}`;
		        this.nombre = `${entero}.${cuartos[cercano]}`;
		        return false;
		    }
	    },
      	//validar campos
      	validarTitulo(){
			let titulo = this.$options.origen == "editar" ? this.edit_.titulo : this.item.titulo ;
			let erroresServidor = this.erroresServidor;

			if(titulo != null){

				if(erroresServidor != null){
					if(erroresServidor.titulo){
						this.errores.titulo = erroresServidor.titulo;
						return false;
	 				};
				}
				
	 			if (titulo.trim() == ''){
					this.errores.titulo = '';
		          	this.errores.titulo = "No puede estar vacío";
		          	return false
		        }	
			}				        
	        return titulo == null? null : true;
		},
		validarTitular(){
			let titular = this.$options.origen == "editar" ? this.edit_.titular : this.item.titular ;
			let erroresServidor = this.erroresServidor;
			console.log(titular)
			if(titular != null){

				if(erroresServidor != null){
					if(erroresServidor.titular){
						this.errores.titular = erroresServidor.titular;
						return false;
	 				};
				}
				
	 			if (titular.trim() == ''){
					this.errores.titular = '';
		          	this.errores.titular = "No puede estar vacío";
		          	return false
		        }	
			}				        
	        return titular == null? null : true;
		},
		validarNombre(){
			let nombre = this.$options.origen == "editar" ? this.edit_.nombre : this.item.nombre ;
			let erroresServidor = this.erroresServidor;

			if(nombre != null){

				if(erroresServidor != null){
					if(erroresServidor.nombre){
						this.errores.nombre = "El código postal con la población ya existe";
						return false;
	 				};
				}
				
	 			if (nombre.trim() == ''){
					this.errores.nombre = '';
		          	this.errores.nombre = "No puede estar vacío";
		          	return false
		        }	
			}				        
	        return nombre == null? null : true;
		},
		validarPala(pala){
			
			let me = this.$options.origen == "editar" ? this.edit_.jugadoresPartida : this.item.jugadoresPartida;
			const patron = /^[ñÑ\w\d.*]+$/i;
			//const soloLetras=/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/;
			let palaEntrada = '';
			
			switch (pala){
				case 'pala1':
					palaEntrada = me.pala;
					break;
				case 'pala2':
					palaEntrada = me.pala2;
					break;
				case 'pala3':
					palaEntrada = me.pala3;
					break;
				case 'pala4':
					palaEntrada = me.pala4;
					break;
			};
			/*if(pala == 'pala1'){
				if(palaEntrada != null && palaEntrada != ''){
					if(palaEntrada == '' || palaEntrada == null ){
						this.errores = 'No puede estar vacío';
						return false;
					}
					if(!patron.test(palaEntrada)){
						this.errores = 'Debe contener caractares alfanumericos';
						return false;
			    	}
			    	
				}
				return palaEntrada == null? false : true;	
			}*/
			if(palaEntrada != null && palaEntrada != ''){
				if(!patron.test(palaEntrada.replace(/ /g, ""))){
					return false;
			    }
			}else{
				this.errores = '';
			    return true;
			}
			
		    		        
	        return palaEntrada == null? null : true;
		},
		validarNick(){
			let nick = this.$options.origen == "editar" ? this.edit_.nick : this.item.nick ;

			if(nick != null){
				if(this.erroresServidor.includes('nick')){
					return false;
	 			};
	 			if (nick.trim() == ''){
					this.errores.nick = '';
		          	this.errores.nick = "No puede estar vacío";
		          	return false
		        }	
			}				        
	        return nick == null? null : true;
		},
		validarResponsable(){
			const responsable = this.$options.origen == "editar" ? this.edit_.responsable : this.item.responsable;
			if( responsable != null){
				if (responsable.trim() == ''){
		          this.errores.responsable = "No puede estar vacío";
		          return false
		        }	
			}
				        
	        return responsable == null? null: true;  
		},
		validarTelefono(){
			const telefono = this.$options.origen == "editar" ? this.edit_.telefono : this.item.telefono;

			if(telefono != null){
				if(this.validarFloat(telefono)){
					this.errores.telefono = "Debe ser un número entero";
					return false;
				}
				if(telefono.toString().length != 9 && telefono != ''){
					this.errores.telefono = "Debe tener nueve digitos";
					return false;
				}
			}
			if(telefono == null || telefono == ''){

			};
			return telefono == null? null: true;  
		},
		validarDocumento(){
			const cif = this.$options.origen == "editar" ? this.edit_.cif : this.item.cif;
			if(cif != null){
				if(cif.trim() == ''){
					this.errores.cif = "No puede estar vacío";
		          	return false
				}
				if(cif.length > 9){
					this.errores.cif = "No puede tener más de 8 dígitos";
					return false;
				}
				if(!this.validateCIF(cif)){
					return false;
				}			
				
			}
			return cif == null? null: true; 
		},
		validarDireccion(){
			const direccion = this.$options.origen == "editar" ? this.edit_.direccion : this.item.direccion;
			if( direccion != null){
				if (direccion.trim() == ''){
		          this.errores.direccion = "No puede estar vacío";
		          return false
		        }	
			}
				        
	        return direccion == null? null: true; 
		},
		validarGerente(){
			const responsable = this.$options.origen == "editar" ? this.edit_.responsable : this.item.responsable;
			if( responsable != null){
				if (responsable.trim() == ''){
		          this.errores.responsable = "No puede estar vacío";
		          return false
		        }	
			}
				        
	        return responsable == null? null: true; 
		},
		validarMovil(){
			const movil = this.$options.origen == "editar" ? this.edit_.movil : this.item.movil;
			if(movil != null){
				if(this.validarFloat(movil)){
					this.errores.movil = "Debe ser un número entero";
					return false;
				}
				if(movil.toString().length != 9){
					this.errores.movil = "Debe tener nueve digitos";
					return false;
				}
			}
			return movil == null? null: true;
		},
		validarUrl(){
			const uri = this.$options.origen == "editar" ? this.edit_.web : this.item.web;
		  	const patron = /(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;
			if(uri != null){
				if(uri == ''){
	 				this.errores.web = '';
	 				return null
	 			};
	 			if(this.erroresServidor.includes('web')){
	 				return false;
	 			};
				if (patron.test(uri)) {
			  		return true;
				} else {
					this.errores.web = "La direccion contiene carácteres no válidos";
			 		return false;
				}
			}
			
 		},
 		validarMail(){
 			const mail = this.$options.origen == "editar" ? this.edit_.email : this.item.email;
 			const patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
 			if(mail != null){
 				if(mail == ''){
	 				this.errores.email = "El email no puede estar vacío";
	 				return false;
	 			};
	 			if(this.erroresServidor.includes('email')){
	 				return false;
	 			};
 				if(patron.test(mail)){
 					return true;
				}else{
					this.errores.email = "La direccion contiene carácteres no válidos";
			  		return false;
				};

 			};
 			return mail == null? null: true;
 			
 		},
 		validarPass(){
 			const pass = this.$options.origen == "editar" ? this.edit_.password : this.item.password;
 			const pass_confirm = this.$options.origen == "editar" ? this.edit_.password_confirmation : this.item.password_confirmation;
 			const patron = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/;

 			if((pass == '' || pass_confirm == '' || pass == null || pass_confirm == null) && this.pass == 'SI'){
 				this.errores.pass = "El campo contraseña no puede estar vacío";
				return false;
 			}else if((pass == '' || pass_confirm == '' || pass == null || pass_confirm == null) && this.pass == 'NO'){
 				return true;
 			}
 			if(pass != null || pass_confirm != null){
 				if(pass != null && pass_confirm == null){
	 				if(pass.trim() == ''){
	 					this.errores.pass = "El campo contraseña no puede estar vacío";
				  		return false;
	 				}else{
	 					this.errores.pass = "";

	 				}
	 				if(pass.length < 4){
	 					this.errores.pass = "La contraseña debe ser mayor a 4 caracteres";
				  		return false;
	 				}else{
	 					this.errores.pass = "";

	 				}
	 			}else{
	 				if(pass_confirm == ''){
	 					this.errores.pass_confirm = "El campo contraseña no puede estar vacío";
				  		return false;
	 				}else{
	 					this.errores.pass_confirm = "";

	 				}
	 				
	 				if(pass_confirm.length < 4){
	 					this.errores.pass_confirm = "La contraseña debe ser mayor a 4 caracteres";
				  		return false;
	 				}else{
	 					this.errores.pass_confirm = "";

	 				}

	 			};
	 			if(pass != null && pass_confirm != null){
		 			if(pass == pass_confirm){
	 					return true;
					}else{
						this.errores.pass = "Las contraseñas no coinciden";
						this.errores.pass_confirm = "Las contraseñas no coinciden";
				  		return false;
					};
				}
				
			}
 		},
 		validarPostal(){
      		const postal = this.$options.origen == "editar" ? this.edit_.postal : this.item.postal;
      		
      		if(postal == null ) return null;
      		if(this.erroresServidor != null){
      			if(this.erroresServidor.postal){
      				this.errores.postal = "El código postal con la población ya existe";
      				return false;
	 			};
      		}
      		

			if(postal > 1000 && postal <= 52080){
				//postal.toString().length == 5? provincia_id = postal.toString().substring(0,2): provincia_id = postal.toString().substring(0,1)
				if(this.item.postal < 0 ){
					this.errores.postal = "Debe ser un número positivo";
					return false;
				}
				if(this.validarFloat(postal) ){
					this.errores.postal = "No puede incluir ni puntos ni comas";
					return false;
				}
				if(postal == ''){
					this.errores.postal = "No puede estar vacío";
					return false;
				}	
			}else if(postal.toString().length == 0 ){
				this.errores.postal = "No puede estar vacío";
				return false;
			}else{
				this.errores.postal = "Debe ser un número entre 1000 y 52080";
           		return false;
			}
			return (postal == null || postal.toString().length == 0) ? null:(this.getPoblacion(), true); 
      	},
      	validarPoblacion(){
      		const poblacion = this.$options.origen == "editar" ? this.edit_.poblacion : this.item.poblacion;
      		if( poblacion != null){
				if (poblacion.trim() == ''){
		          this.errores.poblacion = "Código postal inexistente";
		          return false
		        }	
			}
				        
	        return poblacion == null? null: true;  
      	},
      	validarLatitud(){
      		const latitud = this.$options.origen == "editar" ? this.edit_.latitud : this.item.latitud;
      		if(latitud != null){
				return (latitud == null || latitud.toString().length == 0) ? (this.errores.latitud = "Ingresa una coordenada correcta", false ): true;
			}
			
			return (latitud == null || latitud.toString().length == 0) ? null : true;
      	},
      	validarLongitud(){
      		const longitud = this.$options.origen == "editar" ? this.edit_.longitud : this.item.longitud;
      		if(longitud != null){
				return (longitud == null || longitud.toString().length == 0) ? (this.errores.longitud = "Ingresa una coordenada correcta", false ): true;
			}
			
			return (longitud == null || longitud.toString().length == 0) ? null : true;
      	},
      	validarCorto(){
      		const corto = this.$options.origen == "editar" ? this.edit_.nombre_corto : this.item.nombre_corto;
	        if(corto != null){
		        if(this.erroresServidor != null){
		          if(this.erroresServidor.nombre_corto){
		            this.errores.corto = "La provincia ya existe o el nombre corto es incorrecto";
		            return false;
		          };
		        }	
	          if(corto.length <= 3){
	            if(corto.trim() == ''){
	              this.errores.corto = "No puede estar vacío";
	              return false
	            }
	            if(!this.validarTexto(corto)) {
	              this.errores.corto = "No puede contener números";
	              return false
	            };
	            if(corto.length != 3){
	              this.errores.corto = "Debe ser de tres digitos";
	              return false
	            }
	          }else{
	            this.errores.corto = "Debe ser de tres digitos";
	            return false
	          }
	        }
	        return corto == null? null: true;
	      },
      	validarPista(){
      		const pista = this.$options.origen == "editar" ? this.edit_.pistas : this.item.pistas;
      		if(pista != null){
      			if(pista < 0 ){
					this.errores.pistas = "Debe ser un número positivo";
					return false;
				}
				if(this.validarFloat(pista) ){
					this.errores.pistas = "No puede incluir ni puntos ni comas";
					return false;
				}
				if(pista == ''){
					this.errores.pistas = "No puede estar vacío";
					return false;
				}
      		}
      		
			return pista == null? null: true;	
      	},
      	validarCheck(){
      		const check = this.$options.origen == "editar" ? this.edit_.instalaciones : this.item.instalaciones;
			if(check.length == 0){
				this.errores.check = "Selecciona al menos una instalación";
				return false;
			}
		},
		validarHorario(){
			const error1 = "La hora de apertura no puede ser inferior a la de cierre";
			const error2 =  "Completa este campo correctamente"
			let horario = [];
			const me = this.$options.origen == "editar" ? this.edit_ : this.item;
			const status = this.status;
			
			//horario lunes a viernes ininterrumpido
			if(status.lv && !status.ld){
				if(me.lvm_start == null || me.lvm_start == '' || me.lvm_end == null || me.lvm_end == ''){this.errores.lvm = error2; return false}else{this.errores.lvm = ''}
				if(parseInt(me.lvm_start.substring(0,2)) < parseInt(me.lvm_end.substring(0,2)) || parseInt(me.lvm_end.substring(0,2)) == 0){
					this.errores.lvm = "";
					horario['LV'] = me.lvm_start + " a " + me.lvm_end;
					
				}else{
					this.errores.lvm = error1;
					return false;
				}
			}
			
			//horario lunes a domingo ininterrumpido
			if(status.ld){
				if(me.lvm_start == null || me.lvm_start == '' || me.lvm_end == null || me.lvm_end == ''){this.errores.lvm = error2; return false}else{this.errores.lvm = ''}
				if(parseInt(me.lvm_start.substring(0,2)) < parseInt(me.lvm_end.substring(0,2)) || parseInt(me.lvm_end.substring(0,2)) == 0){
					this.errores.lvm = "";
					horario['LD'] = me.lvm_start + " a " + me.lvm_end;
				}else{
					this.errores.lvm = error1;
					return false;
				}
			}

			//horario fin de semana ininterrumpido
			if(status.fs){
				if(me.fsm_start == null || me.fsm_start == '' || me.fsm_end == null || me.fsm_end == ''){this.errores.fsm = error2; return false}else{this.errores.fsm = ''}
				if(parseInt(me.fsm_start.substring(0,2)) < parseInt(me.fsm_end.substring(0,2)) || parseInt(me.fsm_end.substring(0,2)) == 0){
					this.errores.fsm = "";
					horario['FS'] = me.fsm_start + " a " + me.fsm_end;
				}else{
					this.errores.fsm = error1;
					return false;
				}
			}
			
			//horario lunes a viernes mañana y tarde
			//horario lunes a domingo horario mañana y tarde
			if((!status.lv && !status.ld) || status.ldmt){
				if(me.lvm_start == null || me.lvm_start == '' || me.lvm_end == null || me.lvm_end == ''){
					this.errores.lvm = error2; return false
				}else{
					this.errores.lvm = '';
					
				}
				if(me.lvt_start == null || me.lvt_start == '' || me.lvt_end == null || me.lvt_end == ''){
					
					this.errores.lvt = error2;
					
				}else{
					
					this.errores.lvt = '';
				}
				if(parseInt(me.lvm_start.substring(0,2)) < parseInt(me.lvm_end.substring(0,2))){
					
					if(parseInt(me.lvm_end.substring(0,2)) < parseInt(me.lvt_start.substring(0,2))){
						
						if(parseInt(me.lvt_end.substring(0,2)) > parseInt(me.lvt_start.substring(0,2)) || parseInt(me.lvt_end.substring(0,2)) == 0){
							this.errores.lvm = "";
							this.errores.lvt = "";
														
							if(!status.lv && !status.ld && !status.ldmt){
								
								horario['LVM'] = me.lvm_start + " a " + me.lvm_end;
								horario['LVT'] = me.lvt_start + " a " + me.lvt_end;
							}
							if(status.ldmt && !status.fs){
								horario['LDM'] = me.lvm_start + " a " + me.lvm_end;
								horario['LDT'] = me.lvt_start + " a " + me.lvt_end;
							}
														
						}else{
							
							this.errores.lvm = "";
							this.errores.lvt = error1;
							return false;
						}
					}else{
						
						this.errores.lvt = error1;
						this.errores.lvm = error1;
						return false;
					}
				}else{
					
					this.errores.lvm = error1;
					return false;
				}
			}

			
			//horario fin de semana mañana y tarde
			if(!status.fs && !status.ld && !status.ldmt){
				if(me.fsm_start == null || me.fsm_start == '' || me.fsm_end == null || me.fsm_end == ''){this.errores.fsm = error2; return false}else{this.errores.fsm = ''}
				if(me.fst_start == null || me.fst_start == '' || me.fst_end == null || me.fst_end == ''){this.errores.fst = error2; return false}else{this.errores.fst = ''}
				if(parseInt(me.fsm_start.substring(0,2)) < parseInt(me.fsm_end.substring(0,2))){
					if(parseInt(me.fsm_end.substring(0,2)) < parseInt(me.fst_start.substring(0,2))){
						if(parseInt(me.fst_end.substring(0,2)) > parseInt(me.fst_start.substring(0,2)) || parseInt(me.fst_end.substring(0,2)) == 0){
							this.errores.fsm = "";
							this.errores.fst = "";
							horario['FSM'] = me.fsm_start + " a " + me.fsm_end;
							horario['FST'] = me.fst_start + " a " + me.fst_end;
						}else{
							this.errores.fsm = "";
							this.errores.fst = error1;
							return false;
						}
					}else{
						this.errores.fst = error1;
						this.errores.fsm = error1;
						return false;
					}
				}else{
					this.errores.fsm = error1;
					return false;
				}
			}
			//horario festivo

			if(me.fm_start != null && me.fm_start != ''){
				//me.fm_start == null ? console.log(me.fm_start + ";" + me.fm_end +";"+me.ft_start+ ";" + me.fm_end): '';
				//horario festivo ininterrumpido
				if(status.f){

					if(me.fm_start == null || me.fm_start == '' || me.fm_end == null || me.fm_end == ''){this.errores.fm = error2; return false}else{this.errores.fm = ''}
					if(parseInt(me.fm_start.substring(0,2)) < parseInt(me.fm_end.substring(0,2)) || parseInt(me.fm_end.substring(0,2)) == 0){
						this.errores.fm = "";
						horario['F'] = me.fm_start + " a " + me.fm_end;
					}else{
						this.errores.fm = error1;
						return false;
					}
				}

				//horario dias festivo mañana y tarde
				if(!status.f){
					if((me.fm_start == null || me.fm_start == '' || me.fm_end == null || me.fm_end == '') && (me.ft_start == null || me.ft_start == '' || me.ft_end == null || me.ft_end == '')){
					
					}
					if(me.fm_start == null || me.fm_start == '' || me.fm_end == null || me.fm_end == ''){this.errores.fm = error2; return false}else{this.errores.fsm = ''}
					if(me.ft_start == null || me.ft_start == '' || me.ft_end == null || me.ft_end == ''){this.errores.ft = error2; return false}else{this.errores.ft = ''}
					if(parseInt(me.fm_start.substring(0,2)) < parseInt(me.fm_end.substring(0,2))){
						if(parseInt(me.fm_end.substring(0,2)) < parseInt(me.ft_start.substring(0,2))){
							if(parseInt(me.ft_end.substring(0,2)) > parseInt(me.ft_start.substring(0,2)) || parseInt(me.ft_end.substring(0,2)) == 0){
								this.errores.fm = "";
								this.errores.ft = "";
								horario['FM'] = me.fm_start + " a " + me.fm_end;
								horario['FT'] = me.ft_start + " a " + me.ft_end;
							}else{
								this.errores.fm = "";
								this.errores.ft = error1;
								return false;
							}
						}else{
							this.errores.ft = error1;
							this.errores.fm = error1;
							return false;
						}
					}else{
						this.errores.fm = error1;
						return false;
					}
				}
			}else{
				this.errores.fm = '';

			}
			
			return horario;
		},

		validarFecha(){
			const me = this.$options.origen == "editar" ? this.edit_ : this.item;
			let fecha = new Date();
			if(me.fecha != null){
				console.log(`${fecha.getFullYear()}-${(fecha.getMonth() + 1) < 10? ('0' + (fecha.getMonth() + 1)) : (fecha.getMonth() + 1)}-${fecha.getDate()}`);
				console.log(me.fecha);
			}
		},
      	
	}
}