
import {ROAST_CONFIG} from '../helpers/config';

export default{
  
  data: ()=>({
      apiUrl: 'api',
   
      sitekey: ROAST_CONFIG.RECAPTCHAV3_SITEKEY,
    
      comboNivelesReales:[
         {value: 1, text:'1.00'},
         {value: 2, text:'1.25'},
         {value: 3, text:'1.50'},
         {value: 4, text:'1.75'},
         {value: 5, text:'2.00'},
         {value: 6, text:'2.25'},
         {value: 7, text:'2.50'},
         {value: 8, text:'2.75'},
         {value: 9, text:'3.00'},
         {value: 10, text:'3.25'},
         {value: 11, text:'3.30'},
         {value: 12, text:'3.50'},
         {value: 13, text:'3.75'},
         {value: 14, text:'3.80'},
         {value: 15, text:'4.00'},
         {value: 16, text:'4.20'},
         {value: 17, text:'4.25'},
         {value: 18, text:'4.30'},
         {value: 19, text:'4.40'},
         {value: 20, text:'4.50'},
         {value: 21, text:'4.75'},
         {value: 22, text:'5.00'},
         {value: 23, text:'5.25'},
         {value: 24, text:'5.50'},
         {value: 25, text:'5.75'},
         {value: 26, text:'6.00'},
         {value: 27, text:'6.25'},
         {value: 28, text:'6.50'},
         {value: 29, text:'6.75'},
         {value: 30, text:'7.00'}
      ],
  }),
  
  computed:{
      isMob() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent);
      }
  },
	created(){
    window.scrollTo(0, 0);
      //this.apiUrl = `${window.location.host}/club/api`;
      //console.log(`${this.apiUrl}/cmbprvs`);
	},
	methods:{
        detectmob() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent);
        },

        async getComboProvincias() {
            try {
                const response = await axios.get(`${this.apiUrl}/cmbprvs`);
                const provincias = response.data.map((provincia) => ({
                    value: provincia.id,
                    text: provincia.nombre,
                }));

                provincias.push({ value: 0, text: '- Selecciona provincia -' });
                this.$store.commit('provincias', provincias);
            } catch (error) {
                console.error('Error al obtener provincias:', error);
            }
        },
        async getPoblacion() {
            try {
                const me = this.$options.origen === 'editar' ? this.edit_ : this.item;
                const response = await axios.get(`${this.apiUrl}/pblprv?busqueda=${me.postal}`);

                if (response.data.length > 0) {
                    const poblacionData = response.data[0];
                    me.poblacion_id = poblacionData.id;
                    me.poblacion = poblacionData.nombre;
                    me.provincia_id = poblacionData.provincia_id;
                    me.provincia = poblacionData.provincia.nombre;
                } else {
                    me.poblacion = '';
                }
            } catch (error) {
                console.error('Error al obtener población:', error);
            }
        },

        async getNiveles() {
            try {
                const response = await axios.get(`${this.apiUrl}/cmbnvls`);

                const niveles = response.data.map((nivel) => ({
                    value: nivel.id,
                    text: nivel.nombre,
                }));

                this.$store.commit('niveles', niveles);
            } catch (error) {
                console.error('Error al obtener niveles:', error);
            }
        },

        async getComboInstalaciones() {
            try {
                const response = await axios.get(`${this.apiUrl}/cmbnsts`);

                this.$store.commit('instalaciones', response.data);
            } catch (error) {
                console.error('Error al obtener instalaciones:', error);
            }
        },

        async getClubs() {
            try {
                const response = await axios.get(`${this.apiUrl}/cmbClubs`);

                const clubs = response.data.map((club) => ({
                    value: club.id,
                    text: club.nombre,
                    pistas: club.pistas,
                }));

                this.$store.commit('clubs', clubs);
            } catch (error) {
                console.error('Error al obtener clubs:', error);
            }
        },
        toFormData(obj, logo) {
            const form_data = new FormData();

            for (const [key, value] of Object.entries(obj)) {
                form_data.append((key === 'logo' || key === 'avatar' || key === 'imagen' || key === 'cartel') ? key : logo, value);
            }

            return form_data;
        },

        formatHorario(horario) {
            const horarioTexts = {
                LV: 'Horario de lunes a viernes de ',
                LVM: 'Horario de lunes a viernes de ',
                LVT: ' y de ',
                LD: 'Horario de lunes a domingo de ',
                LDM: 'Horario de lunes a domingo de ',
                LDT: ' y de ',
                FS: 'Horario fin de semana de ',
                FSM: 'Horario fin de semana de ',
                FST: ' y de ',
                F: 'Horario féstivos de ',
                FM: 'Horario féstivos de ',
                FT: ' y de ',
            };

            let salida = '';

            for (const [key, value] of Object.entries(horario)) {
                salida += horarioTexts[key] + value + (key.endsWith('T') ? '|' : '');
            }

            return salida !== '' ? salida.substring(0, salida.length - 1) : '';
        },

        desgloasarHorarioEditar(horario) {
            const regex = /(\d+)/g;
            const me = this.$options.origen === 'editar' ? this.edit_ : this.item;
            const status = this.status;
            const horas = horario.match(regex);

            const assignHorario = (startIdx, endIdx, startProp, endProp, statusProp) => {
                me[startProp] = horas[startIdx] + ':' + horas[startIdx + 1];
                me[endProp] = horas[endIdx] + ':' + horas[endIdx + 1];
                status[statusProp] = true;
            };

            const horarioMappings = {
                'lunes a viernes': { length: 43, status: 'lv', start: 'lvm_start', end: 'lvm_end' },
                'lunes a domingo': { length: 43, status: 'ld', start: 'lvm_start', end: 'lvm_end' },
                'fin de semana': { length: 38, status: 'fs', start: 'fsm_start', end: 'fsm_end' },
                'féstivos': { length: 33, status: 'f', start: 'fm_start', end: 'fm_end' },
            };

            for (const [horarioKey, mapping] of Object.entries(horarioMappings)) {
                if (horario.includes(horarioKey) && horario.length === mapping.length) {
                    assignHorario(0, 3, mapping.start, mapping.end, mapping.status);
                }

                if (horario.includes(horarioKey) && horario.length > mapping.length) {
                    status[mapping.status] = false;
                    assignHorario(0, 3, mapping.start, mapping.end, mapping.status);
                    assignHorario(4, 7, mapping.start + 't', mapping.end + 't', mapping.status + 'mt');
                }
            }
        },

        disponibilidadJugador(item) {
            let disponibilidad = '';

            for (const [key, value] of Object.entries(item)) {
                if (value != null && value !== '') {
                    disponibilidad += `${key}: ${value},`;
                }
            }

            return disponibilidad !== '' ? disponibilidad.substring(0, disponibilidad.length - 1) : '';
        },

        desglosarDisponibilidadEditar(item) {
            const diasHoras = {};

            if (item != null && item !== '') {
                const dias = item.split(',');

                dias.forEach((dia) => {
                    const [key, value] = dia.split(':');
                    diasHoras[key.trim()] = value.trim();
                });
            }

            return diasHoras;
        },

        async buscarPistas(dia, id) {
            try {
                const response = await axios.get(`${this.apiUrl}/pistasDisponibles?club=${id}&fecha=${dia}`);
                this.partidas = response.data;
            } catch (error) {
                console.error('Error al buscar pistas:', error);
            }
        },


    }
}
