

export function initialize(store, router){
	/*gtag('set', 'page', router.currentRoute.path);
	gtag('send', 'pageview');*/

	router.beforeEach(( to, from, next ) => {
	  
	//paginas con autorizacion
	  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
	  const currentUser = store.state.currentUser;

	  if(requiresAuth && !currentUser){

	    next('/login');
	  }else if(to.path == '/login' && currentUser){
	    
	    next('/');
	  }else{
	   
	    next();
	  }

	});

	router.afterEach(( to, from, next ) => {
	  gtag('set', 'page', to.path);
	  gtag('send', 'pageview');
	  //document.title = ( window.Laravel.name + " | " + to.meta.title );

	  //next();
	 
	});

	/*axios.interceptors.response.use(null, (error) => {
		console.log(error.response)
	  	if(error.response.status == 401) {
	    	store.commit('logout');	
	    	//router.push('/iniciarSesion');

	  	}

	  	return Promise.reject(error);
	});*/


	if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}

