@php
	$pala = $user->sexo == "HOMBRE"? "jugador" : "jugadora" ;
	$bienvenido = $user->sexo == "HOMBRE"? "bienvenido" : "bienvenida" ;
@endphp

	
	{{ $user->nombre }} {{ $bienvenido }} a {{ $user->club }}:
	Te has registrado como {{ $user->nick }} en nuestro sistema de reservas online.
	Los datos de acceso a nuestra web son:

	Usuario: {{ $user->email }}
    Contraseña: {{ $user->password }}
   
	Los datos que nos has facilitado no serán cedidos a empresas externas a nuestro club, utilizándose únicamente para comunicaciones del club.

	Esperamos verte pronto en {{ $user->club }}