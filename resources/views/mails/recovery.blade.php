@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' =>  $url])
	<img src="<?php echo $logo ;?>" style="{{$estilo}}"> {{ config('app.name')}}
@endcomponent
@endslot
{{-- Body --}}
	
{{-- Greeting --}}

@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif
{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)

<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent

@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
Regards,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
    
@slot('subcopy')
@component('mail::subcopy')
Si estás teniendo problemas para hacer clic en el botón "{{ $actionText }}"   copia y pega el siguiente URL en el navegador web:  [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endslot
    
{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}.
@endcomponent
@endslot
@endcomponent
