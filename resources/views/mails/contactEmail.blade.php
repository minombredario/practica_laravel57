
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Contacto desde la web</title>
</head>
<body>
	<div style="text-align:center; background-color: #000; color: #fc0101">
		<h2>Contacto web</h2>
	</div>
	<br>
	
	<div style="width: 75%; margin: 10px auto; font-size: 16px">
		<p>
			@php
				echo "Teléfono: " . $telf
			@endphp
		</p>
		<p>
			@php
				echo "Email: " . $from
			@endphp
		</p>
		<p>
			@php
				echo $mensaje
			@endphp
		</p>
	</div>

	
	
    <br>
    <hr>

</body>
</html>