
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Datos nueva alta</title>
</head>
<body>
	@php
		$pala = $user->sexo == "HOMBRE"? "jugador" : "jugadora" ;
		$bienvenido = $user->sexo == "HOMBRE"? "bienvenido" : "bienvenida" ;
	@endphp

	<div style="text-align:center; background-color: #000">
		<a href="{{ $user->url }}"> 
			<img src="{{ $user->logo }}" alt="{{ $user->club }}" title="{{ $user->club }}" height="100" width="100" style="border-radius: 50px; margin-top: 4px"></img>
		</a>
    <h3><a href="{{ $user->url }}">{{ $user->club }}</a></h3>
	</div>
	<br>
	{{ $user->nombre }} {{ $bienvenido }} a {{ $user->club }}:
	<p><strong>Te has registrado como {{ $user->nick }} en nuestro sistema de reservas online.</strong></p>
	<p>Los datos de acceso para acceder a la web son:</p>

	
	<p><strong>Email:</strong> {{ $user->email }}</p>
	<p><strong>Nick:</strong> {{ $user->nick }}</p>
    <p><strong>Contraseña:</strong> {{ $user->password }}</p>
    <br>
    El acceso será mediante el email o el nick y la contraseña
    <br>
    <hr>


	<p>Los datos que nos has facilitado no serán cedidos a empresas externas a nuestro club, utilizándose únicamente para comunicaciones del club.</p>

	<p>Esperamos verte pronto en <a href="{{ $user->url }}"> {{ $user->club }}</a></p>

	</body>
</html>

