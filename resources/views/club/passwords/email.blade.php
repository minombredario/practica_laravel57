@extends('layouts.vueApp')

@section('css')
 <style rel="stylesheet">
    body{
        font-family: 'Roboto', sans-serif;
        background: url({{asset('img/fondo/fondo1.jpg')}}) no-repeat center center fixed;
        background-size: cover;
    }
    /*.fondoModalUser{
        margin-top: 98px !important;
        max-width: 500px
    }*/
    .user-img img{
        height: 120px;
        width: 120px;
    }
    .user-img{
        margin-top: -60px;
        
        
    }
    .form-group input{
        font-size: 18px;
        letter-spacing: 2px;
        padding-left: 54px;
    }

    .ico1::before, .ico2::before{
        font-family: "Ionicons";
        content: "\f3d9";
        position: absolute;
        font-size: 22px;
        left: 28px;
        padding-top: 4px;
        color: #555e60;
    }
    .ico2::before{
        content: "\f1af";
    }
   
    .btn-success{
        background-color: #1c6288;
        font-size: 19px;
        border-radius: 5px;
        padding: 7px 14px;
        border: 1px solid #daf1ff;
    }
    .btn-success:hover{
        background-color: #13445e;
        border: 1px solid #daf1ff;
    }

    /* The Modal (background) */
    .modal {
        display: block; /* Hidden by default */
        background-color: rgba(0,0,0,0);
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        padding-top: 60px;
        max-width: 560px;
        margin: 98px auto;
        padding-right: 30px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #434e5a;
        opacity: .8;
        padding: 0 33px;
        border-radius: 10px;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.2);
        outline: 0;
    }
    .title{
        padding: 5px 0 25px;
        text-align: center;
    }
    .title h3{
        color: #fff;
    }



</style>
@endsection

@section('content')

<div class="modal mx-auto text-center">
    <div class="modal-content">
        
        <b-row class="px-0">
           
            <b-col cols="12" class="user-img mx-auto">
                <b-img src="{{asset('img/avatar/face.png')}}" fluid rounded center />
            </b-col>
            
            <b-col>
                
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </b-col>
            <b-col cols="12">
                <div class="title"><h3>Recuperar contraseña</h3></div>
                <form class="form-horizontal" method="POST" action="{{ route('club.password.email') }}">
                    {{ csrf_field() }}
                        
                        <div class="mb-5 ico1 form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                        <b-form-input type="email" ref="email" name="email" placeholder="Email" value="{{ old('email') }}" required></b-form-input>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        
                    </div>
                    <b-btn variant="success" type="submit" class="mb-2">Enviar email</b-btn>
                   
                </form>
            </b-col>
            <b-col cols="12">
               
            </b-col>
                           
            </b-row>
        </div>   
        
    </div>
@endsection

@section('scripts')
    

@endsection



