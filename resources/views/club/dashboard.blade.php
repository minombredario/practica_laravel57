@extends('layouts.vueApp')

@section('content')
	@if(Auth::guard('club')->check())
		<transition name="slide-fade" mode="out-in" appear>
			<router-view :datos="{{ json_encode($club) }}"></router-view>
		</transition>
	@endif
@endsection

@section('scripts')
	
 <script type="text/javascript">


</script>
@endsection