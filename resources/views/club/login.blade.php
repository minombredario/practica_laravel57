@extends('layouts.vueApp')

@section('css')
 <style rel="stylesheet">
    body{
        font-family: 'Roboto', sans-serif;
        background: url({{asset('img/fondo/fondo1.jpg')}}) no-repeat center center fixed;
        background-size: cover;
    }

</style>
@endsection

@section('content')
    
    <login-component :recuperarpass="{{ json_encode($recuperarPass) }}" :dashboard="{{ json_encode($dashboard) }}"></login-component>

@endsection

@section('scripts')
    

@endsection