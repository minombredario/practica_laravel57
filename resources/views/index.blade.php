@extends('layouts.webLayout')
@section('css')
 <style rel="stylesheet">

 	#fixed {
      background-image: url({{asset('img/fondo/fondo4.jpg')}});
  }
	.padding{
	    padding-bottom: 2rem;
	}
</style>


@section('content')
 	
    <transition name="slide-fade" mode="out-in" appear>
    @guest
       <router-view :datos="{{ json_encode($club) }}" :partidas="{{ json_encode($partidas) }}"></router-view>
    @else
        <router-view :datos="{{ json_encode($club) }}" :partidas="{{ json_encode($partidas) }}" :user="{{ json_encode(Auth::user()->name) }}"></router-view>
    @endguest
      
    </transition>
 
@endsection

@section('scripts')
  
 <script type="text/javascript">


</script>
@endsection

