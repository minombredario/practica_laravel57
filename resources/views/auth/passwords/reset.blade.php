@extends('layouts.vueApp')

@section('css')
 <style rel="stylesheet">
    body{
        font-family: 'Roboto', sans-serif;
        background: url({{asset('img/fondo/fondo1.jpg')}}) no-repeat center center fixed;
        background-size: cover;
    }
    /*.fondoModalUser{
        margin-top: 98px !important;
        max-width: 500px
    }*/
    .user-img img{
        height: 120px;
        width: 120px;
    }
    .user-img{
        margin-top: -60px;
        margin-bottom: 45px;
    }
    .form-group input{
        font-size: 18px;
        letter-spacing: 2px;
        padding-left: 54px;
    }

    .ico1::before, .ico2::before{
        font-family: "Ionicons";
        content: "\f3d9";
        position: absolute;
        font-size: 22px;
        left: 28px;
        padding-top: 4px;
        color: #555e60;
    }
    .ico2::before{
        content: "\f1af";
    }
   
    .btn-success{
        background-color: #1c6288;
        font-size: 19px;
        border-radius: 5px;
        padding: 7px 14px;
        border: 1px solid #daf1ff;
    }
    .btn-success:hover{
        background-color: #13445e;
        border: 1px solid #daf1ff;
    }

    /* The Modal (background) */
    .modal {
        display: block; /* Hidden by default */
        background-color: rgba(0,0,0,0);
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        padding-top: 60px;
        max-width: 560px;
        margin: 98px auto;
        padding-right: 30px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #434e5a;
        opacity: .8;
        padding: 0 33px;
        border-radius: 10px;
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.2);
        outline: 0;
    }



</style>
@endsection

@section('content')

<div class="modal mx-auto text-center">
    <div class="modal-content">
        
        <b-row class="px-0">
           
            <b-col cols="12" class="user-img mx-auto">
                <b-img src="../../img/avatar/face.png" fluid rounded center />
            </b-col>
            
            <b-col>
                
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </b-col>
            <b-col cols="12">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="mb-5 ico1 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </b-col>
            <b-col cols="12">
               
            </b-col>
                           
            </b-row>
        </div>   
        
    </div>
@endsection
