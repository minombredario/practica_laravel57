@extends('layouts.app')
@section('content')
 	
<div class="container text-center">
    <div class="btn-group">
    	<button type="button" class="btn btn-success" name="config-cache" onclick="location.href='{{ url('config-cache') }}'">config-cache</button>
        <button type="button" class="btn btn-success" name="clear-cache" onclick="location.href='{{ url('clear-cache') }}'">clear-cache2</button>
        <button type="button" class="btn btn-success" name="optimize" onclick="location.href='{{ url('optimize') }}'">optimize</button>
        <button type="button" class="btn btn-success" name="route-cache"  onclick="location.href='{{ url('route-cache') }}'">route-cache</button>
        <button type="button" class="btn btn-success" name="view-clear" onclick="location.href='{{ url('view-clear') }}'">view-clear</button>

    </div>
    @if (session('status'))
        <div class="alert alert-success mt-5">
            {{ session('status') }}
        </div>
    @endif
</div>
 
@endsection
@section('scripts')

@endsection