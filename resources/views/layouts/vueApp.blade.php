<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'Laravel') }}</title>
		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">
		<link href="" rel='stylesheet' media='print'>

		<script async defer src=<?php echo 'https://maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAPS_KEY_JS') ?>></script>
		<script>
			window.Laravel = {!! json_encode([
				'csrfToken' => csrf_token(),
				'roles' => Auth::user() ? Auth::user()->rol_id : '',
				'name' => Auth::user() ? Auth::user()->nombre : '',
				]) !!};
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!--script src=<?php echo 'https://www.google.com/recaptcha/api.js?render=' . env('RECAPTCHAV3_SITEKEY') ?> ></script-->
		<script>
		  	window.dataLayer = window.dataLayer || [];
		  	function gtag(){dataLayer.push(arguments);}
		  	gtag('js', new Date());

			gtag('config',<?php echo "'" . env('GOOGLE_ANALYTICS') . "'" ?>);
			gtag('create', <?php echo "'" . env('GOOGLE_ANALYTICS	') . "'" ?>, 'auto');
			gtag('send', 'pageview');
		</script>

	</head>
	<body>
		@yield('css')
		<div id="app" v-cloak>
			@yield('content')   
		</div>
		@yield('scripts')

		<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
		<script type="text/javascript" src="{{mix('js/alerts.js')}}"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		

	</body>
</html>