-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-12-2018 a las 18:42:52
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `padel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_pistas`
--

CREATE TABLE `horario_pistas` (
  `id` int(11) NOT NULL,
  `club_id` int(11) UNSIGNED NOT NULL,
  `pista_id` int(11) UNSIGNED NOT NULL,
  `lunes` varchar(45) DEFAULT '08:00',
  `martes` varchar(45) DEFAULT '08:00',
  `miercoles` varchar(45) DEFAULT '08:00',
  `jueves` varchar(45) DEFAULT '08:00',
  `viernes` varchar(45) DEFAULT '08:00',
  `sabado` varchar(45) DEFAULT '08:00',
  `domingo` varchar(45) DEFAULT '08:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `horario_pistas`
--

INSERT INTO `horario_pistas` (`id`, `club_id`, `pista_id`, `lunes`, `martes`, `miercoles`, `jueves`, `viernes`, `sabado`, `domingo`) VALUES
(1, 1, 1, '08:00', '09:00', '08:00', '08:00', '08:00', '08:00', '08:00'),
(2, 1, 2, '08:00', '09:00', '08:00', '08:00', '08:00', '08:00', '08:00'),
(3, 1, 3, '08:00', '08:00', '08:00', '09:00', '09:00', '08:00', '08:00'),
(4, 1, 4, '08:00', '08:00', '08:00', '08:00', '08:00', '08:00', '08:00'),
(5, 1, 5, '08:00', '08:00', '08:00', '08:00', '08:00', '08:00', '08:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `horario_pistas`
--
ALTER TABLE `horario_pistas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `pista_id_club_id_unique` (`club_id`,`pista_id`),
  ADD KEY `club_id_clubs_id_foreign_idx` (`club_id`),
  ADD KEY `pista_id_pistas_id_foreign_idx` (`pista_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `horario_pistas`
--
ALTER TABLE `horario_pistas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `horario_pistas`
--
ALTER TABLE `horario_pistas`
  ADD CONSTRAINT `club_id_clubs_id_foreign` FOREIGN KEY (`club_id`) REFERENCES `clubs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pista_id_pistas_id_foreign` FOREIGN KEY (`pista_id`) REFERENCES `pistas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
